# java8-zh #

## 说明 ##

- 整个项目基于jdk1.8.0_102

- src-zh下为中文翻译内容,src-en下为原版内容,以便对照

- 整个项目大概有200M,15000+文件,初次下载时请慎重

- 鉴于个人工作经验及英文水平所限,对翻译过程中可能出现的纰漏深表歉意,如有发现敬请指出,在此先表示万分感谢

- 鉴于jdk版本更新相对比较频繁,同时个人时间精力有限,在1.8系列的最终版发布之前,不考虑同步更新.只以jdk1.8.0_102为准.

- 在汉化的过程中,参考了JDK1.6中文版API,对1.6中文版中的一些翻译错误,在本项目中进行了修正.
  比如对`Java.lang.Short`类中的`decode(String s)`方法.原版注释中有这么一句:
	
		The result is negated if first character of the specified {@code String} is the minus sign.
  
  JDK1.6中文版中是这样翻译的:

		如果指定 String 的第一个字符是减号，则结果无效.

  在这里将negated翻译为无效,明显不对头嘛...
	

## 使用方法 ##

1. 安装JDK1.8_102(版本最好保持一致),下载地址在本文档最后.
2. 下载该项目源码.
3. 打开IDE(Eclipse、IDEA或其它),这里以IDEA为例,Platform Settings --> SDKs,界面右侧切换到Sourcepath标签页,删除原有的默认的 JDK根目录\src.zip 一项,然后新加一项,指向第一步下载下来的源码中的src-zh目录.比如我的:
	![](http://i.imgur.com/lydhKfD.png)

## 效果 ##

![](http://i.imgur.com/gIaxIep.png)

![](http://i.imgur.com/qRQYSlu.png)

## 欢迎参与 ##

 欢迎参与

 要不然,一不小心这就会成为一个有生之年系列.

 参与本项目的最大的便捷之处是,在平时码其它项目代码的时如果查看JDK源码,遇到了未翻译的API,可以直接点进去顺手翻译了,然后记得提交就行了...不过请务必遵守下面的翻译约定,宁缺毋滥.


## 约定 ##

- 按照原文翻译,在不改变原文含义的情况下,可按照中文表达习惯进行小幅度重新组织;

- 如果想加入一些额外说明,请在要加入额外说明的位置,按照如下格式添加,其中XXXX为具体备注内容:

    	返回一个新的{@code String}对象(译者注:XXXX)

- 宁缺毋滥

## 参考资料 ##

1. [Java Language Specification 在线地址](http://docs.oracle.com/javase/specs/jls/se8/html/index.html "在线地址") 

2. [JDK1.6中文API文档 在线地址](http://tool.oschina.net/apidocs/apidoc?api=jdk-zh "在线地址")


## 下载资源 ##

  [jdk1.8.0_102 官网下载地址](http://101.110.118.69/download.oracle.com/otn-pub/java/jdk/8u102-b14/jdk-8u102-windows-x64.exe?AuthParam=1470384942_7d1e041715c3f85c9e42a9ad72cd8a06 "官网地址")

  [jdk1.8.0_102 百度云盘地址](http://pan.baidu.com/s/1dFhZyVJ "如果官网的挂了(虽然我觉得不大可能)")

  [Java Language Specification Java SE 8 Edition PDF格式](http://docs.oracle.com/javase/specs/jls/se8/jls8.pdf "PDF版")
