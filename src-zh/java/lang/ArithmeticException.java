/*
 * Copyright (c) 1994, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * 当出现异常的运算条件时,抛出此异常.例如,一个整数"除以零"时,抛出此类的一个实例.
 *
 * 如果{@linkplain Throwable#Throwable(String,Throwable, boolean, boolean)}中suppression被禁用和/或堆栈追踪不可写,
 * 那么{@code ArithmeticException} 对象可能会由虚拟机构造,
 *
 *
 * @author  unascribed
 * @since   JDK1.0
 */
public class ArithmeticException extends RuntimeException {
    private static final long serialVersionUID = 2256477558314496007L;

    /**
     * 构造不带详细消息的{@code ArithmeticException}.
     */
    public ArithmeticException() {
        super();
    }

    /**
     *  构造具有指定详细消息的 {@code ArithmeticException}.
     *
     * @param   s   详细消息.
     */
    public ArithmeticException(String s) {
        super(s);
    }
}
