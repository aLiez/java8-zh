/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.lang.annotation.*;

/**
 * 表示一个方法声明打算重写超类中的另一个方法声明.
 * 如果方法利用此注释类型进行注解,编译器会生成一条错误消息,除非符合下列条件中的至少一项:
 *
 * <ul><li>
 * 该方法重写或者实现父类中的一个方法.
 * </li><li>
 * 该方法相当于重写了{@linkplain Object}类中的任何一个public方法.
 * </li></ul>
 *
 * @author  Peter von der Ah&eacute;
 * @author  Joshua Bloch
 * @jls 9.6.1.4 @Override
 * @since 1.5
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface Override {
}
