/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 *
 * 一个应用尝试创建一个大小为负数的数组时抛出的异常.
 *
 * @author  unascribed
 * @since   JDK1.0
 */
public
class NegativeArraySizeException extends RuntimeException {
    private static final long serialVersionUID = -8960118058596991861L;

    /**
     * 构造一个不带详细消息的 <code>NegativeArraySizeException</code>.
     */
    public NegativeArraySizeException() {
        super();
    }

    /**
     * 构造一个带指定详细消息的  <code>NegativeArraySizeException</code>.
     *
     * @param   s   详细消息.
     */
    public NegativeArraySizeException(String s) {
        super(s);
    }
}
