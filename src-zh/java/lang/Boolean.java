/*
 * Copyright (c) 1994, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * {@code Boolean}是将基本类型{@code boolean}包装成了一个对象.
 * 一个{@code Boolean}类型的对象只包含一个类型为{@code boolean}的字段.
 *
 * <p>
 * 另外,该类还提供了多种将{@code boolean}转换为{@code String}
 * 以及将{@code String}转换为{@code boolean}的方法,
 * 并且还提供了一些其它很有用的处理{@code byte}的方法.
 *
 * @author  Arthur van Hoff
 * @since   JDK1.0
 */
public final class Boolean implements java.io.Serializable,
                                      Comparable<Boolean>
{
    /**
     * 对应基值{@code true}的{@code Boolean}对象.
     */
    public static final Boolean TRUE = new Boolean(true);

    /**
     * 对应基值{@code false}的{@code Boolean}对象.
     */
    public static final Boolean FALSE = new Boolean(false);

    /**
     * 表示基本类型boolean的Class对象.
     *
     * @since   JDK1.1
     */
    @SuppressWarnings("unchecked")
    public static final Class<Boolean> TYPE = (Class<Boolean>) Class.getPrimitiveClass("boolean");

    /**
     * Boolean的值.
     *
     * @serial
     */
    private final boolean value;

    /** 使用来自JDK 1.0.2的serialVersionUID,为了兼容 */
    private static final long serialVersionUID = -3665804199014368530L;

    /**
     * 分配一个代表{@code value}参数的{@code Boolean}对象.
     *
     * <p><b>注: 通常情况下都不建议采用该构造方法.
     * 除非需要一个<i>新的</i>实例,否则静态工厂方法{@link #valueOf(boolean)}会是一个更好的选择.
     * 这可能会显著的提高空间和时间性能</b>
     *
     * @param   value   the value of the {@code Boolean}.
     */
    public Boolean(boolean value) {
        this.value = value;
    }

    /**
     * 如果string参数不是{@code null}并且在忽略大小写时等于字符串{@code "true"},
     * 分配一个代表{@code true}参数的{@code Boolean}对象.
     * 否则, 分配一个代表{@code false}参数的{@code Boolean}对象.
     * 示例:<p>
     * {@code new Boolean("True")} 产生一个代表{@code true}的{@code Boolean}对象.<br>
     * {@code new Boolean("yes")} 产生一个代表{@code false}的{@code Boolean}对象.
     *
     * @param   s   要转换成{@code Boolean}的字符串.
     */
    public Boolean(String s) {
        this(parseBoolean(s));
    }

    /**
     * 将字符串解析为boolean.
     * 如果string参数不是{@code null}并且在忽略大小写时等于字符串{@code "true"},
     * 返回的{@code boolean}值为{@code true}
     * <p>
     * 示例: {@code Boolean.parseBoolean("True")} returns {@code true}.<br>
     * 示例: {@code Boolean.parseBoolean("yes")} returns {@code false}.
     *
     * @param      s   包含要解析的boolean表达式的{@code String}
     * @return     string参数代表的boolean值
     * @since 1.5
     */
    public static boolean parseBoolean(String s) {
        return ((s != null) && s.equalsIgnoreCase("true"));
    }

    /**
     * 将此{@code Boolean}对象的值作为基本类型的布尔值返回.
     *
     * @return  当前对象的基本{@code boolean}值.
     */
    public boolean booleanValue() {
        return value;
    }

    /**
     * 返回一个代表指定{@code boolean}值的{@code Boolean}实例.
     * 如果指定的{@code boolean}值为{@code true},那么该方法将返回{@code Boolean.TRUE};
     * 如果指定的{@code boolean}值为{@code false},那么该方法将返回{@code Boolean.FALSE};
     * 如果并不需要一个新的{@code Boolean}实例,那么应当优先使用该方法,而不是构造方法{@link #Boolean(boolean)},
     * 这可能会显著的提高空间和时间性能.
     *
     * @param  b 一个boolean值.
     * @return 代表{@code b}的{@code Boolean}实例.
     * @since  1.4
     */
    public static Boolean valueOf(boolean b) {
        return (b ? TRUE : FALSE);
    }

    /**
     * 返回一个指定string表示的{@code Boolean}实例.
     * 如果string参数不是{@code null}并且在忽略大小写时等于字符串{@code "true"},
     * 返回的{@code boolean}值为{@code true}.
     *
     * @param   s   一个字符串.
     * @return  该字符串表示的{@code Boolean值.
     */
    public static Boolean valueOf(String s) {
        return parseBoolean(s) ? TRUE : FALSE;
    }

    /**
     * 返回一个代表指定布尔值的{@code String}对象.
     * 如果给定的布尔值为{@code true},那么返回值为等于{@code "true"}的字符串,否则为等于{@code "false"}的字符串
     *
     * @param b 要转换的boolean
     * @return 代表指定的{@code boolean}的字符串
     * @since 1.4
     */
    public static String toString(boolean b) {
        return b ? "true" : "false";
    }

    /**
     * 返回表示该布尔值的{@code String}对象.
     * 如果给定的布尔值为{@code true},那么返回值为等于{@code "true"}的字符串,否则为等于{@code "false"}的字符串
     *
     * @return  代表该对象的字符串.
     */
    public String toString() {
        return value ? "true" : "false";
    }

    /**
     * 返回当前{@code Boolean}对象的hash code.
     *
     * @return  当前对象代表{@code true},返回整数{@code 1231};
     *          当前对象代表{@code false},返回整数{@code 1237};
     */
    @Override
    public int hashCode() {
        return Boolean.hashCode(value);
    }

    /**
     * 返回{@code boolean}值的hash code; 等同于{@code Boolean.hashCode()}.
     *
     * @param value 要hash的值
     * @return {@code boolean}值的hash code.
     * @since 1.8
     */
    public static int hashCode(boolean value) {
        return value ? 1231 : 1237;
    }

   /**
     * 当且仅当参数不为{@code null}并且是一个和该对象代表一样的{@code boolean}值的{@code Boolean}对象时,
     * 返回{@code true}
     *
     * @param   obj   要比较的对象.
     * @return  如果这些布尔对象表示相同的值,返回{@code true};否则返回{@code false}
     */
    public boolean equals(Object obj) {
        if (obj instanceof Boolean) {
            return value == ((Boolean)obj).booleanValue();
        }
        return false;
    }

    /**
     * 当且仅当存在以该参数命名的系统属性,并且等于字符串{@code "true"}的时候,返回{@code true}
     * (从Java<small><sup>TM</sup></small>平台1.0.2版本开始,这个字符串(译者注:指的是字符串{@code "true"})的测试不再区分大小写).
     * 系统属性可通过{@code getProperty}方法访问, 该方法由{@code System}类定义.
     * <p>
     * 如果没有以指定名称命名的属性或者指定名称为空或null,则返回{@code false}.
     *
     * @param   name   系统属性名称.
     * @return  系统属性的{@code boolean}值.
     * @throws  SecurityException 和{@link System#getProperty(String) System.getProperty}方法一样的原因
     * @see     java.lang.System#getProperty(java.lang.String)
     * @see     java.lang.System#getProperty(java.lang.String, java.lang.String)
     */
    public static boolean getBoolean(String name) {
        boolean result = false;
        try {
            result = parseBoolean(System.getProperty(name));
        } catch (IllegalArgumentException | NullPointerException e) {
        }
        return result;
    }

    /**
     * 将当前{@code Boolean}实例同另外一个进行比较.
     *
     * @param   b 要比较的{@code Boolean}实例
     * @return  如果当前对象跟参数是相同的布尔值,返回0;
     *          如果当前对象是true,参数是false,返回正数(译者注:1);
     *          如果当前对象是false,参数是true,返回负数(译者注:-1);
     * @throws  NullPointerException 如果参数为{@code null}
     * @see     Comparable
     * @since  1.5
     */
    public int compareTo(Boolean b) {
        return compare(this.value, b.value);
    }

    /**
     * 对比两个{@code boolean}值.
     * 返回值跟下面代码的返回值完全相同:
     * <pre>
     *    Boolean.valueOf(x).compareTo(Boolean.valueOf(y))
     * </pre>
     *
     * @param  x 第一个要比较的{@code boolean}值
     * @param  y 第二个要比较的{@code boolean}值
     * @return 如果{@code x == y},返回{@code 0};
     *         如果{@code !x && y},返回小于{@code 0}的值(译者注:-1);
     *         如果{@code x && !y},返回大于{@code 0}的值(译者注:1);
     * @since 1.7
     */
    public static int compare(boolean x, boolean y) {
        return (x == y) ? 0 : (x ? 1 : -1);
    }

    /**
     * 返回指定的两个{@code boolean}操作数的逻辑AND操作的结果.
     *
     * @param a 第一个操作数
     * @param b 第二个操作数
     * @return {@code a}和{@code b}的逻辑AND结果
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static boolean logicalAnd(boolean a, boolean b) {
        return a && b;
    }

    /**
     * 返回指定的两个{@code boolean}操作数的逻辑OR操作的结果.
     *
     * @param a 第一个操作数
     * @param b 第二个操作数
     * @return {@code a}和{@code b}的逻辑OR结果
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static boolean logicalOr(boolean a, boolean b) {
        return a || b;
    }

    /**
     *返回指定的两个{@code boolean}操作数的逻辑XOR操作的结果.
     *
     * @param a 第一个操作数
     * @param b 第二个操作数
     * @return  {@code a}和{@code b}的逻辑XOR结果
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static boolean logicalXor(boolean a, boolean b) {
        return a ^ b;
    }
}
