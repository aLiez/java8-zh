/*
 * Copyright (c) 1994, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * {@code Exception} 类及其子类是 {@code Throwable} 的一种形式,它指出了合理的应用程序想要捕获的条件.
 *
 * <p>{@code Exception}类以及它的任何不是{@link RuntimeException}子类的子类均为<em>受检查的异常</em>.
 * 检查的异常如果能够被方法或者构造函数抛出并在方法或构造函数之外传递,
 * 那么就需要在方法或者是构造函数的{@code throws}分句来声明.
 *
 * @author  Frank Yellin
 * @see     java.lang.Error
 * @jls 11.2 Compile-Time Checking of Exceptions
 * @since   JDK1.0
 */
public class Exception extends Throwable {
    static final long serialVersionUID = -3387516993124229948L;

    /**
     * 构造详细消息为 {@code null} 的新异常.
     * 原因尚未被初始化, 可在以后通过调用 {@link #initCause} 对其进行初始化.
     */
    public Exception() {
        super();
    }

    /**
     * 构造带指定详细消息的新异常.
     * 产生原因尚未被初始化, 可在以后通过调用 {@link #initCause} 对其进行初始化.
     *
     * @param   message   详细消息.保存详细消息, 以便以后通过 {@link #getMessage()} 方法获取它.
     */
    public Exception(String message) {
        super(message);
    }

    /**
     * 构造带指定详细消息和产生原因的新异常.
     * <p>注意, 与 {@code cause} 相关的详细消息<i>不是</i>自动合并到这个异常的详细消息中的.
     *
     * @param  message 详细消息 (保存详细消息, 以便以后通过 {@link #getMessage()} 方法获取它).
     * @param  cause 产生原因 (保存此原因, 以便以后通过 {@link #getCause()}方法获取它).
     *               (允许使用<tt>null</tt>值, 指出原因不存在或者是未知的.)
     * @since  1.4
     */
    public Exception(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构造一个带指定 产生原因 和 详细消息为{@code (cause==null ? null : cause.toString())}(它通常包含类和 {@code cause} 的详细消息)的新异常.
     * 对于那些与只比包装器多一点的异常(例如,{@link java.security.PrivilegedActionException}), 此构造方法很有用.
     *
     * @param  cause 产生原因 (保存此原因, 以便以后通过 {@link #getCause()}方法获取它).
     *               (允许使用<tt>null</tt>值, 指出原因不存在或者是未知的.)
     * @since  1.4
     */
    public Exception(Throwable cause) {
        super(cause);
    }

    /**
     * 使用指定的详细消息,产生原因,启用或禁用suppression,启用或禁用可写的堆栈跟踪来构造一个新的异常.
     *
     * @param  message 详细消息.
     * @param cause 产生原因 (保存此原因, 以便以后通过 {@link #getCause()}方法获取它).
     *               (允许使用<tt>null</tt>值, 指出原因不存在或者是未知的.)
     * @param enableSuppression 启用或禁用suppression.
     * @param writableStackTrace 启用或关闭可写的堆栈追踪.
     * @since 1.7
     */
    protected Exception(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
