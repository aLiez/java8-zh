/*
 * Copyright (c) 1994, 2012, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * {@code Object}类是类目录层次结构的基类.
 * {@code Object}类是所有类的父类(superclass).
 * 所有的类,包括arrays,均实现了这个类里面的方法.
 * @author  unascribed
 * @see     java.lang.Class
 * @since   JDK1.0
 */
public class Object {

    private static native void registerNatives();
    static {
        registerNatives();
    }

    /**
     * 返回当前{@code Object}的运行时类. 返回的对象是由所表示的类的
     * {@code static synchronized}方法锁定的对象.
     *
     * <p><b>实际的结果类型是 {@code Class<? extends |X|>}
     * 其中 {@code |X|} 是调用{@code getClass}的清除表达式的静态类型.</b>
     * 例如, 以下代码片段中就不需要进行强制转换:</p>
     *
     * <p>
     * {@code Number n = 0;}<br>
     * {@code Class<? extends Number> c = n.getClass(); }
     * </p>
     *
     * @return 表示此对象运行时类的{@code Class}对象.
     * @jls 15.8.2 Class Literals
     */
    public final native Class<?> getClass();

    /**
     * 返回该对象的hash code value. 这个方法是为了提高例如{@link java.util.HashMap}这样的散列表(hash tables)的性能
     *
     * <p>
     * {@code hashCode}的通常约定是:
     * <ul>
     * <li>在一个Java应用运行期间,只要一个对象的{@code equals}方法所用到的信息没有被修改,
     *     那么对这同一个对象调用多次{@code hashCode}方法,都必须返回同一个整数.
     *     在同一个应用程序的多次执行中,每次执行所返回的整数可以不一样.
     * <li>如果两个对象根据{@code equals(Object)}方法进行比较结果是相等的,那么调用这两个对象的
     *     {@code hashCode}方法必须返回同样的结果.
     * <li>如果两个对象根据{@link java.lang.Object#equals(java.lang.Object)}进行比较是不相等的,
     *     那么并<em>不</em>要求调用这两个对象的{@code hashCode}方法必须返回不同的整数结果.
     *     但是程序员应该了解到,给不相等的对象产生不同的整数结果,可能提高散列表(hash tables)的性能.
     * </ul>
     * <p>
     * 实际上, {@code Object}类定义的hashCode方法对不同的对象返回不同的整数.
     * (通常这是通过将该对象的内部地址转换成一个整数来实现的,但是Java&trade; 编程语言并不需要这种实现技巧)
     *
     * @return  该对象的hash code value.
     * @see     java.lang.Object#equals(java.lang.Object)
     * @see     java.lang.System#identityHashCode
     */
    public native int hashCode();

    /**
     * 指示其他某个对象是否"等于"此对象.
     * <p>
     * {@code equals}方法在非空对象引用上实现相等关系:
     * <ul>
     * <li><i>自反性</i>: 对于任何非空的引用值{@code x}, {@code x.equals(x)}应当返回{@code true}.
     * <li><i>对称性</i>: 对于任何非空的引用值{@code x}和 {@code y},
     * 当且仅当{@code y.equals(x)}返回 {@code true}时,{@code x.equals(y)}返回 {@code true}.
     * <li>It is <i>传递性</i>: 对于任何非空的引用值{@code x}、{@code y}和{@code z},
     * 如果{@code x.equals(y)}返回{@code true}并且{@code y.equals(z)}返回{@code true},
     * 那么 {@code x.equals(z)}也应该返回{@code true}.
     * <li><i>一致性</i>: 对于任何非空的引用值{@code x}和 {@code y},
     * 多次调用{@code x.equals(y)}应当始终返回{@code true}或始终返回{@code false},
     *     前提是对象上 {@code equals} 比较中所用的信息没有被修改.
     * <li>对于任何非空的引用值{@code x},{@code x.equals(null)}应当返回{@code false}.
     * </ul>
     * <p>
     * {@code Object}类的{@code equals}方法实现对象上可能性最大的相等关系;
     * 即,对于任何非空的引用值{@code x}和{@code y}, 当且仅当{@code x}和{@code y}引用同一个对象时,
     * 此方法才返回{@code true}({@code x == y} 具有值 {@code true}).
     * <p>
     * 需要注意的是,当该方法被重写时,通常也要重写{@code hashCode}方法,
     * 以维护{@code hashCode}方法的常规协定,该协定声明相等对象必须具有相等的哈希码.
     *
     * @param   obj   要比较的对象的引用.
     * @return  如果此对象与 obj 参数相同,则返回 {@code true};否则返回 {@code false}.
     * @see     #hashCode()
     * @see     java.util.HashMap
     */
    public boolean equals(Object obj) {
        return (this == obj);
    }

    /**
     * 创建并返回此对象的一个副本. "副本"的准确含义可能依赖于对象的类.这样做的目的是, 对于任何对象{@code x}, 表达式:
     * <blockquote>
     * <pre>
     * x.clone() != x</pre></blockquote>为{@code true},表达式:
     * <blockquote>
     * <pre>
     * x.clone().getClass() == x.getClass()</pre></blockquote>
     * 也为 {@code true},但这些并非必须条件.
     * 通常情况下是这样的:
     * <blockquote>
     * <pre>
     * x.clone().equals(x)</pre></blockquote>
     * 为{@code true}, 但这也不是一个必须条件.
     * <p>
     * 按照惯例, 返回的对象应该通过调用{@code super.clone}获得.
     * 如果一个类及其所有的父类({@code Object}除外)都遵守此约定,则{@code x.clone().getClass() == x.getClass()}.
     * <p>
     * 按照惯例, 此方法返回的对象应该独立于该对象(正被复制的对象).
     * 要获得此独立性, 在{@code super.clone}返回对象之前, 有必要对该对象的一个或多个字段进行修改.
     * 这通常意味着要复制包含正在被复制对象的内部"深层结构"的所有可变对象, 并使用对副本的引用替换对这些对象的引用.
     * 如果一个类只包含基本字段或对不变对象的引用, 那么通常不需要修改{@code super.clone}返回的对象中的字段.
     * <p>
     * {@code Object}类的{@code clone}方法执行特定的复制操作.
     * 首先, 如果此对象的类不能实现接口{@code Cloneable}, 则会抛出 {@code CloneNotSupportedException}.
     * 注意, 所有的数组都被视为实现了接口 {@code Cloneable},
     * 并且一个数组类型{@code T[]}的{@code clone}方法的返回类型也是{@code T[]}, 其中T为任何引用类型或者基本类型.
     * 否则, 此方法会创建此对象的类的一个新实例, 并像通过分配那样, 严格使用此对象相应字段的内容初始化该对象的所有字段;这些字段的内容没有被自我复制.
     * 所以, 此方法执行的是该对象的"浅表复制", 而不"深层复制"操作.
     * <p>
     * {@code Object}自身并没有实现{@code Cloneable}接口,所以在一个对象上调用 {@code clone}方法会导致在在运行时抛出一个异常.
     *
     * @return  此实例的一个副本.
     * @throws  CloneNotSupportedException  如果对象的类不支持 {@code Cloneable} 接口.子类重写{@code clone}方法也会抛出该异常,
     * 以指示无法复制某个实例.
     * @see java.lang.Cloneable
     */
    protected native Object clone() throws CloneNotSupportedException;

    /**
     * 返回该对象的字符串表示. 通常, {@code toString}方法会返回一个"以文本方式表示"此对象的字符串.
     * 结果应是一个简明但易于读懂的信息表达式.
     * 建议所有子类都重写此方法.
     * <p>
     * {@code Object}类的{@code toString}方法返回一个字符串, 该字符串由类名(对象是该类的一个实例)、
     * at标记符'{@code @}'以及此对象哈希码的无符号十六进制表示组成.
     * 换句话说,该方法返回一个字符串,它的值等于:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return  该对象的字符串表示形式.
     */
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

    /**
     * Wakes up a single thread that is waiting on this object's
     * monitor. If any threads are waiting on this object, one of them
     * is chosen to be awakened. The choice is arbitrary and occurs at
     * the discretion of the implementation. A thread waits on an object's
     * monitor by calling one of the {@code wait} methods.
     * <p>
     * The awakened thread will not be able to proceed until the current
     * thread relinquishes the lock on this object. The awakened thread will
     * compete in the usual manner with any other threads that might be
     * actively competing to synchronize on this object; for example, the
     * awakened thread enjoys no reliable privilege or disadvantage in being
     * the next thread to lock this object.
     * <p>
     * This method should only be called by a thread that is the owner
     * of this object's monitor. A thread becomes the owner of the
     * object's monitor in one of three ways:
     * <ul>
     * <li>By executing a synchronized instance method of that object.
     * <li>By executing the body of a {@code synchronized} statement
     *     that synchronizes on the object.
     * <li>For objects of type {@code Class,} by executing a
     *     synchronized static method of that class.
     * </ul>
     * <p>
     * Only one thread at a time can own an object's monitor.
     *
     * @throws  IllegalMonitorStateException  if the current thread is not
     *               the owner of this object's monitor.
     * @see        java.lang.Object#notifyAll()
     * @see        java.lang.Object#wait()
     */
    public final native void notify();

    /**
     * Wakes up all threads that are waiting on this object's monitor. A
     * thread waits on an object's monitor by calling one of the
     * {@code wait} methods.
     * <p>
     * The awakened threads will not be able to proceed until the current
     * thread relinquishes the lock on this object. The awakened threads
     * will compete in the usual manner with any other threads that might
     * be actively competing to synchronize on this object; for example,
     * the awakened threads enjoy no reliable privilege or disadvantage in
     * being the next thread to lock this object.
     * <p>
     * This method should only be called by a thread that is the owner
     * of this object's monitor. See the {@code notify} method for a
     * description of the ways in which a thread can become the owner of
     * a monitor.
     *
     * @throws  IllegalMonitorStateException  if the current thread is not
     *               the owner of this object's monitor.
     * @see        java.lang.Object#notify()
     * @see        java.lang.Object#wait()
     */
    public final native void notifyAll();

    /**
     * Causes the current thread to wait until either another thread invokes the
     * {@link java.lang.Object#notify()} method or the
     * {@link java.lang.Object#notifyAll()} method for this object, or a
     * specified amount of time has elapsed.
     * <p>
     * The current thread must own this object's monitor.
     * <p>
     * This method causes the current thread (call it <var>T</var>) to
     * place itself in the wait set for this object and then to relinquish
     * any and all synchronization claims on this object. Thread <var>T</var>
     * becomes disabled for thread scheduling purposes and lies dormant
     * until one of four things happens:
     * <ul>
     * <li>Some other thread invokes the {@code notify} method for this
     * object and thread <var>T</var> happens to be arbitrarily chosen as
     * the thread to be awakened.
     * <li>Some other thread invokes the {@code notifyAll} method for this
     * object.
     * <li>Some other thread {@linkplain Thread#interrupt() interrupts}
     * thread <var>T</var>.
     * <li>The specified amount of real time has elapsed, more or less.  If
     * {@code timeout} is zero, however, then real time is not taken into
     * consideration and the thread simply waits until notified.
     * </ul>
     * The thread <var>T</var> is then removed from the wait set for this
     * object and re-enabled for thread scheduling. It then competes in the
     * usual manner with other threads for the right to synchronize on the
     * object; once it has gained control of the object, all its
     * synchronization claims on the object are restored to the status quo
     * ante - that is, to the situation as of the time that the {@code wait}
     * method was invoked. Thread <var>T</var> then returns from the
     * invocation of the {@code wait} method. Thus, on return from the
     * {@code wait} method, the synchronization state of the object and of
     * thread {@code T} is exactly as it was when the {@code wait} method
     * was invoked.
     * <p>
     * A thread can also wake up without being notified, interrupted, or
     * timing out, a so-called <i>spurious wakeup</i>.  While this will rarely
     * occur in practice, applications must guard against it by testing for
     * the condition that should have caused the thread to be awakened, and
     * continuing to wait if the condition is not satisfied.  In other words,
     * waits should always occur in loops, like this one:
     * <pre>
     *     synchronized (obj) {
     *         while (&lt;condition does not hold&gt;)
     *             obj.wait(timeout);
     *         ... // Perform action appropriate to condition
     *     }
     * </pre>
     * (For more information on this topic, see Section 3.2.3 in Doug Lea's
     * "Concurrent Programming in Java (Second Edition)" (Addison-Wesley,
     * 2000), or Item 50 in Joshua Bloch's "Effective Java Programming
     * Language Guide" (Addison-Wesley, 2001).
     *
     * <p>If the current thread is {@linkplain java.lang.Thread#interrupt()
     * interrupted} by any thread before or while it is waiting, then an
     * {@code InterruptedException} is thrown.  This exception is not
     * thrown until the lock status of this object has been restored as
     * described above.
     *
     * <p>
     * Note that the {@code wait} method, as it places the current thread
     * into the wait set for this object, unlocks only this object; any
     * other objects on which the current thread may be synchronized remain
     * locked while the thread waits.
     * <p>
     * This method should only be called by a thread that is the owner
     * of this object's monitor. See the {@code notify} method for a
     * description of the ways in which a thread can become the owner of
     * a monitor.
     *
     * @param      timeout   the maximum time to wait in milliseconds.
     * @throws  IllegalArgumentException      if the value of timeout is
     *               negative.
     * @throws  IllegalMonitorStateException  if the current thread is not
     *               the owner of the object's monitor.
     * @throws  InterruptedException if any thread interrupted the
     *             current thread before or while the current thread
     *             was waiting for a notification.  The <i>interrupted
     *             status</i> of the current thread is cleared when
     *             this exception is thrown.
     * @see        java.lang.Object#notify()
     * @see        java.lang.Object#notifyAll()
     */
    public final native void wait(long timeout) throws InterruptedException;

    /**
     * Causes the current thread to wait until another thread invokes the
     * {@link java.lang.Object#notify()} method or the
     * {@link java.lang.Object#notifyAll()} method for this object, or
     * some other thread interrupts the current thread, or a certain
     * amount of real time has elapsed.
     * <p>
     * This method is similar to the {@code wait} method of one
     * argument, but it allows finer control over the amount of time to
     * wait for a notification before giving up. The amount of real time,
     * measured in nanoseconds, is given by:
     * <blockquote>
     * <pre>
     * 1000000*timeout+nanos</pre></blockquote>
     * <p>
     * In all other respects, this method does the same thing as the
     * method {@link #wait(long)} of one argument. In particular,
     * {@code wait(0, 0)} means the same thing as {@code wait(0)}.
     * <p>
     * The current thread must own this object's monitor. The thread
     * releases ownership of this monitor and waits until either of the
     * following two conditions has occurred:
     * <ul>
     * <li>Another thread notifies threads waiting on this object's monitor
     *     to wake up either through a call to the {@code notify} method
     *     or the {@code notifyAll} method.
     * <li>The timeout period, specified by {@code timeout}
     *     milliseconds plus {@code nanos} nanoseconds arguments, has
     *     elapsed.
     * </ul>
     * <p>
     * The thread then waits until it can re-obtain ownership of the
     * monitor and resumes execution.
     * <p>
     * As in the one argument version, interrupts and spurious wakeups are
     * possible, and this method should always be used in a loop:
     * <pre>
     *     synchronized (obj) {
     *         while (&lt;condition does not hold&gt;)
     *             obj.wait(timeout, nanos);
     *         ... // Perform action appropriate to condition
     *     }
     * </pre>
     * This method should only be called by a thread that is the owner
     * of this object's monitor. See the {@code notify} method for a
     * description of the ways in which a thread can become the owner of
     * a monitor.
     *
     * @param      timeout   the maximum time to wait in milliseconds.
     * @param      nanos      additional time, in nanoseconds range
     *                       0-999999.
     * @throws  IllegalArgumentException      if the value of timeout is
     *                      negative or the value of nanos is
     *                      not in the range 0-999999.
     * @throws  IllegalMonitorStateException  if the current thread is not
     *               the owner of this object's monitor.
     * @throws  InterruptedException if any thread interrupted the
     *             current thread before or while the current thread
     *             was waiting for a notification.  The <i>interrupted
     *             status</i> of the current thread is cleared when
     *             this exception is thrown.
     */
    public final void wait(long timeout, int nanos) throws InterruptedException {
        if (timeout < 0) {
            throw new IllegalArgumentException("timeout value is negative");
        }

        if (nanos < 0 || nanos > 999999) {
            throw new IllegalArgumentException(
                                "nanosecond timeout value out of range");
        }

        if (nanos > 0) {
            timeout++;
        }

        wait(timeout);
    }

    /**
     * 暂停当前线程,直到另外一个线程调用了该对象的{@link java.lang.Object#notify()}方法或者{@link java.lang.Object#notifyAll()}方法.
     * 换句话说, 该方法等同于调用{@code wait(0)}.
     * <p>
     * 当前线程必须拥有此对象的监视器.该线程释放对此监视器的所有权并等待,
     * 直到另外一个线程通过调用{@code notify}方法或者{@code notifyAll}方法来唤醒在此对象上的监视器上等待的线程.
     * <p>
     * 对于某一个参数的版本,实现中断和虚假唤醒是可能的,而且此方法应始终在循环中使用:
     * <pre>
     *     synchronized (obj) {
     *         while (&lt;condition does not hold&gt;)
     *             obj.wait();
     *         ... // Perform action appropriate to condition
     *     }
     * </pre>
     * 该方法只能由拥有该对象监视器的线程调用.
     * 有关线程能够成为监视器所有者的方法的描述, 请参阅 {@code notify}方法
     *
     * @throws  IllegalMonitorStateException   如果当前线程不是此对象监视器的所有者.
     * @throws  InterruptedException 如果在当前线程等待通知之前或者正在等待通知时,任何线程中断了当前线程.
     *                               在抛出此异常时,当前线程的<i>中断状态</i>被清除.
     * @see        java.lang.Object#notify()
     * @see        java.lang.Object#notifyAll()
     */
    public final void wait() throws InterruptedException {
        wait(0);
    }

    /**
     * Called by the garbage collector on an object when garbage collection
     * determines that there are no more references to the object.
     * A subclass overrides the {@code finalize} method to dispose of
     * system resources or to perform other cleanup.
     * <p>
     * The general contract of {@code finalize} is that it is invoked
     * if and when the Java&trade; virtual
     * machine has determined that there is no longer any
     * means by which this object can be accessed by any thread that has
     * not yet died, except as a result of an action taken by the
     * finalization of some other object or class which is ready to be
     * finalized. The {@code finalize} method may take any action, including
     * making this object available again to other threads; the usual purpose
     * of {@code finalize}, however, is to perform cleanup actions before
     * the object is irrevocably discarded. For example, the finalize method
     * for an object that represents an input/output connection might perform
     * explicit I/O transactions to break the connection before the object is
     * permanently discarded.
     * <p>
     * The {@code finalize} method of class {@code Object} performs no
     * special action; it simply returns normally. Subclasses of
     * {@code Object} may override this definition.
     * <p>
     * The Java programming language does not guarantee which thread will
     * invoke the {@code finalize} method for any given object. It is
     * guaranteed, however, that the thread that invokes finalize will not
     * be holding any user-visible synchronization locks when finalize is
     * invoked. If an uncaught exception is thrown by the finalize method,
     * the exception is ignored and finalization of that object terminates.
     * <p>
     * After the {@code finalize} method has been invoked for an object, no
     * further action is taken until the Java virtual machine has again
     * determined that there is no longer any means by which this object can
     * be accessed by any thread that has not yet died, including possible
     * actions by other objects or classes which are ready to be finalized,
     * at which point the object may be discarded.
     * <p>
     * The {@code finalize} method is never invoked more than once by a Java
     * virtual machine for any given object.
     * <p>
     * Any exception thrown by the {@code finalize} method causes
     * the finalization of this object to be halted, but is otherwise
     * ignored.
     *
     * @throws Throwable the {@code Exception} raised by this method
     * @see java.lang.ref.WeakReference
     * @see java.lang.ref.PhantomReference
     * @jls 12.6 Finalization of Class Instances
     */
    protected void finalize() throws Throwable { }
}
