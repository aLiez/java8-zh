/*
 * Copyright (c) 2002, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Locale;

/**
 * {@code Character}是将基本类型{@code char}包装成了一个对象.
 * 一个{@code Character}类型的对象只包含一个类型为{@code char}的字段.
 * <p>
 * 另外, 该类提供了多种判断一个字符的类型(小写字母, 数字, 等等)以及将字符从大写转为小写等的方法, 
 * <p>
 * 字符信息基于Unicode标准, 版本6.2.0.
 * <p>
 * {@code Character}类中的方法和数据是通过<i>UnicodeData</i>文件中的信息定义的, 
 * 该文件是由Unicode Consortium 维护的Unicode Character Database的一部分.
 * 该文件指明了包括每个已定义Unicode代码点(code point)或字符范围的名称和常规类别的各种属性.
 * <p>
 * 该文件和它的描述信息可以从Unicode Consortium获得, 地址如下:
 * <ul>
 * <li><a href="http://www.unicode.org">http://www.unicode.org</a>
 * </ul>
 *
 *
 *
 * <h3><a name="unicode">Unicode字符表示形式</a></h3>
 *
 * <p>{@code char}数据类型(以及{@code Character}对象封装的值)都是基于原始的Unicode规范,
 * 该规范将字符定义为固定宽度的16位实体.Unicode标准曾做过修改, 以便支持表示形式需要超过16位的字符.
 * 合法的<em>代码点(code point)</em>范围目前是从U+0000 to U+10FFFF, 被称作为<em>Unicode标量值(scalar value)</em>
 * (参考Unicode标准中U+<i>n</i>表示法的<a href="http://www.unicode.org/reports/tr27/#notation"><i>定义</i></a>).
 *
 *
 * <p><p><a name="BMP">从U+0000 to U+FFFF</a>的字符集有时也被称为<em>基本多文种平面(BMP--Basic Multilingual Plane)</em>.
 * 代码大于U+FFFF的<a name="supplementary">Characters</a>被称为<em>增补字符(supplementary characters)</em>.
 * Java平台在{@code char}数组、{@code String}还有 {@code StringBuffer}中采用UTF-16的表示形式.
 * 第一项来自<em>高代理(high-surrogates)</em>范围, 即(&#92;uD800-&#92;uDBFF),
 * 第二项来自<em>低代理(low-surrogates)</em>范围, 即(&#92;uDC00-&#92;uDFFF).
 *
 * <p>因此, {@code char}值表示基本多文种平面代码点, 包括代理代码点, 以及UTF-16编码的代码单元.
 * {@code int}值表示了所有的Unicode代码点(Unicode code points), 包括了增补代码点.
 * {@code int}的低位的21位(最低有效位)用来表示Unicode代码, 高位的11位(最高有效位)必须为0.
 * 除非另有指定, 否则与增补字符和代理{@code char}值的行为必须遵守如下内容:
 *     　　
 *
 * <ul>
 * <li>只接受一个{@code char}值的方法无法支持增补字符.
 * 它们将代理项字符范围内的{@code char}值视为未定义字符
 * 例如, {@code Character.isLetter('\u005CuD840')}返回{@code false}, 
 * 即便是特定的值, 如果在字符串的后面跟随了任何低代理项的值, 那么它将表示一个字母．
 *
 * <li>接收一个{@code int}值的方法支持所有的Unicode字符集, 包括增补字符集.
 * 例如, {@code Character.isLetter(0x2F81A)}返回{@code true},
 * 因为该代码点值表示一个字母(一个 CJK 象形文字).
 *
 * </ul>
 *
 * <p>在Java SE API文档中,<em>Unicode代码点</em>用于范围在U+0000与U+10FFFF之间的字符值,
 * <em>Unicode代码单元</em>用于作为<em>UTF-16</em>编码的代码单元的16位{@code char}值.
 * 更多关于Unicode技术的信息,请参考<a href="http://www.unicode.org/glossary/">Unicode Glossary</a>.
 *
 * <p>(译者注:Unicode代码点 即 字符,Unicode代码单元 即 字符值)
 *
 * @author  Lee Boynton
 * @author  Guy Steele
 * @author  Akira Tanaka
 * @author  Martin Buchholz
 * @author  Ulf Zibis
 * @since   1.0
 */
public final
class Character implements java.io.Serializable, Comparable<Character> {
    /**
     * 同字符串进行转换时可用的最小的基数.
     * 该常量字段是诸如{@code digit}、{@code forDigit}、{@code Integer}类中的{@code toString}等基数转换方法中
     * 被允许的最小的基数参数.
     *
     * @see     Character#digit(char, int)
     * @see     Character#forDigit(int, int)
     * @see     Integer#toString(int, int)
     * @see     Integer#valueOf(String)
     */
    public static final int MIN_RADIX = 2;

    /**
     * 同字符串进行转换时可用的最大的基数.
     * 该常量字段是诸如{@code digit}、{@code forDigit}、{@code Integer}类中的{@code toString}等基数转换方法中
     * 被允许的最大的基数参数.
     *
     * @see     Character#digit(char, int)
     * @see     Character#forDigit(int, int)
     * @see     Integer#toString(int, int)
     * @see     Integer#valueOf(String)
     */
    public static final int MAX_RADIX = 36;

    /**
     * 该常量字段是{@code char}的最小值, 即{@code '\u005Cu0000'}.
     *
     * @since   1.0.2
     */
    public static final char MIN_VALUE = '\u0000';

    /**
     * 该常量字段是{@code char}的最大值, 即{@code '\u005CuFFFF'}.
     *
     * @since   1.0.2
     */
    public static final char MAX_VALUE = '\uFFFF';

    /**
     * 代表基本类型{@code char}的{@code Class}实例.
     *
     * @since   1.1
     */
    @SuppressWarnings("unchecked")
    public static final Class<Character> TYPE = (Class<Character>) Class.getPrimitiveClass("char");

    /*
     * Normative general types
     */

    /*
     * General character types
     */

    /**
     * Unicode规范中的常规类别 "Cn".
     * @since   1.1
     */
    public static final byte UNASSIGNED = 0;

    /**
     * Unicode规范中的常规类别 "Lu".
     * @since   1.1
     */
    public static final byte UPPERCASE_LETTER = 1;

    /**
     * Unicode规范中的常规类别 "Ll".
     * @since   1.1
     */
    public static final byte LOWERCASE_LETTER = 2;

    /**
     * Unicode规范中的常规类别 "Lt".
     * @since   1.1
     */
    public static final byte TITLECASE_LETTER = 3;

    /**
     * Unicode规范中的常规类别 "Lm".
     * @since   1.1
     */
    public static final byte MODIFIER_LETTER = 4;

    /**
     * Unicode规范中的常规类别 "Lo".
     * @since   1.1
     */
    public static final byte OTHER_LETTER = 5;

    /**
     * Unicode规范中的常规类别 "Mn".
     * @since   1.1
     */
    public static final byte NON_SPACING_MARK = 6;

    /**
     * Unicode规范中的常规类别 "Me".
     * @since   1.1
     */
    public static final byte ENCLOSING_MARK = 7;

    /**
     * Unicode规范中的常规类别 "Mc".
     * @since   1.1
     */
    public static final byte COMBINING_SPACING_MARK = 8;

    /**
     * Unicode规范中的常规类别 "Nd".
     * @since   1.1
     */
    public static final byte DECIMAL_DIGIT_NUMBER        = 9;

    /**
     * Unicode规范中的常规类别 "Nl".
     * @since   1.1
     */
    public static final byte LETTER_NUMBER = 10;

    /**
     * Unicode规范中的常规类别 "No".
     * @since   1.1
     */
    public static final byte OTHER_NUMBER = 11;

    /**
     * Unicode规范中的常规类别 "Zs".
     * @since   1.1
     */
    public static final byte SPACE_SEPARATOR = 12;

    /**
     * Unicode规范中的常规类别 "Zl".
     * @since   1.1
     */
    public static final byte LINE_SEPARATOR = 13;

    /**
     * Unicode规范中的常规类别 "Zp".
     * @since   1.1
     */
    public static final byte PARAGRAPH_SEPARATOR = 14;

    /**
     * Unicode规范中的常规类别 "Cc".
     * @since   1.1
     */
    public static final byte CONTROL = 15;

    /**
     * Unicode规范中的常规类别 "Cf".
     * @since   1.1
     */
    public static final byte FORMAT = 16;

    /**
     * Unicode规范中的常规类别 "Co".
     * @since   1.1
     */
    public static final byte PRIVATE_USE = 18;

    /**
     * Unicode规范中的常规类别 "Cs".
     * @since   1.1
     */
    public static final byte SURROGATE = 19;

    /**
     * Unicode规范中的常规类别 "Pd".
     * @since   1.1
     */
    public static final byte DASH_PUNCTUATION = 20;

    /**
     * Unicode规范中的常规类别 "Ps".
     * @since   1.1
     */
    public static final byte START_PUNCTUATION = 21;

    /**
     * Unicode规范中的常规类别 "Pe".
     * @since   1.1
     */
    public static final byte END_PUNCTUATION = 22;

    /**
     * Unicode规范中的常规类别 "Pc".
     * @since   1.1
     */
    public static final byte CONNECTOR_PUNCTUATION = 23;

    /**
     * Unicode规范中的常规类别 "Po".
     * @since   1.1
     */
    public static final byte OTHER_PUNCTUATION = 24;

    /**
     * Unicode规范中的常规类别 "Sm".
     * @since   1.1
     */
    public static final byte MATH_SYMBOL = 25;

    /**
     * Unicode规范中的常规类别 "Sc".
     * @since   1.1
     */
    public static final byte CURRENCY_SYMBOL = 26;

    /**
     * Unicode规范中的常规类别 "Sk".
     * @since   1.1
     */
    public static final byte MODIFIER_SYMBOL = 27;

    /**
     * Unicode规范中的常规类别 "So".
     * @since   1.1
     */
    public static final byte OTHER_SYMBOL = 28;

    /**
     * Unicode规范中的常规类别 "Pi".
     * @since   1.4
     */
    public static final byte INITIAL_QUOTE_PUNCTUATION = 29;

    /**
     * Unicode规范中的常规类别 "Pf".
     * @since   1.4
     */
    public static final byte FINAL_QUOTE_PUNCTUATION = 30;

    /**
     * 错误标识. 采用int(代码点)以避免和 U+FFFF 混淆.
     */
    static final int ERROR = 0xFFFFFFFF;


    /**
     * 未定义的双向字符类型. 未定义的{@code char}值在Unicode规范中有未定义的方向性.
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_UNDEFINED = -1;

    /**
     * Unicode规范中的强双向字符类型"L".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_LEFT_TO_RIGHT = 0;

    /**
     * Unicode规范中的强双向字符类型"R".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_RIGHT_TO_LEFT = 1;

    /**
     * Unicode规范中的强双向字符类型"AL".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC = 2;

    /**
     * Unicode规范中的弱双向字符类型"EN".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_EUROPEAN_NUMBER = 3;

    /**
     * Unicode规范中的弱双向字符类型"ES".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR = 4;

    /**
     * Unicode规范中的弱双向字符类型"ET".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR = 5;

    /**
     * Unicode规范中的弱双向字符类型"AN".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_ARABIC_NUMBER = 6;

    /**
     * Unicode规范中的弱双向字符类型"CS".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_COMMON_NUMBER_SEPARATOR = 7;

    /**
     * Unicode规范中的弱双向字符类型"NSM".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_NONSPACING_MARK = 8;

    /**
     * Unicode规范中的弱双向字符类型"BN".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_BOUNDARY_NEUTRAL = 9;

    /**
     * Unicode规范中的中性双向字符类型"B".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_PARAGRAPH_SEPARATOR = 10;

    /**
     * Unicode规范中的中性双向字符类型"S".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_SEGMENT_SEPARATOR = 11;

    /**
     * Unicode规范中的中性双向字符类型"WS".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_WHITESPACE = 12;

    /**
     * Unicode规范中的中性双向字符类型"ON".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_OTHER_NEUTRALS = 13;

    /**
     * Unicode规范中的强双向字符类型"LRE".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING = 14;

    /**
     * Unicode规范中的强双向字符类型"LRO".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE = 15;

    /**
     * Unicode规范中的强双向字符类型"RLE".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING = 16;

    /**
     * Unicode规范中的强双向字符类型"RLO".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE = 17;

    /**
     * Unicode规范中的弱双向字符类型"PDF".
     * @since 1.4
     */
    public static final byte DIRECTIONALITY_POP_DIRECTIONAL_FORMAT = 18;

    /**
     * UTF-16编码中<a href="http://www.unicode.org/glossary/#high_surrogate_code_unit">Unicode高代理项代码单元</a>的最小值,
     * 即常量{@code '\u005CuD800'}.
     * 高代理项也称为<i>前导代理项</i>.
     *
     * @since 1.5
     */
    public static final char MIN_HIGH_SURROGATE = '\uD800';

    /**
     * UTF-16编码中<a href="http://www.unicode.org/glossary/#high_surrogate_code_unit">Unicode高代理项代码单元</a>的最大值,
     * 即常量{@code '\u005CuDBFF'}.
     * 高代理项也称为<i>前导代理项</i>.
     *
     * @since 1.5
     */
    public static final char MAX_HIGH_SURROGATE = '\uDBFF';

    /**
     * UTF-16编码中<a href="http://www.unicode.org/glossary/#high_surrogate_code_unit">Unicode低代理项代码单元</a>的最小值,
     * 即常量{@code '\u005CuDC00'}.
     * 低代理项也称为<i>尾部代理项</i>.
     *
     * @since 1.5
     */
    public static final char MIN_LOW_SURROGATE  = '\uDC00';

    /**
     * UTF-16编码中<a href="http://www.unicode.org/glossary/#high_surrogate_code_unit">Unicode低代理项代码单元</a>的最大值,
     * 即常量{@code '\u005CuDFFF'}.
     * 低代理项也称为<i>尾部代理项</i>.
     *
     * @since 1.5
     */
    public static final char MAX_LOW_SURROGATE  = '\uDFFF';

    /**
     * UTF-16编码中的Unicode代理项代码单元的最小值,即常量 {@code '\u005CuD800'}.
     *
     * @since 1.5
     */
    public static final char MIN_SURROGATE = MIN_HIGH_SURROGATE;

    /**
     * UTF-16编码中的Unicode代理项代码单元的最大值,即常量 {@code '\u005CuDFFF'}.
     *
     * @since 1.5
     */
    public static final char MAX_SURROGATE = MAX_LOW_SURROGATE;

    /**
     * <a href="http://www.unicode.org/glossary/#supplementary_code_point">
     * Unicode增补代码点</a>的最小值, 即常量{@code U+10000}.
     *
     * @since 1.5
     */
    public static final int MIN_SUPPLEMENTARY_CODE_POINT = 0x010000;

    /**
     * <a href="http://www.unicode.org/glossary/#supplementary_code_point">
     * Unicode代码点</a>的最小值, 即常量{@code U+0000}.
     *
     * @since 1.5
     */
    public static final int MIN_CODE_POINT = 0x000000;

    /**
     * <a href="http://www.unicode.org/glossary/#supplementary_code_point">
     * Unicode代码点</a>的最大值, 即常量{@code U+10FFFF}.
     *
     * @since 1.5
     */
    public static final int MAX_CODE_POINT = 0X10FFFF;


    /**
     * 该类的实例表示Unicode字符集的特定子集.
     * {@code Character}类中定义的唯一子集系列是{@link Character.UnicodeBlock}.
     * Java API 的其他部分可以针对其各自用途定义其它子集.
     *
     * @since 1.2
     */
    public static class Subset  {

        private String name;

        /**
         * 构造一个新的{@code Subset}实例.
         *
         * @param  name  子集名称
         * @exception NullPointerException name为{@code null}
         */
        protected Subset(String name) {
            if (name == null) {
                throw new NullPointerException("name");
            }
            this.name = name;
        }

        /**
         * 比较两个{@code Subset}对象的相等性.
         * 当且仅当{@code this}和该参数引用同一个对象时,返回{@code true};
         * 由于该类是 {@code final}, 这保证了保持所有子类.
         */
        public final boolean equals(Object obj) {
            return (this == obj);
        }

        /**
         * 返回{@link Object#hashCode}方法定义的标准hash code.
         * 为了确保{@code equals}和{@code hashCode}方法在所有子类中保持一致, 该方法设置为final.
         */
        public final int hashCode() {
            return super.hashCode();
        }

        /**
         * 返回该子集的名称.
         */
        public final String toString() {
            return name;
        }
    }

    // See http://www.unicode.org/Public/UNIDATA/Blocks.txt
    // for the latest specification of Unicode Blocks.

    /**
     * 表示 Unicode 规范中字符块的一系列字符子集.
     * 字符块通常定义用于特定脚本或目的的字符.
     * 一个字符至多可以包含在一个 Unicode 块中.
     *
     * @since 1.2
     */
    public static final class UnicodeBlock extends Subset {

        private static Map<String, UnicodeBlock> map = new HashMap<>(256);

        /**
         * 使用指定的标识名称创建一个UnicodeBlock.
         * 该名称必须和块标识符一致.
         */
        private UnicodeBlock(String idName) {
            super(idName);
            map.put(idName, this);
        }

        /**
         * 使用指定的标识名称和别名创建一个UnicodeBlock.
         */
        private UnicodeBlock(String idName, String alias) {
            this(idName);
            map.put(alias, this);
        }

        /**
         * 使用指定的标识名称和别名创建一个UnicodeBlock.
         */
        private UnicodeBlock(String idName, String... aliases) {
            this(idName);
            for (String alias : aliases)
                map.put(alias, this);
        }

        /**
         * "Basic Latin" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock  BASIC_LATIN =
            new UnicodeBlock("BASIC_LATIN", 
                             "BASIC LATIN", 
                             "BASICLATIN");

        /**
         * "Latin-1 Supplement" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock LATIN_1_SUPPLEMENT =
            new UnicodeBlock("LATIN_1_SUPPLEMENT", 
                             "LATIN-1 SUPPLEMENT", 
                             "LATIN-1SUPPLEMENT");

        /**
         * "Latin Extended-A" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock LATIN_EXTENDED_A =
            new UnicodeBlock("LATIN_EXTENDED_A", 
                             "LATIN EXTENDED-A", 
                             "LATINEXTENDED-A");

        /**
         * "Latin Extended-B" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock LATIN_EXTENDED_B =
            new UnicodeBlock("LATIN_EXTENDED_B", 
                             "LATIN EXTENDED-B", 
                             "LATINEXTENDED-B");

        /**
         * "IPA Extensions" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock IPA_EXTENSIONS =
            new UnicodeBlock("IPA_EXTENSIONS", 
                             "IPA EXTENSIONS", 
                             "IPAEXTENSIONS");

        /**
         * "Spacing Modifier Letters" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock SPACING_MODIFIER_LETTERS =
            new UnicodeBlock("SPACING_MODIFIER_LETTERS", 
                             "SPACING MODIFIER LETTERS", 
                             "SPACINGMODIFIERLETTERS");

        /**
         * "Combining Diacritical Marks" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock COMBINING_DIACRITICAL_MARKS =
            new UnicodeBlock("COMBINING_DIACRITICAL_MARKS", 
                             "COMBINING DIACRITICAL MARKS", 
                             "COMBININGDIACRITICALMARKS");

        /**
         * "Greek and Coptic" Unicode字符块的常量.
         * <p>
         * 该字符块以前被称做"Greek"字符块.
         *
         * @since 1.2
         */
        public static final UnicodeBlock GREEK =
            new UnicodeBlock("GREEK", 
                             "GREEK AND COPTIC", 
                             "GREEKANDCOPTIC");

        /**
         * "Cyrillic" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CYRILLIC =
            new UnicodeBlock("CYRILLIC");

        /**
         * "Armenian" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ARMENIAN =
            new UnicodeBlock("ARMENIAN");

        /**
         * "Hebrew" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock HEBREW =
            new UnicodeBlock("HEBREW");

        /**
         * "Arabic" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ARABIC =
            new UnicodeBlock("ARABIC");

        /**
         * "Devanagari" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock DEVANAGARI =
            new UnicodeBlock("DEVANAGARI");

        /**
         * "Bengali" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock BENGALI =
            new UnicodeBlock("BENGALI");

        /**
         * "Gurmukhi" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock GURMUKHI =
            new UnicodeBlock("GURMUKHI");

        /**
         * "Gujarati" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock GUJARATI =
            new UnicodeBlock("GUJARATI");

        /**
         * "Oriya" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ORIYA =
            new UnicodeBlock("ORIYA");

        /**
         * "Tamil" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock TAMIL =
            new UnicodeBlock("TAMIL");

        /**
         * "Telugu" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock TELUGU =
            new UnicodeBlock("TELUGU");

        /**
         * "Kannada" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock KANNADA =
            new UnicodeBlock("KANNADA");

        /**
         * "Malayalam" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock MALAYALAM =
            new UnicodeBlock("MALAYALAM");

        /**
         * "Thai" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock THAI =
            new UnicodeBlock("THAI");

        /**
         * "Lao" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock LAO =
            new UnicodeBlock("LAO");

        /**
         * "Tibetan" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock TIBETAN =
            new UnicodeBlock("TIBETAN");

        /**
         * "Georgian" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock GEORGIAN =
            new UnicodeBlock("GEORGIAN");

        /**
         * "Hangul Jamo" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock HANGUL_JAMO =
            new UnicodeBlock("HANGUL_JAMO", 
                             "HANGUL JAMO", 
                             "HANGULJAMO");

        /**
         * "Latin Extended Additional" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock LATIN_EXTENDED_ADDITIONAL =
            new UnicodeBlock("LATIN_EXTENDED_ADDITIONAL", 
                             "LATIN EXTENDED ADDITIONAL", 
                             "LATINEXTENDEDADDITIONAL");

        /**
         * "Greek Extended" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock GREEK_EXTENDED =
            new UnicodeBlock("GREEK_EXTENDED", 
                             "GREEK EXTENDED", 
                             "GREEKEXTENDED");

        /**
         * "General Punctuation" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock GENERAL_PUNCTUATION =
            new UnicodeBlock("GENERAL_PUNCTUATION", 
                             "GENERAL PUNCTUATION", 
                             "GENERALPUNCTUATION");

        /**
         * "Superscripts and Subscripts" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock SUPERSCRIPTS_AND_SUBSCRIPTS =
            new UnicodeBlock("SUPERSCRIPTS_AND_SUBSCRIPTS", 
                             "SUPERSCRIPTS AND SUBSCRIPTS", 
                             "SUPERSCRIPTSANDSUBSCRIPTS");

        /**
         * "Currency Symbols" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CURRENCY_SYMBOLS =
            new UnicodeBlock("CURRENCY_SYMBOLS", 
                             "CURRENCY SYMBOLS", 
                             "CURRENCYSYMBOLS");

        /**
         * "Combining Diacritical Marks for Symbols" Unicode字符块的常量.
         * <p>
         * 该字符块以前被称为 "Combining Marks for Symbols".
         * @since 1.2
         */
        public static final UnicodeBlock COMBINING_MARKS_FOR_SYMBOLS =
            new UnicodeBlock("COMBINING_MARKS_FOR_SYMBOLS", 
                             "COMBINING DIACRITICAL MARKS FOR SYMBOLS", 
                             "COMBININGDIACRITICALMARKSFORSYMBOLS", 
                             "COMBINING MARKS FOR SYMBOLS", 
                             "COMBININGMARKSFORSYMBOLS");

        /**
         * "Letterlike Symbols" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock LETTERLIKE_SYMBOLS =
            new UnicodeBlock("LETTERLIKE_SYMBOLS", 
                             "LETTERLIKE SYMBOLS", 
                             "LETTERLIKESYMBOLS");

        /**
         * "Number Forms" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock NUMBER_FORMS =
            new UnicodeBlock("NUMBER_FORMS", 
                             "NUMBER FORMS", 
                             "NUMBERFORMS");

        /**
         * "Arrows" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ARROWS =
            new UnicodeBlock("ARROWS");

        /**
         * "Mathematical Operators" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock MATHEMATICAL_OPERATORS =
            new UnicodeBlock("MATHEMATICAL_OPERATORS", 
                             "MATHEMATICAL OPERATORS", 
                             "MATHEMATICALOPERATORS");

        /**
         * "Miscellaneous Technical" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock MISCELLANEOUS_TECHNICAL =
            new UnicodeBlock("MISCELLANEOUS_TECHNICAL", 
                             "MISCELLANEOUS TECHNICAL", 
                             "MISCELLANEOUSTECHNICAL");

        /**
         * "Control Pictures" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CONTROL_PICTURES =
            new UnicodeBlock("CONTROL_PICTURES", 
                             "CONTROL PICTURES", 
                             "CONTROLPICTURES");

        /**
         * "Optical Character Recognition" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock OPTICAL_CHARACTER_RECOGNITION =
            new UnicodeBlock("OPTICAL_CHARACTER_RECOGNITION", 
                             "OPTICAL CHARACTER RECOGNITION", 
                             "OPTICALCHARACTERRECOGNITION");

        /**
         * "Enclosed Alphanumerics" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ENCLOSED_ALPHANUMERICS =
            new UnicodeBlock("ENCLOSED_ALPHANUMERICS", 
                             "ENCLOSED ALPHANUMERICS", 
                             "ENCLOSEDALPHANUMERICS");

        /**
         * "Box Drawing" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock BOX_DRAWING =
            new UnicodeBlock("BOX_DRAWING", 
                             "BOX DRAWING", 
                             "BOXDRAWING");

        /**
         * "Block Elements" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock BLOCK_ELEMENTS =
            new UnicodeBlock("BLOCK_ELEMENTS", 
                             "BLOCK ELEMENTS", 
                             "BLOCKELEMENTS");

        /**
         * "Geometric Shapes" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock GEOMETRIC_SHAPES =
            new UnicodeBlock("GEOMETRIC_SHAPES", 
                             "GEOMETRIC SHAPES", 
                             "GEOMETRICSHAPES");

        /**
         * "Miscellaneous Symbols" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock MISCELLANEOUS_SYMBOLS =
            new UnicodeBlock("MISCELLANEOUS_SYMBOLS", 
                             "MISCELLANEOUS SYMBOLS", 
                             "MISCELLANEOUSSYMBOLS");

        /**
         * "Dingbats" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock DINGBATS =
            new UnicodeBlock("DINGBATS");

        /**
         * "CJK Symbols and Punctuation" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CJK_SYMBOLS_AND_PUNCTUATION =
            new UnicodeBlock("CJK_SYMBOLS_AND_PUNCTUATION", 
                             "CJK SYMBOLS AND PUNCTUATION", 
                             "CJKSYMBOLSANDPUNCTUATION");

        /**
         * "Hiragana" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock HIRAGANA =
            new UnicodeBlock("HIRAGANA");

        /**
         * "Katakana" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock KATAKANA =
            new UnicodeBlock("KATAKANA");

        /**
         * "Bopomofo" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock BOPOMOFO =
            new UnicodeBlock("BOPOMOFO");

        /**
         * "Hangul Compatibility Jamo" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock HANGUL_COMPATIBILITY_JAMO =
            new UnicodeBlock("HANGUL_COMPATIBILITY_JAMO", 
                             "HANGUL COMPATIBILITY JAMO", 
                             "HANGULCOMPATIBILITYJAMO");

        /**
         * "Kanbun" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock KANBUN =
            new UnicodeBlock("KANBUN");

        /**
         * "Enclosed CJK Letters and Months" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ENCLOSED_CJK_LETTERS_AND_MONTHS =
            new UnicodeBlock("ENCLOSED_CJK_LETTERS_AND_MONTHS", 
                             "ENCLOSED CJK LETTERS AND MONTHS", 
                             "ENCLOSEDCJKLETTERSANDMONTHS");

        /**
         * "CJK Compatibility" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CJK_COMPATIBILITY =
            new UnicodeBlock("CJK_COMPATIBILITY", 
                             "CJK COMPATIBILITY", 
                             "CJKCOMPATIBILITY");

        /**
         * "CJK Unified Ideographs" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CJK_UNIFIED_IDEOGRAPHS =
            new UnicodeBlock("CJK_UNIFIED_IDEOGRAPHS", 
                             "CJK UNIFIED IDEOGRAPHS", 
                             "CJKUNIFIEDIDEOGRAPHS");

        /**
         * "Hangul Syllables" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock HANGUL_SYLLABLES =
            new UnicodeBlock("HANGUL_SYLLABLES", 
                             "HANGUL SYLLABLES", 
                             "HANGULSYLLABLES");

        /**
         * "Private Use Area" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock PRIVATE_USE_AREA =
            new UnicodeBlock("PRIVATE_USE_AREA", 
                             "PRIVATE USE AREA", 
                             "PRIVATEUSEAREA");

        /**
         * "CJK Compatibility Ideographs"  Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CJK_COMPATIBILITY_IDEOGRAPHS =
            new UnicodeBlock("CJK_COMPATIBILITY_IDEOGRAPHS", 
                             "CJK COMPATIBILITY IDEOGRAPHS", 
                             "CJKCOMPATIBILITYIDEOGRAPHS");

        /**
         * "Alphabetic Presentation Forms" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ALPHABETIC_PRESENTATION_FORMS =
            new UnicodeBlock("ALPHABETIC_PRESENTATION_FORMS", 
                             "ALPHABETIC PRESENTATION FORMS", 
                             "ALPHABETICPRESENTATIONFORMS");

        /**
         * "Arabic Presentation Forms-A"  Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ARABIC_PRESENTATION_FORMS_A =
            new UnicodeBlock("ARABIC_PRESENTATION_FORMS_A", 
                             "ARABIC PRESENTATION FORMS-A", 
                             "ARABICPRESENTATIONFORMS-A");

        /**
         * "Combining Half Marks" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock COMBINING_HALF_MARKS =
            new UnicodeBlock("COMBINING_HALF_MARKS", 
                             "COMBINING HALF MARKS", 
                             "COMBININGHALFMARKS");

        /**
         * "CJK Compatibility Forms" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock CJK_COMPATIBILITY_FORMS =
            new UnicodeBlock("CJK_COMPATIBILITY_FORMS", 
                             "CJK COMPATIBILITY FORMS", 
                             "CJKCOMPATIBILITYFORMS");

        /**
         * "Small Form Variants" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock SMALL_FORM_VARIANTS =
            new UnicodeBlock("SMALL_FORM_VARIANTS", 
                             "SMALL FORM VARIANTS", 
                             "SMALLFORMVARIANTS");

        /**
         * "Arabic Presentation Forms-B" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock ARABIC_PRESENTATION_FORMS_B =
            new UnicodeBlock("ARABIC_PRESENTATION_FORMS_B", 
                             "ARABIC PRESENTATION FORMS-B", 
                             "ARABICPRESENTATIONFORMS-B");

        /**
         * "Halfwidth and Fullwidth Forms"  Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock HALFWIDTH_AND_FULLWIDTH_FORMS =
            new UnicodeBlock("HALFWIDTH_AND_FULLWIDTH_FORMS", 
                             "HALFWIDTH AND FULLWIDTH FORMS", 
                             "HALFWIDTHANDFULLWIDTHFORMS");

        /**
         * "Specials" Unicode字符块的常量.
         * @since 1.2
         */
        public static final UnicodeBlock SPECIALS =
            new UnicodeBlock("SPECIALS");

        /**
         * @deprecated 从J2SE 5开始, 使用{@link #HIGH_SURROGATES},
         *             {@link #HIGH_PRIVATE_USE_SURROGATES}, 还有
         *             {@link #LOW_SURROGATES}. 这些新常量符合 Unicode 标准的块的定义
         *             {@link #of(char)} 和 {@link #of(int)}方法返回新常量, 而不是 SURROGATES_AREA.
         */
        @Deprecated
        public static final UnicodeBlock SURROGATES_AREA =
            new UnicodeBlock("SURROGATES_AREA");

        /**
         * "Syriac" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock SYRIAC =
            new UnicodeBlock("SYRIAC");

        /**
         * "Thaana" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock THAANA =
            new UnicodeBlock("THAANA");

        /**
         * "Sinhala" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock SINHALA =
            new UnicodeBlock("SINHALA");

        /**
         * "Myanmar" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock MYANMAR =
            new UnicodeBlock("MYANMAR");

        /**
         * "Ethiopic" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock ETHIOPIC =
            new UnicodeBlock("ETHIOPIC");

        /**
         * "Cherokee" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock CHEROKEE =
            new UnicodeBlock("CHEROKEE");

        /**
         * "Unified Canadian Aboriginal Syllabics" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS =
            new UnicodeBlock("UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS", 
                             "UNIFIED CANADIAN ABORIGINAL SYLLABICS", 
                             "UNIFIEDCANADIANABORIGINALSYLLABICS");

        /**
         * "Ogham" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock OGHAM =
            new UnicodeBlock("OGHAM");

        /**
         * "Runic" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock RUNIC =
            new UnicodeBlock("RUNIC");

        /**
         * "Khmer" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock KHMER =
            new UnicodeBlock("KHMER");

        /**
         * "Mongolian" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock MONGOLIAN =
            new UnicodeBlock("MONGOLIAN");

        /**
         * "Braille Patterns" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock BRAILLE_PATTERNS =
            new UnicodeBlock("BRAILLE_PATTERNS", 
                             "BRAILLE PATTERNS", 
                             "BRAILLEPATTERNS");

        /**
         * "CJK Radicals Supplement" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock CJK_RADICALS_SUPPLEMENT =
            new UnicodeBlock("CJK_RADICALS_SUPPLEMENT", 
                             "CJK RADICALS SUPPLEMENT", 
                             "CJKRADICALSSUPPLEMENT");

        /**
         * "Kangxi Radicals" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock KANGXI_RADICALS =
            new UnicodeBlock("KANGXI_RADICALS", 
                             "KANGXI RADICALS", 
                             "KANGXIRADICALS");

        /**
         * "Ideographic Description Characters" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock IDEOGRAPHIC_DESCRIPTION_CHARACTERS =
            new UnicodeBlock("IDEOGRAPHIC_DESCRIPTION_CHARACTERS", 
                             "IDEOGRAPHIC DESCRIPTION CHARACTERS", 
                             "IDEOGRAPHICDESCRIPTIONCHARACTERS");

        /**
         * "Bopomofo Extended" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock BOPOMOFO_EXTENDED =
            new UnicodeBlock("BOPOMOFO_EXTENDED", 
                             "BOPOMOFO EXTENDED", 
                             "BOPOMOFOEXTENDED");

        /**
         * "CJK Unified Ideographs Extension A" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A =
            new UnicodeBlock("CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A", 
                             "CJK UNIFIED IDEOGRAPHS EXTENSION A", 
                             "CJKUNIFIEDIDEOGRAPHSEXTENSIONA");

        /**
         * "Yi Syllables" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock YI_SYLLABLES =
            new UnicodeBlock("YI_SYLLABLES", 
                             "YI SYLLABLES", 
                             "YISYLLABLES");

        /**
         * "Yi Radicals" Unicode字符块的常量.
         * @since 1.4
         */
        public static final UnicodeBlock YI_RADICALS =
            new UnicodeBlock("YI_RADICALS", 
                             "YI RADICALS", 
                             "YIRADICALS");

        /**
         * "Cyrillic Supplementary" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock CYRILLIC_SUPPLEMENTARY =
            new UnicodeBlock("CYRILLIC_SUPPLEMENTARY", 
                             "CYRILLIC SUPPLEMENTARY", 
                             "CYRILLICSUPPLEMENTARY", 
                             "CYRILLIC SUPPLEMENT", 
                             "CYRILLICSUPPLEMENT");

        /**
         * "Tagalog" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock TAGALOG =
            new UnicodeBlock("TAGALOG");

        /**
         * "Hanunoo" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock HANUNOO =
            new UnicodeBlock("HANUNOO");

        /**
         * "Buhid" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock BUHID =
            new UnicodeBlock("BUHID");

        /**
         * "Tagbanwa" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock TAGBANWA =
            new UnicodeBlock("TAGBANWA");

        /**
         * "Limbu" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock LIMBU =
            new UnicodeBlock("LIMBU");

        /**
         * "Tai Le" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock TAI_LE =
            new UnicodeBlock("TAI_LE", 
                             "TAI LE", 
                             "TAILE");

        /**
         * "Khmer Symbols" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock KHMER_SYMBOLS =
            new UnicodeBlock("KHMER_SYMBOLS", 
                             "KHMER SYMBOLS", 
                             "KHMERSYMBOLS");

        /**
         * "Phonetic Extensions" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock PHONETIC_EXTENSIONS =
            new UnicodeBlock("PHONETIC_EXTENSIONS", 
                             "PHONETIC EXTENSIONS", 
                             "PHONETICEXTENSIONS");

        /**
         * "Miscellaneous Mathematical Symbols-A" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A =
            new UnicodeBlock("MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A", 
                             "MISCELLANEOUS MATHEMATICAL SYMBOLS-A", 
                             "MISCELLANEOUSMATHEMATICALSYMBOLS-A");

        /**
         * "Supplemental Arrows-A" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock SUPPLEMENTAL_ARROWS_A =
            new UnicodeBlock("SUPPLEMENTAL_ARROWS_A", 
                             "SUPPLEMENTAL ARROWS-A", 
                             "SUPPLEMENTALARROWS-A");

        /**
         * "Supplemental Arrows-B" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock SUPPLEMENTAL_ARROWS_B =
            new UnicodeBlock("SUPPLEMENTAL_ARROWS_B", 
                             "SUPPLEMENTAL ARROWS-B", 
                             "SUPPLEMENTALARROWS-B");

        /**
         * "Miscellaneous Mathematical Symbols-B" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B =
            new UnicodeBlock("MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B", 
                             "MISCELLANEOUS MATHEMATICAL SYMBOLS-B", 
                             "MISCELLANEOUSMATHEMATICALSYMBOLS-B");

        /**
         * "Supplemental Mathematical Operators" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock SUPPLEMENTAL_MATHEMATICAL_OPERATORS =
            new UnicodeBlock("SUPPLEMENTAL_MATHEMATICAL_OPERATORS", 
                             "SUPPLEMENTAL MATHEMATICAL OPERATORS", 
                             "SUPPLEMENTALMATHEMATICALOPERATORS");

        /**
         * "Miscellaneous Symbols and Arrows" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock MISCELLANEOUS_SYMBOLS_AND_ARROWS =
            new UnicodeBlock("MISCELLANEOUS_SYMBOLS_AND_ARROWS", 
                             "MISCELLANEOUS SYMBOLS AND ARROWS", 
                             "MISCELLANEOUSSYMBOLSANDARROWS");

        /**
         * "Katakana Phonetic Extensions" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock KATAKANA_PHONETIC_EXTENSIONS =
            new UnicodeBlock("KATAKANA_PHONETIC_EXTENSIONS", 
                             "KATAKANA PHONETIC EXTENSIONS", 
                             "KATAKANAPHONETICEXTENSIONS");

        /**
         * "Yijing Hexagram Symbols" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock YIJING_HEXAGRAM_SYMBOLS =
            new UnicodeBlock("YIJING_HEXAGRAM_SYMBOLS", 
                             "YIJING HEXAGRAM SYMBOLS", 
                             "YIJINGHEXAGRAMSYMBOLS");

        /**
         * "Variation Selectors" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock VARIATION_SELECTORS =
            new UnicodeBlock("VARIATION_SELECTORS", 
                             "VARIATION SELECTORS", 
                             "VARIATIONSELECTORS");

        /**
         * "Linear B Syllabary" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock LINEAR_B_SYLLABARY =
            new UnicodeBlock("LINEAR_B_SYLLABARY", 
                             "LINEAR B SYLLABARY", 
                             "LINEARBSYLLABARY");

        /**
         * "Linear B Ideograms" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock LINEAR_B_IDEOGRAMS =
            new UnicodeBlock("LINEAR_B_IDEOGRAMS", 
                             "LINEAR B IDEOGRAMS", 
                             "LINEARBIDEOGRAMS");

        /**
         * "Aegean Numbers" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock AEGEAN_NUMBERS =
            new UnicodeBlock("AEGEAN_NUMBERS", 
                             "AEGEAN NUMBERS", 
                             "AEGEANNUMBERS");

        /**
         * "Old Italic" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock OLD_ITALIC =
            new UnicodeBlock("OLD_ITALIC", 
                             "OLD ITALIC", 
                             "OLDITALIC");

        /**
         * "Gothic" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock GOTHIC =
            new UnicodeBlock("GOTHIC");

        /**
         * "Ugaritic" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock UGARITIC =
            new UnicodeBlock("UGARITIC");

        /**
         * "Deseret" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock DESERET =
            new UnicodeBlock("DESERET");

        /**
         * "Shavian" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock SHAVIAN =
            new UnicodeBlock("SHAVIAN");

        /**
         * "Osmanya" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock OSMANYA =
            new UnicodeBlock("OSMANYA");

        /**
         * "Cypriot Syllabary" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock CYPRIOT_SYLLABARY =
            new UnicodeBlock("CYPRIOT_SYLLABARY", 
                             "CYPRIOT SYLLABARY", 
                             "CYPRIOTSYLLABARY");

        /**
         * "Byzantine Musical Symbols" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock BYZANTINE_MUSICAL_SYMBOLS =
            new UnicodeBlock("BYZANTINE_MUSICAL_SYMBOLS", 
                             "BYZANTINE MUSICAL SYMBOLS", 
                             "BYZANTINEMUSICALSYMBOLS");

        /**
         * "Musical Symbols" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock MUSICAL_SYMBOLS =
            new UnicodeBlock("MUSICAL_SYMBOLS", 
                             "MUSICAL SYMBOLS", 
                             "MUSICALSYMBOLS");

        /**
         * "Tai Xuan Jing Symbols" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock TAI_XUAN_JING_SYMBOLS =
            new UnicodeBlock("TAI_XUAN_JING_SYMBOLS", 
                             "TAI XUAN JING SYMBOLS", 
                             "TAIXUANJINGSYMBOLS");

        /**
         * "Mathematical Alphanumeric Symbols"　Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock MATHEMATICAL_ALPHANUMERIC_SYMBOLS =
            new UnicodeBlock("MATHEMATICAL_ALPHANUMERIC_SYMBOLS", 
                             "MATHEMATICAL ALPHANUMERIC SYMBOLS", 
                             "MATHEMATICALALPHANUMERICSYMBOLS");

        /**
         * "CJK Unified Ideographs Extension B" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B =
            new UnicodeBlock("CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B", 
                             "CJK UNIFIED IDEOGRAPHS EXTENSION B", 
                             "CJKUNIFIEDIDEOGRAPHSEXTENSIONB");

        /**
         * "CJK Compatibility Ideographs Supplement" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT =
            new UnicodeBlock("CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT", 
                             "CJK COMPATIBILITY IDEOGRAPHS SUPPLEMENT", 
                             "CJKCOMPATIBILITYIDEOGRAPHSSUPPLEMENT");

        /**
         * "Tags" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock TAGS =
            new UnicodeBlock("TAGS");

        /**
         * "Variation Selectors Supplement"　Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock VARIATION_SELECTORS_SUPPLEMENT =
            new UnicodeBlock("VARIATION_SELECTORS_SUPPLEMENT", 
                             "VARIATION SELECTORS SUPPLEMENT", 
                             "VARIATIONSELECTORSSUPPLEMENT");

        /**
         * "Supplementary Private Use Area-A" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock SUPPLEMENTARY_PRIVATE_USE_AREA_A =
            new UnicodeBlock("SUPPLEMENTARY_PRIVATE_USE_AREA_A", 
                             "SUPPLEMENTARY PRIVATE USE AREA-A", 
                             "SUPPLEMENTARYPRIVATEUSEAREA-A");

        /**
         * "Supplementary Private Use Area-B" Unicode字符块的常量.
         * @since 1.5
         */
        public static final UnicodeBlock SUPPLEMENTARY_PRIVATE_USE_AREA_B =
            new UnicodeBlock("SUPPLEMENTARY_PRIVATE_USE_AREA_B", 
                             "SUPPLEMENTARY PRIVATE USE AREA-B", 
                             "SUPPLEMENTARYPRIVATEUSEAREA-B");

        /**
         * "High Surrogates" Unicode字符块的常量.
         * This block represents codepoint values in the high surrogate
         * range: U+D800 through U+DB7F
         *
         * @since 1.5
         */
        public static final UnicodeBlock HIGH_SURROGATES =
            new UnicodeBlock("HIGH_SURROGATES", 
                             "HIGH SURROGATES", 
                             "HIGHSURROGATES");

        /**
         * "High Private Use Surrogates" Unicode字符块的常量.
         * 该字符块表示低代理项范围(从 0xDB80 到 0xDBFF)内的代码点值.
         *
         * @since 1.5
         */
        public static final UnicodeBlock HIGH_PRIVATE_USE_SURROGATES =
            new UnicodeBlock("HIGH_PRIVATE_USE_SURROGATES", 
                             "HIGH PRIVATE USE SURROGATES", 
                             "HIGHPRIVATEUSESURROGATES");

        /**
         * "Low Surrogates" Unicode字符块的常量.
         * 该字符块表示低代理项范围(从 0xDC00 到 0xDFFF)内的代码点值.
         *
         * @since 1.5
         */
        public static final UnicodeBlock LOW_SURROGATES =
            new UnicodeBlock("LOW_SURROGATES", 
                             "LOW SURROGATES", 
                             "LOWSURROGATES");

        /**
         * "Arabic Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ARABIC_SUPPLEMENT =
            new UnicodeBlock("ARABIC_SUPPLEMENT", 
                             "ARABIC SUPPLEMENT", 
                             "ARABICSUPPLEMENT");

        /**
         * "NKo" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock NKO =
            new UnicodeBlock("NKO");

        /**
         * "Samaritan" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock SAMARITAN =
            new UnicodeBlock("SAMARITAN");

        /**
         * "Mandaic" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock MANDAIC =
            new UnicodeBlock("MANDAIC");

        /**
         * "Ethiopic Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ETHIOPIC_SUPPLEMENT =
            new UnicodeBlock("ETHIOPIC_SUPPLEMENT", 
                             "ETHIOPIC SUPPLEMENT", 
                             "ETHIOPICSUPPLEMENT");

        /**
         * "Unified Canadian Aboriginal Syllabics Extended" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED =
            new UnicodeBlock("UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED", 
                             "UNIFIED CANADIAN ABORIGINAL SYLLABICS EXTENDED", 
                             "UNIFIEDCANADIANABORIGINALSYLLABICSEXTENDED");

        /**
         * "New Tai Lue" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock NEW_TAI_LUE =
            new UnicodeBlock("NEW_TAI_LUE", 
                             "NEW TAI LUE", 
                             "NEWTAILUE");

        /**
         * "Buginese" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock BUGINESE =
            new UnicodeBlock("BUGINESE");

        /**
         * "Tai Tham" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock TAI_THAM =
            new UnicodeBlock("TAI_THAM", 
                             "TAI THAM", 
                             "TAITHAM");

        /**
         * "Balinese" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock BALINESE =
            new UnicodeBlock("BALINESE");

        /**
         * "Sundanese" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock SUNDANESE =
            new UnicodeBlock("SUNDANESE");

        /**
         * "Batak" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock BATAK =
            new UnicodeBlock("BATAK");

        /**
         * "Lepcha" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock LEPCHA =
            new UnicodeBlock("LEPCHA");

        /**
         * "Ol Chiki" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock OL_CHIKI =
            new UnicodeBlock("OL_CHIKI", 
                             "OL CHIKI", 
                             "OLCHIKI");

        /**
         * "Vedic Extensions" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock VEDIC_EXTENSIONS =
            new UnicodeBlock("VEDIC_EXTENSIONS", 
                             "VEDIC EXTENSIONS", 
                             "VEDICEXTENSIONS");

        /**
         * "Phonetic Extensions Supplement"  Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock PHONETIC_EXTENSIONS_SUPPLEMENT =
            new UnicodeBlock("PHONETIC_EXTENSIONS_SUPPLEMENT", 
                             "PHONETIC EXTENSIONS SUPPLEMENT", 
                             "PHONETICEXTENSIONSSUPPLEMENT");

        /**
         * "Combining Diacritical Marks Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock COMBINING_DIACRITICAL_MARKS_SUPPLEMENT =
            new UnicodeBlock("COMBINING_DIACRITICAL_MARKS_SUPPLEMENT", 
                             "COMBINING DIACRITICAL MARKS SUPPLEMENT", 
                             "COMBININGDIACRITICALMARKSSUPPLEMENT");

        /**
         * "Glagolitic" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock GLAGOLITIC =
            new UnicodeBlock("GLAGOLITIC");

        /**
         * "Latin Extended-C" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock LATIN_EXTENDED_C =
            new UnicodeBlock("LATIN_EXTENDED_C", 
                             "LATIN EXTENDED-C", 
                             "LATINEXTENDED-C");

        /**
         * "Coptic" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock COPTIC =
            new UnicodeBlock("COPTIC");

        /**
         * "Georgian Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock GEORGIAN_SUPPLEMENT =
            new UnicodeBlock("GEORGIAN_SUPPLEMENT", 
                             "GEORGIAN SUPPLEMENT", 
                             "GEORGIANSUPPLEMENT");

        /**
         * "Tifinagh" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock TIFINAGH =
            new UnicodeBlock("TIFINAGH");

        /**
         * "Ethiopic Extended" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ETHIOPIC_EXTENDED =
            new UnicodeBlock("ETHIOPIC_EXTENDED", 
                             "ETHIOPIC EXTENDED", 
                             "ETHIOPICEXTENDED");

        /**
         * "Cyrillic Extended-A" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CYRILLIC_EXTENDED_A =
            new UnicodeBlock("CYRILLIC_EXTENDED_A", 
                             "CYRILLIC EXTENDED-A", 
                             "CYRILLICEXTENDED-A");

        /**
         * "Supplemental Punctuation" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock SUPPLEMENTAL_PUNCTUATION =
            new UnicodeBlock("SUPPLEMENTAL_PUNCTUATION", 
                             "SUPPLEMENTAL PUNCTUATION", 
                             "SUPPLEMENTALPUNCTUATION");

        /**
         * "CJK Strokes" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CJK_STROKES =
            new UnicodeBlock("CJK_STROKES", 
                             "CJK STROKES", 
                             "CJKSTROKES");

        /**
         * "Lisu" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock LISU =
            new UnicodeBlock("LISU");

        /**
         * "Vai" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock VAI =
            new UnicodeBlock("VAI");

        /**
         * "Cyrillic Extended-B" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CYRILLIC_EXTENDED_B =
            new UnicodeBlock("CYRILLIC_EXTENDED_B", 
                             "CYRILLIC EXTENDED-B", 
                             "CYRILLICEXTENDED-B");

        /**
         * "Bamum" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock BAMUM =
            new UnicodeBlock("BAMUM");

        /**
         * "Modifier Tone Letters" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock MODIFIER_TONE_LETTERS =
            new UnicodeBlock("MODIFIER_TONE_LETTERS", 
                             "MODIFIER TONE LETTERS", 
                             "MODIFIERTONELETTERS");

        /**
         * "Latin Extended-D" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock LATIN_EXTENDED_D =
            new UnicodeBlock("LATIN_EXTENDED_D", 
                             "LATIN EXTENDED-D", 
                             "LATINEXTENDED-D");

        /**
         * "Syloti Nagri" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock SYLOTI_NAGRI =
            new UnicodeBlock("SYLOTI_NAGRI", 
                             "SYLOTI NAGRI", 
                             "SYLOTINAGRI");

        /**
         * "Common Indic Number Forms" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock COMMON_INDIC_NUMBER_FORMS =
            new UnicodeBlock("COMMON_INDIC_NUMBER_FORMS", 
                             "COMMON INDIC NUMBER FORMS", 
                             "COMMONINDICNUMBERFORMS");

        /**
         * "Phags-pa" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock PHAGS_PA =
            new UnicodeBlock("PHAGS_PA", 
                             "PHAGS-PA");

        /**
         * "Saurashtra" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock SAURASHTRA =
            new UnicodeBlock("SAURASHTRA");

        /**
         * "Devanagari Extended" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock DEVANAGARI_EXTENDED =
            new UnicodeBlock("DEVANAGARI_EXTENDED", 
                             "DEVANAGARI EXTENDED", 
                             "DEVANAGARIEXTENDED");

        /**
         * "Kayah Li" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock KAYAH_LI =
            new UnicodeBlock("KAYAH_LI", 
                             "KAYAH LI", 
                             "KAYAHLI");

        /**
         * "Rejang" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock REJANG =
            new UnicodeBlock("REJANG");

        /**
         * "Hangul Jamo Extended-A" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock HANGUL_JAMO_EXTENDED_A =
            new UnicodeBlock("HANGUL_JAMO_EXTENDED_A", 
                             "HANGUL JAMO EXTENDED-A", 
                             "HANGULJAMOEXTENDED-A");

        /**
         * "Javanese" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock JAVANESE =
            new UnicodeBlock("JAVANESE");

        /**
         * "Cham" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CHAM =
            new UnicodeBlock("CHAM");

        /**
         * "Myanmar Extended-A" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock MYANMAR_EXTENDED_A =
            new UnicodeBlock("MYANMAR_EXTENDED_A", 
                             "MYANMAR EXTENDED-A", 
                             "MYANMAREXTENDED-A");

        /**
         * "Tai Viet" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock TAI_VIET =
            new UnicodeBlock("TAI_VIET", 
                             "TAI VIET", 
                             "TAIVIET");

        /**
         * "Ethiopic Extended-A" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ETHIOPIC_EXTENDED_A =
            new UnicodeBlock("ETHIOPIC_EXTENDED_A", 
                             "ETHIOPIC EXTENDED-A", 
                             "ETHIOPICEXTENDED-A");

        /**
         * "Meetei Mayek" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock MEETEI_MAYEK =
            new UnicodeBlock("MEETEI_MAYEK", 
                             "MEETEI MAYEK", 
                             "MEETEIMAYEK");

        /**
         * "Hangul Jamo Extended-B" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock HANGUL_JAMO_EXTENDED_B =
            new UnicodeBlock("HANGUL_JAMO_EXTENDED_B", 
                             "HANGUL JAMO EXTENDED-B", 
                             "HANGULJAMOEXTENDED-B");

        /**
         * "Vertical Forms" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock VERTICAL_FORMS =
            new UnicodeBlock("VERTICAL_FORMS", 
                             "VERTICAL FORMS", 
                             "VERTICALFORMS");

        /**
         * "Ancient Greek Numbers" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ANCIENT_GREEK_NUMBERS =
            new UnicodeBlock("ANCIENT_GREEK_NUMBERS", 
                             "ANCIENT GREEK NUMBERS", 
                             "ANCIENTGREEKNUMBERS");

        /**
         * "Ancient Symbols" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ANCIENT_SYMBOLS =
            new UnicodeBlock("ANCIENT_SYMBOLS", 
                             "ANCIENT SYMBOLS", 
                             "ANCIENTSYMBOLS");

        /**
         * "Phaistos Disc" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock PHAISTOS_DISC =
            new UnicodeBlock("PHAISTOS_DISC", 
                             "PHAISTOS DISC", 
                             "PHAISTOSDISC");

        /**
         * "Lycian" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock LYCIAN =
            new UnicodeBlock("LYCIAN");

        /**
         * "Carian" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CARIAN =
            new UnicodeBlock("CARIAN");

        /**
         * "Old Persian" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock OLD_PERSIAN =
            new UnicodeBlock("OLD_PERSIAN", 
                             "OLD PERSIAN", 
                             "OLDPERSIAN");

        /**
         * "Imperial Aramaic" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock IMPERIAL_ARAMAIC =
            new UnicodeBlock("IMPERIAL_ARAMAIC", 
                             "IMPERIAL ARAMAIC", 
                             "IMPERIALARAMAIC");

        /**
         * "Phoenician" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock PHOENICIAN =
            new UnicodeBlock("PHOENICIAN");

        /**
         * "Lydian" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock LYDIAN =
            new UnicodeBlock("LYDIAN");

        /**
         * "Kharoshthi" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock KHAROSHTHI =
            new UnicodeBlock("KHAROSHTHI");

        /**
         * "Old South Arabian" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock OLD_SOUTH_ARABIAN =
            new UnicodeBlock("OLD_SOUTH_ARABIAN", 
                             "OLD SOUTH ARABIAN", 
                             "OLDSOUTHARABIAN");

        /**
         * "Avestan" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock AVESTAN =
            new UnicodeBlock("AVESTAN");

        /**
         * "Inscriptional Parthian" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock INSCRIPTIONAL_PARTHIAN =
            new UnicodeBlock("INSCRIPTIONAL_PARTHIAN", 
                             "INSCRIPTIONAL PARTHIAN", 
                             "INSCRIPTIONALPARTHIAN");

        /**
         * "Inscriptional Pahlavi" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock INSCRIPTIONAL_PAHLAVI =
            new UnicodeBlock("INSCRIPTIONAL_PAHLAVI", 
                             "INSCRIPTIONAL PAHLAVI", 
                             "INSCRIPTIONALPAHLAVI");

        /**
         * "Old Turkic" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock OLD_TURKIC =
            new UnicodeBlock("OLD_TURKIC", 
                             "OLD TURKIC", 
                             "OLDTURKIC");

        /**
         * "Rumi Numeral Symbols" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock RUMI_NUMERAL_SYMBOLS =
            new UnicodeBlock("RUMI_NUMERAL_SYMBOLS", 
                             "RUMI NUMERAL SYMBOLS", 
                             "RUMINUMERALSYMBOLS");

        /**
         * "Brahmi" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock BRAHMI =
            new UnicodeBlock("BRAHMI");

        /**
         * "Kaithi" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock KAITHI =
            new UnicodeBlock("KAITHI");

        /**
         * "Cuneiform" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CUNEIFORM =
            new UnicodeBlock("CUNEIFORM");

        /**
         * "Cuneiform Numbers and Punctuation" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CUNEIFORM_NUMBERS_AND_PUNCTUATION =
            new UnicodeBlock("CUNEIFORM_NUMBERS_AND_PUNCTUATION", 
                             "CUNEIFORM NUMBERS AND PUNCTUATION", 
                             "CUNEIFORMNUMBERSANDPUNCTUATION");

        /**
         * "Egyptian Hieroglyphs" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock EGYPTIAN_HIEROGLYPHS =
            new UnicodeBlock("EGYPTIAN_HIEROGLYPHS", 
                             "EGYPTIAN HIEROGLYPHS", 
                             "EGYPTIANHIEROGLYPHS");

        /**
         * "Bamum Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock BAMUM_SUPPLEMENT =
            new UnicodeBlock("BAMUM_SUPPLEMENT", 
                             "BAMUM SUPPLEMENT", 
                             "BAMUMSUPPLEMENT");

        /**
         * "Kana Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock KANA_SUPPLEMENT =
            new UnicodeBlock("KANA_SUPPLEMENT", 
                             "KANA SUPPLEMENT", 
                             "KANASUPPLEMENT");

        /**
         * "Ancient Greek Musical Notation" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ANCIENT_GREEK_MUSICAL_NOTATION =
            new UnicodeBlock("ANCIENT_GREEK_MUSICAL_NOTATION", 
                             "ANCIENT GREEK MUSICAL NOTATION", 
                             "ANCIENTGREEKMUSICALNOTATION");

        /**
         * "Counting Rod Numerals" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock COUNTING_ROD_NUMERALS =
            new UnicodeBlock("COUNTING_ROD_NUMERALS", 
                             "COUNTING ROD NUMERALS", 
                             "COUNTINGRODNUMERALS");

        /**
         * "Mahjong Tiles" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock MAHJONG_TILES =
            new UnicodeBlock("MAHJONG_TILES", 
                             "MAHJONG TILES", 
                             "MAHJONGTILES");

        /**
         * "Domino Tiles" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock DOMINO_TILES =
            new UnicodeBlock("DOMINO_TILES", 
                             "DOMINO TILES", 
                             "DOMINOTILES");

        /**
         * "Playing Cards" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock PLAYING_CARDS =
            new UnicodeBlock("PLAYING_CARDS", 
                             "PLAYING CARDS", 
                             "PLAYINGCARDS");

        /**
         * "Enclosed Alphanumeric Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ENCLOSED_ALPHANUMERIC_SUPPLEMENT =
            new UnicodeBlock("ENCLOSED_ALPHANUMERIC_SUPPLEMENT", 
                             "ENCLOSED ALPHANUMERIC SUPPLEMENT", 
                             "ENCLOSEDALPHANUMERICSUPPLEMENT");

        /**
         * "Enclosed Ideographic Supplement" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ENCLOSED_IDEOGRAPHIC_SUPPLEMENT =
            new UnicodeBlock("ENCLOSED_IDEOGRAPHIC_SUPPLEMENT", 
                             "ENCLOSED IDEOGRAPHIC SUPPLEMENT", 
                             "ENCLOSEDIDEOGRAPHICSUPPLEMENT");

        /**
         * "Miscellaneous Symbols And Pictographs" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS =
            new UnicodeBlock("MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS", 
                             "MISCELLANEOUS SYMBOLS AND PICTOGRAPHS", 
                             "MISCELLANEOUSSYMBOLSANDPICTOGRAPHS");

        /**
         * "Emoticons" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock EMOTICONS =
            new UnicodeBlock("EMOTICONS");

        /**
         * "Transport And Map Symbols" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock TRANSPORT_AND_MAP_SYMBOLS =
            new UnicodeBlock("TRANSPORT_AND_MAP_SYMBOLS", 
                             "TRANSPORT AND MAP SYMBOLS", 
                             "TRANSPORTANDMAPSYMBOLS");

        /**
         * "Alchemical Symbols" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock ALCHEMICAL_SYMBOLS =
            new UnicodeBlock("ALCHEMICAL_SYMBOLS", 
                             "ALCHEMICAL SYMBOLS", 
                             "ALCHEMICALSYMBOLS");

        /**
         * "CJK Unified Ideographs Extension C" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C =
            new UnicodeBlock("CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C", 
                             "CJK UNIFIED IDEOGRAPHS EXTENSION C", 
                             "CJKUNIFIEDIDEOGRAPHSEXTENSIONC");

        /**
         * "CJK Unified Ideographs Extension D" Unicode字符块的常量.
         * @since 1.7
         */
        public static final UnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D =
            new UnicodeBlock("CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D", 
                             "CJK UNIFIED IDEOGRAPHS EXTENSION D", 
                             "CJKUNIFIEDIDEOGRAPHSEXTENSIOND");

        /**
         * "Arabic Extended-A" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock ARABIC_EXTENDED_A =
            new UnicodeBlock("ARABIC_EXTENDED_A", 
                             "ARABIC EXTENDED-A", 
                             "ARABICEXTENDED-A");

        /**
         * "Sundanese Supplement" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock SUNDANESE_SUPPLEMENT =
            new UnicodeBlock("SUNDANESE_SUPPLEMENT", 
                             "SUNDANESE SUPPLEMENT", 
                             "SUNDANESESUPPLEMENT");

        /**
         * "Meetei Mayek Extensions" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock MEETEI_MAYEK_EXTENSIONS =
            new UnicodeBlock("MEETEI_MAYEK_EXTENSIONS", 
                             "MEETEI MAYEK EXTENSIONS", 
                             "MEETEIMAYEKEXTENSIONS");

        /**
         * "Meroitic Hieroglyphs" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock MEROITIC_HIEROGLYPHS =
            new UnicodeBlock("MEROITIC_HIEROGLYPHS", 
                             "MEROITIC HIEROGLYPHS", 
                             "MEROITICHIEROGLYPHS");

        /**
         * "Meroitic Cursive" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock MEROITIC_CURSIVE =
            new UnicodeBlock("MEROITIC_CURSIVE", 
                             "MEROITIC CURSIVE", 
                             "MEROITICCURSIVE");

        /**
         * "Sora Sompeng" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock SORA_SOMPENG =
            new UnicodeBlock("SORA_SOMPENG", 
                             "SORA SOMPENG", 
                             "SORASOMPENG");

        /**
         * "Chakma" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock CHAKMA =
            new UnicodeBlock("CHAKMA");

        /**
         * "Sharada" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock SHARADA =
            new UnicodeBlock("SHARADA");

        /**
         * "Takri" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock TAKRI =
            new UnicodeBlock("TAKRI");

        /**
         * "Miao" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock MIAO =
            new UnicodeBlock("MIAO");

        /**
         * "Arabic Mathematical Alphabetic Symbols" Unicode字符块的常量.
         * @since 1.8
         */
        public static final UnicodeBlock ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS =
            new UnicodeBlock("ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS", 
                             "ARABIC MATHEMATICAL ALPHABETIC SYMBOLS", 
                             "ARABICMATHEMATICALALPHABETICSYMBOLS");

        private static final int blockStarts[] = {
            0x0000,   // 0000..007F; Basic Latin
            0x0080,   // 0080..00FF; Latin-1 Supplement
            0x0100,   // 0100..017F; Latin Extended-A
            0x0180,   // 0180..024F; Latin Extended-B
            0x0250,   // 0250..02AF; IPA Extensions
            0x02B0,   // 02B0..02FF; Spacing Modifier Letters
            0x0300,   // 0300..036F; Combining Diacritical Marks
            0x0370,   // 0370..03FF; Greek and Coptic
            0x0400,   // 0400..04FF; Cyrillic
            0x0500,   // 0500..052F; Cyrillic Supplement
            0x0530,   // 0530..058F; Armenian
            0x0590,   // 0590..05FF; Hebrew
            0x0600,   // 0600..06FF; Arabic
            0x0700,   // 0700..074F; Syriac
            0x0750,   // 0750..077F; Arabic Supplement
            0x0780,   // 0780..07BF; Thaana
            0x07C0,   // 07C0..07FF; NKo
            0x0800,   // 0800..083F; Samaritan
            0x0840,   // 0840..085F; Mandaic
            0x0860,   //             unassigned
            0x08A0,   // 08A0..08FF; Arabic Extended-A
            0x0900,   // 0900..097F; Devanagari
            0x0980,   // 0980..09FF; Bengali
            0x0A00,   // 0A00..0A7F; Gurmukhi
            0x0A80,   // 0A80..0AFF; Gujarati
            0x0B00,   // 0B00..0B7F; Oriya
            0x0B80,   // 0B80..0BFF; Tamil
            0x0C00,   // 0C00..0C7F; Telugu
            0x0C80,   // 0C80..0CFF; Kannada
            0x0D00,   // 0D00..0D7F; Malayalam
            0x0D80,   // 0D80..0DFF; Sinhala
            0x0E00,   // 0E00..0E7F; Thai
            0x0E80,   // 0E80..0EFF; Lao
            0x0F00,   // 0F00..0FFF; Tibetan
            0x1000,   // 1000..109F; Myanmar
            0x10A0,   // 10A0..10FF; Georgian
            0x1100,   // 1100..11FF; Hangul Jamo
            0x1200,   // 1200..137F; Ethiopic
            0x1380,   // 1380..139F; Ethiopic Supplement
            0x13A0,   // 13A0..13FF; Cherokee
            0x1400,   // 1400..167F; Unified Canadian Aboriginal Syllabics
            0x1680,   // 1680..169F; Ogham
            0x16A0,   // 16A0..16FF; Runic
            0x1700,   // 1700..171F; Tagalog
            0x1720,   // 1720..173F; Hanunoo
            0x1740,   // 1740..175F; Buhid
            0x1760,   // 1760..177F; Tagbanwa
            0x1780,   // 1780..17FF; Khmer
            0x1800,   // 1800..18AF; Mongolian
            0x18B0,   // 18B0..18FF; Unified Canadian Aboriginal Syllabics Extended
            0x1900,   // 1900..194F; Limbu
            0x1950,   // 1950..197F; Tai Le
            0x1980,   // 1980..19DF; New Tai Lue
            0x19E0,   // 19E0..19FF; Khmer Symbols
            0x1A00,   // 1A00..1A1F; Buginese
            0x1A20,   // 1A20..1AAF; Tai Tham
            0x1AB0,   //             unassigned
            0x1B00,   // 1B00..1B7F; Balinese
            0x1B80,   // 1B80..1BBF; Sundanese
            0x1BC0,   // 1BC0..1BFF; Batak
            0x1C00,   // 1C00..1C4F; Lepcha
            0x1C50,   // 1C50..1C7F; Ol Chiki
            0x1C80,   //             unassigned
            0x1CC0,   // 1CC0..1CCF; Sundanese Supplement
            0x1CD0,   // 1CD0..1CFF; Vedic Extensions
            0x1D00,   // 1D00..1D7F; Phonetic Extensions
            0x1D80,   // 1D80..1DBF; Phonetic Extensions Supplement
            0x1DC0,   // 1DC0..1DFF; Combining Diacritical Marks Supplement
            0x1E00,   // 1E00..1EFF; Latin Extended Additional
            0x1F00,   // 1F00..1FFF; Greek Extended
            0x2000,   // 2000..206F; General Punctuation
            0x2070,   // 2070..209F; Superscripts and Subscripts
            0x20A0,   // 20A0..20CF; Currency Symbols
            0x20D0,   // 20D0..20FF; Combining Diacritical Marks for Symbols
            0x2100,   // 2100..214F; Letterlike Symbols
            0x2150,   // 2150..218F; Number Forms
            0x2190,   // 2190..21FF; Arrows
            0x2200,   // 2200..22FF; Mathematical Operators
            0x2300,   // 2300..23FF; Miscellaneous Technical
            0x2400,   // 2400..243F; Control Pictures
            0x2440,   // 2440..245F; Optical Character Recognition
            0x2460,   // 2460..24FF; Enclosed Alphanumerics
            0x2500,   // 2500..257F; Box Drawing
            0x2580,   // 2580..259F; Block Elements
            0x25A0,   // 25A0..25FF; Geometric Shapes
            0x2600,   // 2600..26FF; Miscellaneous Symbols
            0x2700,   // 2700..27BF; Dingbats
            0x27C0,   // 27C0..27EF; Miscellaneous Mathematical Symbols-A
            0x27F0,   // 27F0..27FF; Supplemental Arrows-A
            0x2800,   // 2800..28FF; Braille Patterns
            0x2900,   // 2900..297F; Supplemental Arrows-B
            0x2980,   // 2980..29FF; Miscellaneous Mathematical Symbols-B
            0x2A00,   // 2A00..2AFF; Supplemental Mathematical Operators
            0x2B00,   // 2B00..2BFF; Miscellaneous Symbols and Arrows
            0x2C00,   // 2C00..2C5F; Glagolitic
            0x2C60,   // 2C60..2C7F; Latin Extended-C
            0x2C80,   // 2C80..2CFF; Coptic
            0x2D00,   // 2D00..2D2F; Georgian Supplement
            0x2D30,   // 2D30..2D7F; Tifinagh
            0x2D80,   // 2D80..2DDF; Ethiopic Extended
            0x2DE0,   // 2DE0..2DFF; Cyrillic Extended-A
            0x2E00,   // 2E00..2E7F; Supplemental Punctuation
            0x2E80,   // 2E80..2EFF; CJK Radicals Supplement
            0x2F00,   // 2F00..2FDF; Kangxi Radicals
            0x2FE0,   //             unassigned
            0x2FF0,   // 2FF0..2FFF; Ideographic Description Characters
            0x3000,   // 3000..303F; CJK Symbols and Punctuation
            0x3040,   // 3040..309F; Hiragana
            0x30A0,   // 30A0..30FF; Katakana
            0x3100,   // 3100..312F; Bopomofo
            0x3130,   // 3130..318F; Hangul Compatibility Jamo
            0x3190,   // 3190..319F; Kanbun
            0x31A0,   // 31A0..31BF; Bopomofo Extended
            0x31C0,   // 31C0..31EF; CJK Strokes
            0x31F0,   // 31F0..31FF; Katakana Phonetic Extensions
            0x3200,   // 3200..32FF; Enclosed CJK Letters and Months
            0x3300,   // 3300..33FF; CJK Compatibility
            0x3400,   // 3400..4DBF; CJK Unified Ideographs Extension A
            0x4DC0,   // 4DC0..4DFF; Yijing Hexagram Symbols
            0x4E00,   // 4E00..9FFF; CJK Unified Ideographs
            0xA000,   // A000..A48F; Yi Syllables
            0xA490,   // A490..A4CF; Yi Radicals
            0xA4D0,   // A4D0..A4FF; Lisu
            0xA500,   // A500..A63F; Vai
            0xA640,   // A640..A69F; Cyrillic Extended-B
            0xA6A0,   // A6A0..A6FF; Bamum
            0xA700,   // A700..A71F; Modifier Tone Letters
            0xA720,   // A720..A7FF; Latin Extended-D
            0xA800,   // A800..A82F; Syloti Nagri
            0xA830,   // A830..A83F; Common Indic Number Forms
            0xA840,   // A840..A87F; Phags-pa
            0xA880,   // A880..A8DF; Saurashtra
            0xA8E0,   // A8E0..A8FF; Devanagari Extended
            0xA900,   // A900..A92F; Kayah Li
            0xA930,   // A930..A95F; Rejang
            0xA960,   // A960..A97F; Hangul Jamo Extended-A
            0xA980,   // A980..A9DF; Javanese
            0xA9E0,   //             unassigned
            0xAA00,   // AA00..AA5F; Cham
            0xAA60,   // AA60..AA7F; Myanmar Extended-A
            0xAA80,   // AA80..AADF; Tai Viet
            0xAAE0,   // AAE0..AAFF; Meetei Mayek Extensions
            0xAB00,   // AB00..AB2F; Ethiopic Extended-A
            0xAB30,   //             unassigned
            0xABC0,   // ABC0..ABFF; Meetei Mayek
            0xAC00,   // AC00..D7AF; Hangul Syllables
            0xD7B0,   // D7B0..D7FF; Hangul Jamo Extended-B
            0xD800,   // D800..DB7F; High Surrogates
            0xDB80,   // DB80..DBFF; High Private Use Surrogates
            0xDC00,   // DC00..DFFF; Low Surrogates
            0xE000,   // E000..F8FF; Private Use Area
            0xF900,   // F900..FAFF; CJK Compatibility Ideographs
            0xFB00,   // FB00..FB4F; Alphabetic Presentation Forms
            0xFB50,   // FB50..FDFF; Arabic Presentation Forms-A
            0xFE00,   // FE00..FE0F; Variation Selectors
            0xFE10,   // FE10..FE1F; Vertical Forms
            0xFE20,   // FE20..FE2F; Combining Half Marks
            0xFE30,   // FE30..FE4F; CJK Compatibility Forms
            0xFE50,   // FE50..FE6F; Small Form Variants
            0xFE70,   // FE70..FEFF; Arabic Presentation Forms-B
            0xFF00,   // FF00..FFEF; Halfwidth and Fullwidth Forms
            0xFFF0,   // FFF0..FFFF; Specials
            0x10000,  // 10000..1007F; Linear B Syllabary
            0x10080,  // 10080..100FF; Linear B Ideograms
            0x10100,  // 10100..1013F; Aegean Numbers
            0x10140,  // 10140..1018F; Ancient Greek Numbers
            0x10190,  // 10190..101CF; Ancient Symbols
            0x101D0,  // 101D0..101FF; Phaistos Disc
            0x10200,  //               unassigned
            0x10280,  // 10280..1029F; Lycian
            0x102A0,  // 102A0..102DF; Carian
            0x102E0,  //               unassigned
            0x10300,  // 10300..1032F; Old Italic
            0x10330,  // 10330..1034F; Gothic
            0x10350,  //               unassigned
            0x10380,  // 10380..1039F; Ugaritic
            0x103A0,  // 103A0..103DF; Old Persian
            0x103E0,  //               unassigned
            0x10400,  // 10400..1044F; Deseret
            0x10450,  // 10450..1047F; Shavian
            0x10480,  // 10480..104AF; Osmanya
            0x104B0,  //               unassigned
            0x10800,  // 10800..1083F; Cypriot Syllabary
            0x10840,  // 10840..1085F; Imperial Aramaic
            0x10860,  //               unassigned
            0x10900,  // 10900..1091F; Phoenician
            0x10920,  // 10920..1093F; Lydian
            0x10940,  //               unassigned
            0x10980,  // 10980..1099F; Meroitic Hieroglyphs
            0x109A0,  // 109A0..109FF; Meroitic Cursive
            0x10A00,  // 10A00..10A5F; Kharoshthi
            0x10A60,  // 10A60..10A7F; Old South Arabian
            0x10A80,  //               unassigned
            0x10B00,  // 10B00..10B3F; Avestan
            0x10B40,  // 10B40..10B5F; Inscriptional Parthian
            0x10B60,  // 10B60..10B7F; Inscriptional Pahlavi
            0x10B80,  //               unassigned
            0x10C00,  // 10C00..10C4F; Old Turkic
            0x10C50,  //               unassigned
            0x10E60,  // 10E60..10E7F; Rumi Numeral Symbols
            0x10E80,  //               unassigned
            0x11000,  // 11000..1107F; Brahmi
            0x11080,  // 11080..110CF; Kaithi
            0x110D0,  // 110D0..110FF; Sora Sompeng
            0x11100,  // 11100..1114F; Chakma
            0x11150,  //               unassigned
            0x11180,  // 11180..111DF; Sharada
            0x111E0,  //               unassigned
            0x11680,  // 11680..116CF; Takri
            0x116D0,  //               unassigned
            0x12000,  // 12000..123FF; Cuneiform
            0x12400,  // 12400..1247F; Cuneiform Numbers and Punctuation
            0x12480,  //               unassigned
            0x13000,  // 13000..1342F; Egyptian Hieroglyphs
            0x13430,  //               unassigned
            0x16800,  // 16800..16A3F; Bamum Supplement
            0x16A40,  //               unassigned
            0x16F00,  // 16F00..16F9F; Miao
            0x16FA0,  //               unassigned
            0x1B000,  // 1B000..1B0FF; Kana Supplement
            0x1B100,  //               unassigned
            0x1D000,  // 1D000..1D0FF; Byzantine Musical Symbols
            0x1D100,  // 1D100..1D1FF; Musical Symbols
            0x1D200,  // 1D200..1D24F; Ancient Greek Musical Notation
            0x1D250,  //               unassigned
            0x1D300,  // 1D300..1D35F; Tai Xuan Jing Symbols
            0x1D360,  // 1D360..1D37F; Counting Rod Numerals
            0x1D380,  //               unassigned
            0x1D400,  // 1D400..1D7FF; Mathematical Alphanumeric Symbols
            0x1D800,  //               unassigned
            0x1EE00,  // 1EE00..1EEFF; Arabic Mathematical Alphabetic Symbols
            0x1EF00,  //               unassigned
            0x1F000,  // 1F000..1F02F; Mahjong Tiles
            0x1F030,  // 1F030..1F09F; Domino Tiles
            0x1F0A0,  // 1F0A0..1F0FF; Playing Cards
            0x1F100,  // 1F100..1F1FF; Enclosed Alphanumeric Supplement
            0x1F200,  // 1F200..1F2FF; Enclosed Ideographic Supplement
            0x1F300,  // 1F300..1F5FF; Miscellaneous Symbols And Pictographs
            0x1F600,  // 1F600..1F64F; Emoticons
            0x1F650,  //               unassigned
            0x1F680,  // 1F680..1F6FF; Transport And Map Symbols
            0x1F700,  // 1F700..1F77F; Alchemical Symbols
            0x1F780,  //               unassigned
            0x20000,  // 20000..2A6DF; CJK Unified Ideographs Extension B
            0x2A6E0,  //               unassigned
            0x2A700,  // 2A700..2B73F; CJK Unified Ideographs Extension C
            0x2B740,  // 2B740..2B81F; CJK Unified Ideographs Extension D
            0x2B820,  //               unassigned
            0x2F800,  // 2F800..2FA1F; CJK Compatibility Ideographs Supplement
            0x2FA20,  //               unassigned
            0xE0000,  // E0000..E007F; Tags
            0xE0080,  //               unassigned
            0xE0100,  // E0100..E01EF; Variation Selectors Supplement
            0xE01F0,  //               unassigned
            0xF0000,  // F0000..FFFFF; Supplementary Private Use Area-A
            0x100000  // 100000..10FFFF; Supplementary Private Use Area-B
        };

        private static final UnicodeBlock[] blocks = {
            BASIC_LATIN, 
            LATIN_1_SUPPLEMENT, 
            LATIN_EXTENDED_A, 
            LATIN_EXTENDED_B, 
            IPA_EXTENSIONS, 
            SPACING_MODIFIER_LETTERS, 
            COMBINING_DIACRITICAL_MARKS, 
            GREEK, 
            CYRILLIC, 
            CYRILLIC_SUPPLEMENTARY, 
            ARMENIAN, 
            HEBREW, 
            ARABIC, 
            SYRIAC, 
            ARABIC_SUPPLEMENT, 
            THAANA, 
            NKO, 
            SAMARITAN, 
            MANDAIC, 
            null, 
            ARABIC_EXTENDED_A, 
            DEVANAGARI, 
            BENGALI, 
            GURMUKHI, 
            GUJARATI, 
            ORIYA, 
            TAMIL, 
            TELUGU, 
            KANNADA, 
            MALAYALAM, 
            SINHALA, 
            THAI, 
            LAO, 
            TIBETAN, 
            MYANMAR, 
            GEORGIAN, 
            HANGUL_JAMO, 
            ETHIOPIC, 
            ETHIOPIC_SUPPLEMENT, 
            CHEROKEE, 
            UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS, 
            OGHAM, 
            RUNIC, 
            TAGALOG, 
            HANUNOO, 
            BUHID, 
            TAGBANWA, 
            KHMER, 
            MONGOLIAN, 
            UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED, 
            LIMBU, 
            TAI_LE, 
            NEW_TAI_LUE, 
            KHMER_SYMBOLS, 
            BUGINESE, 
            TAI_THAM, 
            null, 
            BALINESE, 
            SUNDANESE, 
            BATAK, 
            LEPCHA, 
            OL_CHIKI, 
            null, 
            SUNDANESE_SUPPLEMENT, 
            VEDIC_EXTENSIONS, 
            PHONETIC_EXTENSIONS, 
            PHONETIC_EXTENSIONS_SUPPLEMENT, 
            COMBINING_DIACRITICAL_MARKS_SUPPLEMENT, 
            LATIN_EXTENDED_ADDITIONAL, 
            GREEK_EXTENDED, 
            GENERAL_PUNCTUATION, 
            SUPERSCRIPTS_AND_SUBSCRIPTS, 
            CURRENCY_SYMBOLS, 
            COMBINING_MARKS_FOR_SYMBOLS, 
            LETTERLIKE_SYMBOLS, 
            NUMBER_FORMS, 
            ARROWS, 
            MATHEMATICAL_OPERATORS, 
            MISCELLANEOUS_TECHNICAL, 
            CONTROL_PICTURES, 
            OPTICAL_CHARACTER_RECOGNITION, 
            ENCLOSED_ALPHANUMERICS, 
            BOX_DRAWING, 
            BLOCK_ELEMENTS, 
            GEOMETRIC_SHAPES, 
            MISCELLANEOUS_SYMBOLS, 
            DINGBATS, 
            MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A, 
            SUPPLEMENTAL_ARROWS_A, 
            BRAILLE_PATTERNS, 
            SUPPLEMENTAL_ARROWS_B, 
            MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B, 
            SUPPLEMENTAL_MATHEMATICAL_OPERATORS, 
            MISCELLANEOUS_SYMBOLS_AND_ARROWS, 
            GLAGOLITIC, 
            LATIN_EXTENDED_C, 
            COPTIC, 
            GEORGIAN_SUPPLEMENT, 
            TIFINAGH, 
            ETHIOPIC_EXTENDED, 
            CYRILLIC_EXTENDED_A, 
            SUPPLEMENTAL_PUNCTUATION, 
            CJK_RADICALS_SUPPLEMENT, 
            KANGXI_RADICALS, 
            null, 
            IDEOGRAPHIC_DESCRIPTION_CHARACTERS, 
            CJK_SYMBOLS_AND_PUNCTUATION, 
            HIRAGANA, 
            KATAKANA, 
            BOPOMOFO, 
            HANGUL_COMPATIBILITY_JAMO, 
            KANBUN, 
            BOPOMOFO_EXTENDED, 
            CJK_STROKES, 
            KATAKANA_PHONETIC_EXTENSIONS, 
            ENCLOSED_CJK_LETTERS_AND_MONTHS, 
            CJK_COMPATIBILITY, 
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A, 
            YIJING_HEXAGRAM_SYMBOLS, 
            CJK_UNIFIED_IDEOGRAPHS, 
            YI_SYLLABLES, 
            YI_RADICALS, 
            LISU, 
            VAI, 
            CYRILLIC_EXTENDED_B, 
            BAMUM, 
            MODIFIER_TONE_LETTERS, 
            LATIN_EXTENDED_D, 
            SYLOTI_NAGRI, 
            COMMON_INDIC_NUMBER_FORMS, 
            PHAGS_PA, 
            SAURASHTRA, 
            DEVANAGARI_EXTENDED, 
            KAYAH_LI, 
            REJANG, 
            HANGUL_JAMO_EXTENDED_A, 
            JAVANESE, 
            null, 
            CHAM, 
            MYANMAR_EXTENDED_A, 
            TAI_VIET, 
            MEETEI_MAYEK_EXTENSIONS, 
            ETHIOPIC_EXTENDED_A, 
            null, 
            MEETEI_MAYEK, 
            HANGUL_SYLLABLES, 
            HANGUL_JAMO_EXTENDED_B, 
            HIGH_SURROGATES, 
            HIGH_PRIVATE_USE_SURROGATES, 
            LOW_SURROGATES, 
            PRIVATE_USE_AREA, 
            CJK_COMPATIBILITY_IDEOGRAPHS, 
            ALPHABETIC_PRESENTATION_FORMS, 
            ARABIC_PRESENTATION_FORMS_A, 
            VARIATION_SELECTORS, 
            VERTICAL_FORMS, 
            COMBINING_HALF_MARKS, 
            CJK_COMPATIBILITY_FORMS, 
            SMALL_FORM_VARIANTS, 
            ARABIC_PRESENTATION_FORMS_B, 
            HALFWIDTH_AND_FULLWIDTH_FORMS, 
            SPECIALS, 
            LINEAR_B_SYLLABARY, 
            LINEAR_B_IDEOGRAMS, 
            AEGEAN_NUMBERS, 
            ANCIENT_GREEK_NUMBERS, 
            ANCIENT_SYMBOLS, 
            PHAISTOS_DISC, 
            null, 
            LYCIAN, 
            CARIAN, 
            null, 
            OLD_ITALIC, 
            GOTHIC, 
            null, 
            UGARITIC, 
            OLD_PERSIAN, 
            null, 
            DESERET, 
            SHAVIAN, 
            OSMANYA, 
            null, 
            CYPRIOT_SYLLABARY, 
            IMPERIAL_ARAMAIC, 
            null, 
            PHOENICIAN, 
            LYDIAN, 
            null, 
            MEROITIC_HIEROGLYPHS, 
            MEROITIC_CURSIVE, 
            KHAROSHTHI, 
            OLD_SOUTH_ARABIAN, 
            null, 
            AVESTAN, 
            INSCRIPTIONAL_PARTHIAN, 
            INSCRIPTIONAL_PAHLAVI, 
            null, 
            OLD_TURKIC, 
            null, 
            RUMI_NUMERAL_SYMBOLS, 
            null, 
            BRAHMI, 
            KAITHI, 
            SORA_SOMPENG, 
            CHAKMA, 
            null, 
            SHARADA, 
            null, 
            TAKRI, 
            null, 
            CUNEIFORM, 
            CUNEIFORM_NUMBERS_AND_PUNCTUATION, 
            null, 
            EGYPTIAN_HIEROGLYPHS, 
            null, 
            BAMUM_SUPPLEMENT, 
            null, 
            MIAO, 
            null, 
            KANA_SUPPLEMENT, 
            null, 
            BYZANTINE_MUSICAL_SYMBOLS, 
            MUSICAL_SYMBOLS, 
            ANCIENT_GREEK_MUSICAL_NOTATION, 
            null, 
            TAI_XUAN_JING_SYMBOLS, 
            COUNTING_ROD_NUMERALS, 
            null, 
            MATHEMATICAL_ALPHANUMERIC_SYMBOLS, 
            null, 
            ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS, 
            null, 
            MAHJONG_TILES, 
            DOMINO_TILES, 
            PLAYING_CARDS, 
            ENCLOSED_ALPHANUMERIC_SUPPLEMENT, 
            ENCLOSED_IDEOGRAPHIC_SUPPLEMENT, 
            MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS, 
            EMOTICONS, 
            null, 
            TRANSPORT_AND_MAP_SYMBOLS, 
            ALCHEMICAL_SYMBOLS, 
            null, 
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B, 
            null, 
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C, 
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D, 
            null, 
            CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT, 
            null, 
            TAGS, 
            null, 
            VARIATION_SELECTORS_SUPPLEMENT, 
            null, 
            SUPPLEMENTARY_PRIVATE_USE_AREA_A, 
            SUPPLEMENTARY_PRIVATE_USE_AREA_B
        };


        /**
         * 返回表示包含给定字符的 Unicode 块的对象；如果该字符不是某个定义块的成员, 则返回{@code null}.
         *
         * <p><b>注意:</b> 该方法并不支持<a href="Character.html#supplementary"> 增补字符集</a>.
         * 为了支持包括增补字符集在内的所有的 Unicode字符, 请使用{@link #of(int)}方法.
         *
         * @param   c  要查询的字符
         * @return  表示该字符所属的 Unicode 块的 {@code UnicodeBlock} 实例；如果该字符不是任何 Unicode 块的成员, 则返回{@code null}.
         */
        public static UnicodeBlock of(char c) {
            return of((int)c);
        }

        /**
         * 返回表示包含给定字符的 Unicode 块的对象；如果该字符不是某个定义块的成员, 则返回{@code null}.
         *
         * @param   codePoint 要查询的字符(Unicode 代码点).
         * @return  表示该字符所属的 Unicode 块的 {@code UnicodeBlock} 实例；如果该字符不是任何 Unicode 块的成员, 则返回{@code null}.
         * @exception IllegalArgumentException 如果指定的{@code codePoint}是一个无效的 Unicode 代码点.
         * @see Character#isValidCodePoint(int)
         * @since   1.5
         */
        public static UnicodeBlock of(int codePoint) {
            if (!isValidCodePoint(codePoint)) {
                throw new IllegalArgumentException();
            }

            int top, bottom, current;
            bottom = 0;
            top = blockStarts.length;
            current = top/2;

            // invariant: top > current >= bottom && codePoint >= unicodeBlockStarts[bottom]
            while (top - bottom > 1) {
                if (codePoint >= blockStarts[current]) {
                    bottom = current;
                } else {
                    top = current;
                }
                current = (top + bottom) / 2;
            }
            return blocks[current];
        }

        /**
         * 返回带有给定名称的 UnicodeBlock. 块名由Unicode标准定义.
         * 文件Blocks-&lt;version&gt;.txt定义了特定标准版本的块.
         * {@link Character}类指定支持的标准的版本.
         * <p>
         * 该方法接收下列格式的块名:
         * <ol>
         * <li> Unicode标准中定义的规范化块名称.
         * 例如, Unicode标准定义了一个 "Basic Latin" 块. 因此, 此方法接受"Basic Latin"作为有效的块名称.
         * 每个 UnicodeBlock 的文档都提供一个规范化名称.
         * <li>移除所有空格的规范化块名称.
         * 例如, "BasicLatin"是"Basic Latin"块的一个有效名称.
         * <li>每个常量 UnicodeBlock 标识符的文本表示形式.
         * 例如, 如果提供"BASIC_LATIN"名称, 此方法将返回 {@link #BASIC_LATIN} 块.
         * 这种形式将使用下划线取代规范化名称中的所有空格和连字符.
         * </ol>
         * 最后, 对于所有有效的块名称形式而言, 字符大小写将被忽略.
         * 例如, "BASIC_LATIN" 和 "basic_latin" 都是有效的名称.
         * 采用en_US语言环境的大小写映射规则为块名称验证提供区分大小写的字符串比较.
         * <p>
         * 如果 Unicode 标准更改块名称, 以前和当前的名称都将被接受.
         *
         * @param blockName 一个{@code UnicodeBlock}名称.
         * @return 一个由{@code blockName}标识的 {@code UnicodeBlock}实例.
         * @throws IllegalArgumentException 如果{@code blockName}是一个无效的名称.
         * @throws NullPointerException 如果{@code blockName}为null.
         * @since 1.5
         */
        public static final UnicodeBlock forName(String blockName) {
            UnicodeBlock block = map.get(blockName.toUpperCase(Locale.US));
            if (block == null) {
                throw new IllegalArgumentException();
            }
            return block;
        }
    }


    /**
     * 表示由<a href="http://www.unicode.org/reports/tr24/"><i>Unicode 标准附录 #24: 脚本名称 </i></a>定义的字符脚本的一系列字符子集.
     * 每一个 Unicode 字符都会被分配给一个唯一的Unicode脚本(Unicode 脚本),
     * 或者一个特定的脚本,比如{@link Character.UnicodeScript#LATIN Latin}或者下列三个指定的值:
     * {@link Character.UnicodeScript#INHERITED Inherited}, 
     * {@link Character.UnicodeScript#COMMON Common} 或
     * {@link Character.UnicodeScript#UNKNOWN Unknown}.
     *
     * @since 1.7
     */
    public static enum UnicodeScript {
        /**
         * Unicode脚本 "Common".
         */
        COMMON, 

        /**
         * Unicode 脚本 "Latin".
         */
        LATIN, 

        /**
         * Unicode 脚本 "Greek".
         */
        GREEK, 

        /**
         * Unicode 脚本 "Cyrillic".
         */
        CYRILLIC, 

        /**
         * Unicode 脚本 "Armenian".
         */
        ARMENIAN, 

        /**
         * Unicode 脚本 "Hebrew".
         */
        HEBREW, 

        /**
         * Unicode 脚本 "Arabic".
         */
        ARABIC, 

        /**
         * Unicode 脚本 "Syriac".
         */
        SYRIAC, 

        /**
         * Unicode 脚本 "Thaana".
         */
        THAANA, 

        /**
         * Unicode 脚本 "Devanagari".
         */
        DEVANAGARI, 

        /**
         * Unicode 脚本 "Bengali".
         */
        BENGALI, 

        /**
         * Unicode 脚本 "Gurmukhi".
         */
        GURMUKHI, 

        /**
         * Unicode 脚本 "Gujarati".
         */
        GUJARATI, 

        /**
         * Unicode 脚本 "Oriya".
         */
        ORIYA, 

        /**
         * Unicode 脚本 "Tamil".
         */
        TAMIL, 

        /**
         * Unicode 脚本 "Telugu".
         */
        TELUGU, 

        /**
         * Unicode 脚本 "Kannada".
         */
        KANNADA, 

        /**
         * Unicode 脚本 "Malayalam".
         */
        MALAYALAM, 

        /**
         * Unicode 脚本 "Sinhala".
         */
        SINHALA, 

        /**
         * Unicode 脚本 "Thai".
         */
        THAI, 

        /**
         * Unicode 脚本 "Lao".
         */
        LAO, 

        /**
         * Unicode 脚本 "Tibetan".
         */
        TIBETAN, 

        /**
         * Unicode 脚本 "Myanmar".
         */
        MYANMAR, 

        /**
         * Unicode 脚本 "Georgian".
         */
        GEORGIAN, 

        /**
         * Unicode 脚本 "Hangul".
         */
        HANGUL, 

        /**
         * Unicode 脚本 "Ethiopic".
         */
        ETHIOPIC, 

        /**
         * Unicode 脚本 "Cherokee".
         */
        CHEROKEE, 

        /**
         * Unicode 脚本 "Canadian_Aboriginal".
         */
        CANADIAN_ABORIGINAL, 

        /**
         * Unicode 脚本 "Ogham".
         */
        OGHAM, 

        /**
         * Unicode 脚本 "Runic".
         */
        RUNIC, 

        /**
         * Unicode 脚本 "Khmer".
         */
        KHMER, 

        /**
         * Unicode 脚本 "Mongolian".
         */
        MONGOLIAN, 

        /**
         * Unicode 脚本 "Hiragana".
         */
        HIRAGANA, 

        /**
         * Unicode 脚本 "Katakana".
         */
        KATAKANA, 

        /**
         * Unicode 脚本 "Bopomofo".
         */
        BOPOMOFO, 

        /**
         * Unicode 脚本 "Han".
         */
        HAN, 

        /**
         * Unicode 脚本 "Yi".
         */
        YI, 

        /**
         * Unicode 脚本 "Old_Italic".
         */
        OLD_ITALIC, 

        /**
         * Unicode 脚本 "Gothic".
         */
        GOTHIC, 

        /**
         * Unicode 脚本 "Deseret".
         */
        DESERET, 

        /**
         * Unicode 脚本 "Inherited".
         */
        INHERITED, 

        /**
         * Unicode 脚本 "Tagalog".
         */
        TAGALOG, 

        /**
         * Unicode 脚本 "Hanunoo".
         */
        HANUNOO, 

        /**
         * Unicode 脚本 "Buhid".
         */
        BUHID, 

        /**
         * Unicode 脚本 "Tagbanwa".
         */
        TAGBANWA, 

        /**
         * Unicode 脚本 "Limbu".
         */
        LIMBU, 

        /**
         * Unicode 脚本 "Tai_Le".
         */
        TAI_LE, 

        /**
         * Unicode 脚本 "Linear_B".
         */
        LINEAR_B, 

        /**
         * Unicode 脚本 "Ugaritic".
         */
        UGARITIC, 

        /**
         * Unicode 脚本 "Shavian".
         */
        SHAVIAN, 

        /**
         * Unicode 脚本 "Osmanya".
         */
        OSMANYA, 

        /**
         * Unicode 脚本 "Cypriot".
         */
        CYPRIOT, 

        /**
         * Unicode 脚本 "Braille".
         */
        BRAILLE, 

        /**
         * Unicode 脚本 "Buginese".
         */
        BUGINESE, 

        /**
         * Unicode 脚本 "Coptic".
         */
        COPTIC, 

        /**
         * Unicode 脚本 "New_Tai_Lue".
         */
        NEW_TAI_LUE, 

        /**
         * Unicode 脚本 "Glagolitic".
         */
        GLAGOLITIC, 

        /**
         * Unicode 脚本 "Tifinagh".
         */
        TIFINAGH, 

        /**
         * Unicode 脚本 "Syloti_Nagri".
         */
        SYLOTI_NAGRI, 

        /**
         * Unicode 脚本 "Old_Persian".
         */
        OLD_PERSIAN, 

        /**
         * Unicode 脚本 "Kharoshthi".
         */
        KHAROSHTHI, 

        /**
         * Unicode 脚本 "Balinese".
         */
        BALINESE, 

        /**
         * Unicode 脚本 "Cuneiform".
         */
        CUNEIFORM, 

        /**
         * Unicode 脚本 "Phoenician".
         */
        PHOENICIAN, 

        /**
         * Unicode 脚本 "Phags_Pa".
         */
        PHAGS_PA, 

        /**
         * Unicode 脚本 "Nko".
         */
        NKO, 

        /**
         * Unicode 脚本 "Sundanese".
         */
        SUNDANESE, 

        /**
         * Unicode 脚本 "Batak".
         */
        BATAK, 

        /**
         * Unicode 脚本 "Lepcha".
         */
        LEPCHA, 

        /**
         * Unicode 脚本 "Ol_Chiki".
         */
        OL_CHIKI, 

        /**
         * Unicode 脚本 "Vai".
         */
        VAI, 

        /**
         * Unicode 脚本 "Saurashtra".
         */
        SAURASHTRA, 

        /**
         * Unicode 脚本 "Kayah_Li".
         */
        KAYAH_LI, 

        /**
         * Unicode 脚本 "Rejang".
         */
        REJANG, 

        /**
         * Unicode 脚本 "Lycian".
         */
        LYCIAN, 

        /**
         * Unicode 脚本 "Carian".
         */
        CARIAN, 

        /**
         * Unicode 脚本 "Lydian".
         */
        LYDIAN, 

        /**
         * Unicode 脚本 "Cham".
         */
        CHAM, 

        /**
         * Unicode 脚本 "Tai_Tham".
         */
        TAI_THAM, 

        /**
         * Unicode 脚本 "Tai_Viet".
         */
        TAI_VIET, 

        /**
         * Unicode 脚本 "Avestan".
         */
        AVESTAN, 

        /**
         * Unicode 脚本 "Egyptian_Hieroglyphs".
         */
        EGYPTIAN_HIEROGLYPHS, 

        /**
         * Unicode 脚本 "Samaritan".
         */
        SAMARITAN, 

        /**
         * Unicode 脚本 "Mandaic".
         */
        MANDAIC, 

        /**
         * Unicode 脚本 "Lisu".
         */
        LISU, 

        /**
         * Unicode 脚本 "Bamum".
         */
        BAMUM, 

        /**
         * Unicode 脚本 "Javanese".
         */
        JAVANESE, 

        /**
         * Unicode 脚本 "Meetei_Mayek".
         */
        MEETEI_MAYEK, 

        /**
         * Unicode 脚本 "Imperial_Aramaic".
         */
        IMPERIAL_ARAMAIC, 

        /**
         * Unicode 脚本 "Old_South_Arabian".
         */
        OLD_SOUTH_ARABIAN, 

        /**
         * Unicode 脚本 "Inscriptional_Parthian".
         */
        INSCRIPTIONAL_PARTHIAN, 

        /**
         * Unicode 脚本 "Inscriptional_Pahlavi".
         */
        INSCRIPTIONAL_PAHLAVI, 

        /**
         * Unicode 脚本 "Old_Turkic".
         */
        OLD_TURKIC, 

        /**
         * Unicode 脚本 "Brahmi".
         */
        BRAHMI, 

        /**
         * Unicode 脚本 "Kaithi".
         */
        KAITHI, 

        /**
         * Unicode 脚本 "Meroitic Hieroglyphs".
         */
        MEROITIC_HIEROGLYPHS, 

        /**
         * Unicode 脚本 "Meroitic Cursive".
         */
        MEROITIC_CURSIVE, 

        /**
         * Unicode 脚本 "Sora Sompeng".
         */
        SORA_SOMPENG, 

        /**
         * Unicode 脚本 "Chakma".
         */
        CHAKMA, 

        /**
         * Unicode 脚本 "Sharada".
         */
        SHARADA, 

        /**
         * Unicode 脚本 "Takri".
         */
        TAKRI, 

        /**
         * Unicode 脚本 "Miao".
         */
        MIAO, 

        /**
         * Unicode 脚本 "Unknown".
         */
        UNKNOWN;

        private static final int[] scriptStarts = {
            0x0000,   // 0000..0040; COMMON
            0x0041,   // 0041..005A; LATIN
            0x005B,   // 005B..0060; COMMON
            0x0061,   // 0061..007A; LATIN
            0x007B,   // 007B..00A9; COMMON
            0x00AA,   // 00AA..00AA; LATIN
            0x00AB,   // 00AB..00B9; COMMON
            0x00BA,   // 00BA..00BA; LATIN
            0x00BB,   // 00BB..00BF; COMMON
            0x00C0,   // 00C0..00D6; LATIN
            0x00D7,   // 00D7..00D7; COMMON
            0x00D8,   // 00D8..00F6; LATIN
            0x00F7,   // 00F7..00F7; COMMON
            0x00F8,   // 00F8..02B8; LATIN
            0x02B9,   // 02B9..02DF; COMMON
            0x02E0,   // 02E0..02E4; LATIN
            0x02E5,   // 02E5..02E9; COMMON
            0x02EA,   // 02EA..02EB; BOPOMOFO
            0x02EC,   // 02EC..02FF; COMMON
            0x0300,   // 0300..036F; INHERITED
            0x0370,   // 0370..0373; GREEK
            0x0374,   // 0374..0374; COMMON
            0x0375,   // 0375..037D; GREEK
            0x037E,   // 037E..0383; COMMON
            0x0384,   // 0384..0384; GREEK
            0x0385,   // 0385..0385; COMMON
            0x0386,   // 0386..0386; GREEK
            0x0387,   // 0387..0387; COMMON
            0x0388,   // 0388..03E1; GREEK
            0x03E2,   // 03E2..03EF; COPTIC
            0x03F0,   // 03F0..03FF; GREEK
            0x0400,   // 0400..0484; CYRILLIC
            0x0485,   // 0485..0486; INHERITED
            0x0487,   // 0487..0530; CYRILLIC
            0x0531,   // 0531..0588; ARMENIAN
            0x0589,   // 0589..0589; COMMON
            0x058A,   // 058A..0590; ARMENIAN
            0x0591,   // 0591..05FF; HEBREW
            0x0600,   // 0600..060B; ARABIC
            0x060C,   // 060C..060C; COMMON
            0x060D,   // 060D..061A; ARABIC
            0x061B,   // 061B..061D; COMMON
            0x061E,   // 061E..061E; ARABIC
            0x061F,   // 061F..061F; COMMON
            0x0620,   // 0620..063F; ARABIC
            0x0640,   // 0640..0640; COMMON
            0x0641,   // 0641..064A; ARABIC
            0x064B,   // 064B..0655; INHERITED
            0x0656,   // 0656..065F; ARABIC
            0x0660,   // 0660..0669; COMMON
            0x066A,   // 066A..066F; ARABIC
            0x0670,   // 0670..0670; INHERITED
            0x0671,   // 0671..06DC; ARABIC
            0x06DD,   // 06DD..06DD; COMMON
            0x06DE,   // 06DE..06FF; ARABIC
            0x0700,   // 0700..074F; SYRIAC
            0x0750,   // 0750..077F; ARABIC
            0x0780,   // 0780..07BF; THAANA
            0x07C0,   // 07C0..07FF; NKO
            0x0800,   // 0800..083F; SAMARITAN
            0x0840,   // 0840..089F; MANDAIC
            0x08A0,   // 08A0..08FF; ARABIC
            0x0900,   // 0900..0950; DEVANAGARI
            0x0951,   // 0951..0952; INHERITED
            0x0953,   // 0953..0963; DEVANAGARI
            0x0964,   // 0964..0965; COMMON
            0x0966,   // 0966..0980; DEVANAGARI
            0x0981,   // 0981..0A00; BENGALI
            0x0A01,   // 0A01..0A80; GURMUKHI
            0x0A81,   // 0A81..0B00; GUJARATI
            0x0B01,   // 0B01..0B81; ORIYA
            0x0B82,   // 0B82..0C00; TAMIL
            0x0C01,   // 0C01..0C81; TELUGU
            0x0C82,   // 0C82..0CF0; KANNADA
            0x0D02,   // 0D02..0D81; MALAYALAM
            0x0D82,   // 0D82..0E00; SINHALA
            0x0E01,   // 0E01..0E3E; THAI
            0x0E3F,   // 0E3F..0E3F; COMMON
            0x0E40,   // 0E40..0E80; THAI
            0x0E81,   // 0E81..0EFF; LAO
            0x0F00,   // 0F00..0FD4; TIBETAN
            0x0FD5,   // 0FD5..0FD8; COMMON
            0x0FD9,   // 0FD9..0FFF; TIBETAN
            0x1000,   // 1000..109F; MYANMAR
            0x10A0,   // 10A0..10FA; GEORGIAN
            0x10FB,   // 10FB..10FB; COMMON
            0x10FC,   // 10FC..10FF; GEORGIAN
            0x1100,   // 1100..11FF; HANGUL
            0x1200,   // 1200..139F; ETHIOPIC
            0x13A0,   // 13A0..13FF; CHEROKEE
            0x1400,   // 1400..167F; CANADIAN_ABORIGINAL
            0x1680,   // 1680..169F; OGHAM
            0x16A0,   // 16A0..16EA; RUNIC
            0x16EB,   // 16EB..16ED; COMMON
            0x16EE,   // 16EE..16FF; RUNIC
            0x1700,   // 1700..171F; TAGALOG
            0x1720,   // 1720..1734; HANUNOO
            0x1735,   // 1735..173F; COMMON
            0x1740,   // 1740..175F; BUHID
            0x1760,   // 1760..177F; TAGBANWA
            0x1780,   // 1780..17FF; KHMER
            0x1800,   // 1800..1801; MONGOLIAN
            0x1802,   // 1802..1803; COMMON
            0x1804,   // 1804..1804; MONGOLIAN
            0x1805,   // 1805..1805; COMMON
            0x1806,   // 1806..18AF; MONGOLIAN
            0x18B0,   // 18B0..18FF; CANADIAN_ABORIGINAL
            0x1900,   // 1900..194F; LIMBU
            0x1950,   // 1950..197F; TAI_LE
            0x1980,   // 1980..19DF; NEW_TAI_LUE
            0x19E0,   // 19E0..19FF; KHMER
            0x1A00,   // 1A00..1A1F; BUGINESE
            0x1A20,   // 1A20..1AFF; TAI_THAM
            0x1B00,   // 1B00..1B7F; BALINESE
            0x1B80,   // 1B80..1BBF; SUNDANESE
            0x1BC0,   // 1BC0..1BFF; BATAK
            0x1C00,   // 1C00..1C4F; LEPCHA
            0x1C50,   // 1C50..1CBF; OL_CHIKI
            0x1CC0,   // 1CC0..1CCF; SUNDANESE
            0x1CD0,   // 1CD0..1CD2; INHERITED
            0x1CD3,   // 1CD3..1CD3; COMMON
            0x1CD4,   // 1CD4..1CE0; INHERITED
            0x1CE1,   // 1CE1..1CE1; COMMON
            0x1CE2,   // 1CE2..1CE8; INHERITED
            0x1CE9,   // 1CE9..1CEC; COMMON
            0x1CED,   // 1CED..1CED; INHERITED
            0x1CEE,   // 1CEE..1CF3; COMMON
            0x1CF4,   // 1CF4..1CF4; INHERITED
            0x1CF5,   // 1CF5..1CFF; COMMON
            0x1D00,   // 1D00..1D25; LATIN
            0x1D26,   // 1D26..1D2A; GREEK
            0x1D2B,   // 1D2B..1D2B; CYRILLIC
            0x1D2C,   // 1D2C..1D5C; LATIN
            0x1D5D,   // 1D5D..1D61; GREEK
            0x1D62,   // 1D62..1D65; LATIN
            0x1D66,   // 1D66..1D6A; GREEK
            0x1D6B,   // 1D6B..1D77; LATIN
            0x1D78,   // 1D78..1D78; CYRILLIC
            0x1D79,   // 1D79..1DBE; LATIN
            0x1DBF,   // 1DBF..1DBF; GREEK
            0x1DC0,   // 1DC0..1DFF; INHERITED
            0x1E00,   // 1E00..1EFF; LATIN
            0x1F00,   // 1F00..1FFF; GREEK
            0x2000,   // 2000..200B; COMMON
            0x200C,   // 200C..200D; INHERITED
            0x200E,   // 200E..2070; COMMON
            0x2071,   // 2071..2073; LATIN
            0x2074,   // 2074..207E; COMMON
            0x207F,   // 207F..207F; LATIN
            0x2080,   // 2080..208F; COMMON
            0x2090,   // 2090..209F; LATIN
            0x20A0,   // 20A0..20CF; COMMON
            0x20D0,   // 20D0..20FF; INHERITED
            0x2100,   // 2100..2125; COMMON
            0x2126,   // 2126..2126; GREEK
            0x2127,   // 2127..2129; COMMON
            0x212A,   // 212A..212B; LATIN
            0x212C,   // 212C..2131; COMMON
            0x2132,   // 2132..2132; LATIN
            0x2133,   // 2133..214D; COMMON
            0x214E,   // 214E..214E; LATIN
            0x214F,   // 214F..215F; COMMON
            0x2160,   // 2160..2188; LATIN
            0x2189,   // 2189..27FF; COMMON
            0x2800,   // 2800..28FF; BRAILLE
            0x2900,   // 2900..2BFF; COMMON
            0x2C00,   // 2C00..2C5F; GLAGOLITIC
            0x2C60,   // 2C60..2C7F; LATIN
            0x2C80,   // 2C80..2CFF; COPTIC
            0x2D00,   // 2D00..2D2F; GEORGIAN
            0x2D30,   // 2D30..2D7F; TIFINAGH
            0x2D80,   // 2D80..2DDF; ETHIOPIC
            0x2DE0,   // 2DE0..2DFF; CYRILLIC
            0x2E00,   // 2E00..2E7F; COMMON
            0x2E80,   // 2E80..2FEF; HAN
            0x2FF0,   // 2FF0..3004; COMMON
            0x3005,   // 3005..3005; HAN
            0x3006,   // 3006..3006; COMMON
            0x3007,   // 3007..3007; HAN
            0x3008,   // 3008..3020; COMMON
            0x3021,   // 3021..3029; HAN
            0x302A,   // 302A..302D; INHERITED
            0x302E,   // 302E..302F; HANGUL
            0x3030,   // 3030..3037; COMMON
            0x3038,   // 3038..303B; HAN
            0x303C,   // 303C..3040; COMMON
            0x3041,   // 3041..3098; HIRAGANA
            0x3099,   // 3099..309A; INHERITED
            0x309B,   // 309B..309C; COMMON
            0x309D,   // 309D..309F; HIRAGANA
            0x30A0,   // 30A0..30A0; COMMON
            0x30A1,   // 30A1..30FA; KATAKANA
            0x30FB,   // 30FB..30FC; COMMON
            0x30FD,   // 30FD..3104; KATAKANA
            0x3105,   // 3105..3130; BOPOMOFO
            0x3131,   // 3131..318F; HANGUL
            0x3190,   // 3190..319F; COMMON
            0x31A0,   // 31A0..31BF; BOPOMOFO
            0x31C0,   // 31C0..31EF; COMMON
            0x31F0,   // 31F0..31FF; KATAKANA
            0x3200,   // 3200..321F; HANGUL
            0x3220,   // 3220..325F; COMMON
            0x3260,   // 3260..327E; HANGUL
            0x327F,   // 327F..32CF; COMMON
            0x32D0,   // 32D0..3357; KATAKANA
            0x3358,   // 3358..33FF; COMMON
            0x3400,   // 3400..4DBF; HAN
            0x4DC0,   // 4DC0..4DFF; COMMON
            0x4E00,   // 4E00..9FFF; HAN
            0xA000,   // A000..A4CF; YI
            0xA4D0,   // A4D0..A4FF; LISU
            0xA500,   // A500..A63F; VAI
            0xA640,   // A640..A69F; CYRILLIC
            0xA6A0,   // A6A0..A6FF; BAMUM
            0xA700,   // A700..A721; COMMON
            0xA722,   // A722..A787; LATIN
            0xA788,   // A788..A78A; COMMON
            0xA78B,   // A78B..A7FF; LATIN
            0xA800,   // A800..A82F; SYLOTI_NAGRI
            0xA830,   // A830..A83F; COMMON
            0xA840,   // A840..A87F; PHAGS_PA
            0xA880,   // A880..A8DF; SAURASHTRA
            0xA8E0,   // A8E0..A8FF; DEVANAGARI
            0xA900,   // A900..A92F; KAYAH_LI
            0xA930,   // A930..A95F; REJANG
            0xA960,   // A960..A97F; HANGUL
            0xA980,   // A980..A9FF; JAVANESE
            0xAA00,   // AA00..AA5F; CHAM
            0xAA60,   // AA60..AA7F; MYANMAR
            0xAA80,   // AA80..AADF; TAI_VIET
            0xAAE0,   // AAE0..AB00; MEETEI_MAYEK
            0xAB01,   // AB01..ABBF; ETHIOPIC
            0xABC0,   // ABC0..ABFF; MEETEI_MAYEK
            0xAC00,   // AC00..D7FB; HANGUL
            0xD7FC,   // D7FC..F8FF; UNKNOWN
            0xF900,   // F900..FAFF; HAN
            0xFB00,   // FB00..FB12; LATIN
            0xFB13,   // FB13..FB1C; ARMENIAN
            0xFB1D,   // FB1D..FB4F; HEBREW
            0xFB50,   // FB50..FD3D; ARABIC
            0xFD3E,   // FD3E..FD4F; COMMON
            0xFD50,   // FD50..FDFC; ARABIC
            0xFDFD,   // FDFD..FDFF; COMMON
            0xFE00,   // FE00..FE0F; INHERITED
            0xFE10,   // FE10..FE1F; COMMON
            0xFE20,   // FE20..FE2F; INHERITED
            0xFE30,   // FE30..FE6F; COMMON
            0xFE70,   // FE70..FEFE; ARABIC
            0xFEFF,   // FEFF..FF20; COMMON
            0xFF21,   // FF21..FF3A; LATIN
            0xFF3B,   // FF3B..FF40; COMMON
            0xFF41,   // FF41..FF5A; LATIN
            0xFF5B,   // FF5B..FF65; COMMON
            0xFF66,   // FF66..FF6F; KATAKANA
            0xFF70,   // FF70..FF70; COMMON
            0xFF71,   // FF71..FF9D; KATAKANA
            0xFF9E,   // FF9E..FF9F; COMMON
            0xFFA0,   // FFA0..FFDF; HANGUL
            0xFFE0,   // FFE0..FFFF; COMMON
            0x10000,  // 10000..100FF; LINEAR_B
            0x10100,  // 10100..1013F; COMMON
            0x10140,  // 10140..1018F; GREEK
            0x10190,  // 10190..101FC; COMMON
            0x101FD,  // 101FD..1027F; INHERITED
            0x10280,  // 10280..1029F; LYCIAN
            0x102A0,  // 102A0..102FF; CARIAN
            0x10300,  // 10300..1032F; OLD_ITALIC
            0x10330,  // 10330..1037F; GOTHIC
            0x10380,  // 10380..1039F; UGARITIC
            0x103A0,  // 103A0..103FF; OLD_PERSIAN
            0x10400,  // 10400..1044F; DESERET
            0x10450,  // 10450..1047F; SHAVIAN
            0x10480,  // 10480..107FF; OSMANYA
            0x10800,  // 10800..1083F; CYPRIOT
            0x10840,  // 10840..108FF; IMPERIAL_ARAMAIC
            0x10900,  // 10900..1091F; PHOENICIAN
            0x10920,  // 10920..1097F; LYDIAN
            0x10980,  // 10980..1099F; MEROITIC_HIEROGLYPHS
            0x109A0,  // 109A0..109FF; MEROITIC_CURSIVE
            0x10A00,  // 10A00..10A5F; KHAROSHTHI
            0x10A60,  // 10A60..10AFF; OLD_SOUTH_ARABIAN
            0x10B00,  // 10B00..10B3F; AVESTAN
            0x10B40,  // 10B40..10B5F; INSCRIPTIONAL_PARTHIAN
            0x10B60,  // 10B60..10BFF; INSCRIPTIONAL_PAHLAVI
            0x10C00,  // 10C00..10E5F; OLD_TURKIC
            0x10E60,  // 10E60..10FFF; ARABIC
            0x11000,  // 11000..1107F; BRAHMI
            0x11080,  // 11080..110CF; KAITHI
            0x110D0,  // 110D0..110FF; SORA_SOMPENG
            0x11100,  // 11100..1117F; CHAKMA
            0x11180,  // 11180..1167F; SHARADA
            0x11680,  // 11680..116CF; TAKRI
            0x12000,  // 12000..12FFF; CUNEIFORM
            0x13000,  // 13000..167FF; EGYPTIAN_HIEROGLYPHS
            0x16800,  // 16800..16A38; BAMUM
            0x16F00,  // 16F00..16F9F; MIAO
            0x1B000,  // 1B000..1B000; KATAKANA
            0x1B001,  // 1B001..1CFFF; HIRAGANA
            0x1D000,  // 1D000..1D166; COMMON
            0x1D167,  // 1D167..1D169; INHERITED
            0x1D16A,  // 1D16A..1D17A; COMMON
            0x1D17B,  // 1D17B..1D182; INHERITED
            0x1D183,  // 1D183..1D184; COMMON
            0x1D185,  // 1D185..1D18B; INHERITED
            0x1D18C,  // 1D18C..1D1A9; COMMON
            0x1D1AA,  // 1D1AA..1D1AD; INHERITED
            0x1D1AE,  // 1D1AE..1D1FF; COMMON
            0x1D200,  // 1D200..1D2FF; GREEK
            0x1D300,  // 1D300..1EDFF; COMMON
            0x1EE00,  // 1EE00..1EFFF; ARABIC
            0x1F000,  // 1F000..1F1FF; COMMON
            0x1F200,  // 1F200..1F200; HIRAGANA
            0x1F201,  // 1F210..1FFFF; COMMON
            0x20000,  // 20000..E0000; HAN
            0xE0001,  // E0001..E00FF; COMMON
            0xE0100,  // E0100..E01EF; INHERITED
            0xE01F0   // E01F0..10FFFF; UNKNOWN

        };

        private static final UnicodeScript[] scripts = {
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            BOPOMOFO, 
            COMMON, 
            INHERITED, 
            GREEK, 
            COMMON, 
            GREEK, 
            COMMON, 
            GREEK, 
            COMMON, 
            GREEK, 
            COMMON, 
            GREEK, 
            COPTIC, 
            GREEK, 
            CYRILLIC, 
            INHERITED, 
            CYRILLIC, 
            ARMENIAN, 
            COMMON, 
            ARMENIAN, 
            HEBREW, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            INHERITED, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            INHERITED, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            SYRIAC, 
            ARABIC, 
            THAANA, 
            NKO, 
            SAMARITAN, 
            MANDAIC, 
            ARABIC, 
            DEVANAGARI, 
            INHERITED, 
            DEVANAGARI, 
            COMMON, 
            DEVANAGARI, 
            BENGALI, 
            GURMUKHI, 
            GUJARATI, 
            ORIYA, 
            TAMIL, 
            TELUGU, 
            KANNADA, 
            MALAYALAM, 
            SINHALA, 
            THAI, 
            COMMON, 
            THAI, 
            LAO, 
            TIBETAN, 
            COMMON, 
            TIBETAN, 
            MYANMAR, 
            GEORGIAN, 
            COMMON, 
            GEORGIAN, 
            HANGUL, 
            ETHIOPIC, 
            CHEROKEE, 
            CANADIAN_ABORIGINAL, 
            OGHAM, 
            RUNIC, 
            COMMON, 
            RUNIC, 
            TAGALOG, 
            HANUNOO, 
            COMMON, 
            BUHID, 
            TAGBANWA, 
            KHMER, 
            MONGOLIAN, 
            COMMON, 
            MONGOLIAN, 
            COMMON, 
            MONGOLIAN, 
            CANADIAN_ABORIGINAL, 
            LIMBU, 
            TAI_LE, 
            NEW_TAI_LUE, 
            KHMER, 
            BUGINESE, 
            TAI_THAM, 
            BALINESE, 
            SUNDANESE, 
            BATAK, 
            LEPCHA, 
            OL_CHIKI, 
            SUNDANESE, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            LATIN, 
            GREEK, 
            CYRILLIC, 
            LATIN, 
            GREEK, 
            LATIN, 
            GREEK, 
            LATIN, 
            CYRILLIC, 
            LATIN, 
            GREEK, 
            INHERITED, 
            LATIN, 
            GREEK, 
            COMMON, 
            INHERITED, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            INHERITED, 
            COMMON, 
            GREEK, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            BRAILLE, 
            COMMON, 
            GLAGOLITIC, 
            LATIN, 
            COPTIC, 
            GEORGIAN, 
            TIFINAGH, 
            ETHIOPIC, 
            CYRILLIC, 
            COMMON, 
            HAN, 
            COMMON, 
            HAN, 
            COMMON, 
            HAN, 
            COMMON, 
            HAN, 
            INHERITED, 
            HANGUL, 
            COMMON, 
            HAN, 
            COMMON, 
            HIRAGANA, 
            INHERITED, 
            COMMON, 
            HIRAGANA, 
            COMMON, 
            KATAKANA, 
            COMMON, 
            KATAKANA, 
            BOPOMOFO, 
            HANGUL, 
            COMMON, 
            BOPOMOFO, 
            COMMON, 
            KATAKANA, 
            HANGUL, 
            COMMON, 
            HANGUL, 
            COMMON, 
            KATAKANA, 
            COMMON, 
            HAN, 
            COMMON, 
            HAN, 
            YI, 
            LISU, 
            VAI, 
            CYRILLIC, 
            BAMUM, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            SYLOTI_NAGRI, 
            COMMON, 
            PHAGS_PA, 
            SAURASHTRA, 
            DEVANAGARI, 
            KAYAH_LI, 
            REJANG, 
            HANGUL, 
            JAVANESE, 
            CHAM, 
            MYANMAR, 
            TAI_VIET, 
            MEETEI_MAYEK, 
            ETHIOPIC, 
            MEETEI_MAYEK, 
            HANGUL, 
            UNKNOWN     , 
            HAN, 
            LATIN, 
            ARMENIAN, 
            HEBREW, 
            ARABIC, 
            COMMON, 
            ARABIC, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            ARABIC, 
            COMMON, 
            LATIN, 
            COMMON, 
            LATIN, 
            COMMON, 
            KATAKANA, 
            COMMON, 
            KATAKANA, 
            COMMON, 
            HANGUL, 
            COMMON, 
            LINEAR_B, 
            COMMON, 
            GREEK, 
            COMMON, 
            INHERITED, 
            LYCIAN, 
            CARIAN, 
            OLD_ITALIC, 
            GOTHIC, 
            UGARITIC, 
            OLD_PERSIAN, 
            DESERET, 
            SHAVIAN, 
            OSMANYA, 
            CYPRIOT, 
            IMPERIAL_ARAMAIC, 
            PHOENICIAN, 
            LYDIAN, 
            MEROITIC_HIEROGLYPHS, 
            MEROITIC_CURSIVE, 
            KHAROSHTHI, 
            OLD_SOUTH_ARABIAN, 
            AVESTAN, 
            INSCRIPTIONAL_PARTHIAN, 
            INSCRIPTIONAL_PAHLAVI, 
            OLD_TURKIC, 
            ARABIC, 
            BRAHMI, 
            KAITHI, 
            SORA_SOMPENG, 
            CHAKMA, 
            SHARADA, 
            TAKRI, 
            CUNEIFORM, 
            EGYPTIAN_HIEROGLYPHS, 
            BAMUM, 
            MIAO, 
            KATAKANA, 
            HIRAGANA, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            INHERITED, 
            COMMON, 
            GREEK, 
            COMMON, 
            ARABIC, 
            COMMON, 
            HIRAGANA, 
            COMMON, 
            HAN, 
            COMMON, 
            INHERITED, 
            UNKNOWN
        };

        private static HashMap<String, Character.UnicodeScript> aliases;
        static {
            aliases = new HashMap<>(128);
            aliases.put("ARAB", ARABIC);
            aliases.put("ARMI", IMPERIAL_ARAMAIC);
            aliases.put("ARMN", ARMENIAN);
            aliases.put("AVST", AVESTAN);
            aliases.put("BALI", BALINESE);
            aliases.put("BAMU", BAMUM);
            aliases.put("BATK", BATAK);
            aliases.put("BENG", BENGALI);
            aliases.put("BOPO", BOPOMOFO);
            aliases.put("BRAI", BRAILLE);
            aliases.put("BRAH", BRAHMI);
            aliases.put("BUGI", BUGINESE);
            aliases.put("BUHD", BUHID);
            aliases.put("CAKM", CHAKMA);
            aliases.put("CANS", CANADIAN_ABORIGINAL);
            aliases.put("CARI", CARIAN);
            aliases.put("CHAM", CHAM);
            aliases.put("CHER", CHEROKEE);
            aliases.put("COPT", COPTIC);
            aliases.put("CPRT", CYPRIOT);
            aliases.put("CYRL", CYRILLIC);
            aliases.put("DEVA", DEVANAGARI);
            aliases.put("DSRT", DESERET);
            aliases.put("EGYP", EGYPTIAN_HIEROGLYPHS);
            aliases.put("ETHI", ETHIOPIC);
            aliases.put("GEOR", GEORGIAN);
            aliases.put("GLAG", GLAGOLITIC);
            aliases.put("GOTH", GOTHIC);
            aliases.put("GREK", GREEK);
            aliases.put("GUJR", GUJARATI);
            aliases.put("GURU", GURMUKHI);
            aliases.put("HANG", HANGUL);
            aliases.put("HANI", HAN);
            aliases.put("HANO", HANUNOO);
            aliases.put("HEBR", HEBREW);
            aliases.put("HIRA", HIRAGANA);
            // it appears we don't have the KATAKANA_OR_HIRAGANA
            //aliases.put("HRKT", KATAKANA_OR_HIRAGANA);
            aliases.put("ITAL", OLD_ITALIC);
            aliases.put("JAVA", JAVANESE);
            aliases.put("KALI", KAYAH_LI);
            aliases.put("KANA", KATAKANA);
            aliases.put("KHAR", KHAROSHTHI);
            aliases.put("KHMR", KHMER);
            aliases.put("KNDA", KANNADA);
            aliases.put("KTHI", KAITHI);
            aliases.put("LANA", TAI_THAM);
            aliases.put("LAOO", LAO);
            aliases.put("LATN", LATIN);
            aliases.put("LEPC", LEPCHA);
            aliases.put("LIMB", LIMBU);
            aliases.put("LINB", LINEAR_B);
            aliases.put("LISU", LISU);
            aliases.put("LYCI", LYCIAN);
            aliases.put("LYDI", LYDIAN);
            aliases.put("MAND", MANDAIC);
            aliases.put("MERC", MEROITIC_CURSIVE);
            aliases.put("MERO", MEROITIC_HIEROGLYPHS);
            aliases.put("MLYM", MALAYALAM);
            aliases.put("MONG", MONGOLIAN);
            aliases.put("MTEI", MEETEI_MAYEK);
            aliases.put("MYMR", MYANMAR);
            aliases.put("NKOO", NKO);
            aliases.put("OGAM", OGHAM);
            aliases.put("OLCK", OL_CHIKI);
            aliases.put("ORKH", OLD_TURKIC);
            aliases.put("ORYA", ORIYA);
            aliases.put("OSMA", OSMANYA);
            aliases.put("PHAG", PHAGS_PA);
            aliases.put("PLRD", MIAO);
            aliases.put("PHLI", INSCRIPTIONAL_PAHLAVI);
            aliases.put("PHNX", PHOENICIAN);
            aliases.put("PRTI", INSCRIPTIONAL_PARTHIAN);
            aliases.put("RJNG", REJANG);
            aliases.put("RUNR", RUNIC);
            aliases.put("SAMR", SAMARITAN);
            aliases.put("SARB", OLD_SOUTH_ARABIAN);
            aliases.put("SAUR", SAURASHTRA);
            aliases.put("SHAW", SHAVIAN);
            aliases.put("SHRD", SHARADA);
            aliases.put("SINH", SINHALA);
            aliases.put("SORA", SORA_SOMPENG);
            aliases.put("SUND", SUNDANESE);
            aliases.put("SYLO", SYLOTI_NAGRI);
            aliases.put("SYRC", SYRIAC);
            aliases.put("TAGB", TAGBANWA);
            aliases.put("TALE", TAI_LE);
            aliases.put("TAKR", TAKRI);
            aliases.put("TALU", NEW_TAI_LUE);
            aliases.put("TAML", TAMIL);
            aliases.put("TAVT", TAI_VIET);
            aliases.put("TELU", TELUGU);
            aliases.put("TFNG", TIFINAGH);
            aliases.put("TGLG", TAGALOG);
            aliases.put("THAA", THAANA);
            aliases.put("THAI", THAI);
            aliases.put("TIBT", TIBETAN);
            aliases.put("UGAR", UGARITIC);
            aliases.put("VAII", VAI);
            aliases.put("XPEO", OLD_PERSIAN);
            aliases.put("XSUX", CUNEIFORM);
            aliases.put("YIII", YI);
            aliases.put("ZINH", INHERITED);
            aliases.put("ZYYY", COMMON);
            aliases.put("ZZZZ", UNKNOWN);
        }

        /**
         * 返回给定字符(Unicode 代码点)所分配的Unicode 脚本的枚举常量.
         *
         * @param   codePoint 要查询的字符(Unicode 代码点).
         * @return  给定字符(Unicode 代码点)所分配的{@code UnicodeScript}常量.
         *
         * @exception IllegalArgumentException 如果指定的{@code codePoint}是个无效的Unicode代码点.
         * @see Character#isValidCodePoint(int)
         *
         */
        public static UnicodeScript of(int codePoint) {
            if (!isValidCodePoint(codePoint))
                throw new IllegalArgumentException();
            int type = getType(codePoint);
            // leave SURROGATE and PRIVATE_USE for table lookup
            if (type == UNASSIGNED)
                return UNKNOWN;
            int index = Arrays.binarySearch(scriptStarts, codePoint);
            if (index < 0)
                index = -index - 2;
            return scripts[index];
        }

        /**
         * 通过给定的Unicode脚本名称或者脚本别名,返回UnicodeScript常量.
         * 脚本名称以及他们的别名由Unicode标准定义.
         * 文件Scripts&lt;version&gt;.txt以及PropertyValueAliases&lt;version&gt;.txt定义了
         * 特定标准版本的脚本名称和别名名称.
         * {@link Character}类指定支持的标准的版本.
         * <p>
         * 对于所有有效的脚本名称形式而言, 字符大小写将被忽略.
         * 采用en_US语言环境的大小写映射规则为块名称验证提供区分大小写的字符串比较.
         * <p>
         *
         * @param scriptName {@code UnicodeScript}名称.
         * @return {@code scriptName}标识的 {@code UnicodeScript}常量.
         * @throws IllegalArgumentException 如果{@code scriptName}是一个无效的名称.
         * @throws NullPointerException 如果{@code scriptName}为null.
         */
        public static final UnicodeScript forName(String scriptName) {
            scriptName = scriptName.toUpperCase(Locale.ENGLISH);
                                 //.replace(' ', '_'));
            UnicodeScript sc = aliases.get(scriptName);
            if (sc != null)
                return sc;
            return valueOf(scriptName);
        }
    }

    /**
     * {@code Character}的值.
     *
     * @serial
     */
    private final char value;

    /** 使用来自JDK 1.0.2的serialVersionUID, 以便兼容 */
    private static final long serialVersionUID = 3786198910865385080L;

    /**
     * 构造一个新分配的{@code Character}对象, 以表示指定的{@code char}值.
     *
     * @param  value   {@code Character}表示的值.
     */
    public Character(char value) {
        this.value = value;
    }

    private static class CharacterCache {
        private CharacterCache(){}

        static final Character cache[] = new Character[127 + 1];

        static {
            for (int i = 0; i < cache.length; i++)
                cache[i] = new Character((char)i);
        }
    }

    /**
     * 返回一个表示指定<tt>char</tt>值的<tt>Character</tt>实例.
     * 如果不需要一个新的<tt>Character</tt>实例, 则通常应该优先采用此方法,而不是构造方法{@link #Character(char)},
     * 因为该方法可能通过缓存经常请求的值来显著提高空间和时间性能.
     *
     * 该方法会缓存 {@code '\u005Cu0000'} 到  {@code '\u005Cu007F'}之间的字符,
     * 可能会缓存这个范围之外的字符.
     *
     * @param  c 字符值.
     * @return 代表<tt>c</tt>的<tt>Character</tt>实例.
     * @since  1.5
     */
    public static Character valueOf(char c) {
        if (c <= 127) { // must cache
            return CharacterCache.cache[(int)c];
        }
        return new Character(c);
    }

    /**
     * 返回该{@code Character}对象的值.
     * @return  该对象表示的基本{@code char}值.
     */
    public char charValue() {
        return value;
    }

    /**
     * 返回当前{@code Character}对象的hash code; 返回值等价于请求{@code charValue()}的返回值.
     *
     * @return 当前{@code Character}对象的hash code值.
     */
    @Override
    public int hashCode() {
        return Character.hashCode(value);
    }

    /**
     * 返回指定{@code char}值的hash code; 等同于{@code Character.hashCode()}.
     *
     * @since 1.8
     *
     * @param value 需要去返回hash code的{@code char}.
     * @return 指定的{@code char}值的hash code值.
     */
    public static int hashCode(char value) {
        return (int)value;
    }

    /**
     * 将当前对象与指定对象进行对比.
     * 当且仅当参数非{@code null}并且是一个和当前对象有相同的{@code char}值的{@code Character}对象时,
     * 结果为{@code true}.
     *
     * @param   obj   要比较的对象.
     * @return  如果是同一个对象,返回{@code true};
     *          否则返回{@code false} .
     */
    public boolean equals(Object obj) {
        if (obj instanceof Character) {
            return value == ((Character)obj).charValue();
        }
        return false;
    }

    /**
     * 返回一个代表该{@code Character}的值的{@code String}对象.
     * 返回结果是一个长度为1的字符串,该字符串仅由当前{@code Character}对象代表的基本{@code char}值组成.
     *
     * @return  代表当前对象的字符串.
     */
    public String toString() {
        char buf[] = {value};
        return String.valueOf(buf);
    }

    /**
     * 返回一个代表指定{@code char}的{@code String}对象.
     * 返回结果是一个仅包含指定{@code char}的长度为1的字符串.
     *
     * @param c 待转换的{@code char}.
     * @return 代表指定{@code char}的字符串.
     * @since 1.4
     */
    public static String toString(char c) {
        return String.valueOf(c);
    }

    /**
     * 确定指定的字符是否是一个有效的<a href="http://www.unicode.org/glossary/#code_point">Unicode代码点值</a>.
     * (译者注:即是否是一个有效的Unicode字符)
     * @param  codePoint 要测试的Unicode代码点
     * @return 如果指定的代码点在{@link #MIN_CODE_POINT}和{@link #MAX_CODE_POINT}之间, 返回{@code true};
     *         否则返回{@code false}.
     * @since  1.5
     */
    public static boolean isValidCodePoint(int codePoint) {
        // Optimized form of:
        //     codePoint >= MIN_CODE_POINT && codePoint <= MAX_CODE_POINT
        int plane = codePoint >>> 16;
        return plane < ((MAX_CODE_POINT + 1) >>> 16);
    }

    /**
     * 确定指定的字符(Unicode代码点)是否在<a href="#BMP">基本多文种平面(BMP)</a>内.
     * 这些代码点都是可以用一个{@code char}表示.
     *
     * @param  codePoint 要测试的(Unicode 代码点).
     * @return {@code true} 如果指定的代码点在{@link #MIN_VALUE}和{@link #MAX_VALUE}之间, 返回{@code true};
     *         否则返回{@code false}.
     * @since  1.7
     */
    public static boolean isBmpCodePoint(int codePoint) {
        return codePoint >>> 16 == 0;
        // Optimized form of:
        //     codePoint >= MIN_VALUE && codePoint <= MAX_VALUE
        // We consistently use logical shift (>>>) to facilitate
        // additional runtime optimizations.
    }

    /**
     * 确定指定的字符(Unicode代码点)是否在<a href="#supplementary">增补字符</a>范围内.
     *
     * @param  codePoint 要测试的(Unicode 代码点).
     * @return {@code true} 如果指定的代码点在{@link #MIN_SUPPLEMENTARY_CODE_POINT}和{@link #MAX_CODE_POINT}之间, 返回{@code true}.
     *         否则返回{@code false}.
     * @since  1.5
     */
    public static boolean isSupplementaryCodePoint(int codePoint) {
        return codePoint >= MIN_SUPPLEMENTARY_CODE_POINT
            && codePoint <  MAX_CODE_POINT + 1;
    }

    /**
     * 确定给定的{@code char}值是否是<a href="http://www.unicode.org/glossary/#high_surrogate_code_unit">Unicode高代理项代码单元</a>
     * (也被称为<i>前导代理项代码单元</i>).
     *
     * <p>这类值并不表示它们自身的字符,而被用来表示UTF-16编码中的<a href="#supplementary">增补字符</a>
     *
     * @param  ch 要测试的{@code char}值.
     * @return 如果该{@code char}值在{@link #MIN_HIGH_SURROGATE}和{@link #MAX_HIGH_SURROGATE}之间,返回{@code true};
     *         否则返回{@code false}.
     * @see    Character#isLowSurrogate(char)
     * @see    Character.UnicodeBlock#of(int)
     * @since  1.5
     */
    public static boolean isHighSurrogate(char ch) {
        // Help VM constant-fold; MAX_HIGH_SURROGATE + 1 == MIN_LOW_SURROGATE
        return ch >= MIN_HIGH_SURROGATE && ch < (MAX_HIGH_SURROGATE + 1);
    }

    /**
     * 确定给定的{@code char}值是否是<a href="http://www.unicode.org/glossary/#low_surrogate_code_unit">Unicode低代理项代码单元</a>
     * (也被称为<i>尾部代理项代码单元</i>).
     *
     * <p>这类值并不表示它们自身的字符,而被用来表示UTF-16编码中的<a href="#supplementary">增补字符</a>
     *
     * @param  ch 要测试的{@code char}值.
     * @return  如果{@code char}值在{@link #MIN_LOW_SURROGATE}和{@link #MAX_LOW_SURROGATE}之间,返回{@code true};
     *         否则返回{@code false}.
     * @see    Character#isHighSurrogate(char)
     * @since  1.5
     */
    public static boolean isLowSurrogate(char ch) {
        return ch >= MIN_LOW_SURROGATE && ch < (MAX_LOW_SURROGATE + 1);
    }

    /**
     * 确定给定的{@code char}值是否是一个Unicode<i>代理代码单元</i>.
     *
     * <p>这类值并不表示它们自身的字符,而被用来表示UTF-16编码中的<a href="#supplementary">增补字符</a>
     *
     * <p>当且仅当一个字符值满足{@linkplain #isLowSurrogate(char) isLowSurrogate}
     * 或{@linkplain #isHighSurrogate(char) isHighSurrogate}时,它才是一个代理代码单元
     *
     * @param  ch 要测试的{@code char}值.
     * @return 如果{@code char}值在{@link #MIN_SURROGATE}和{@link #MAX_SURROGATE}之间,返回{@code true};
     *         否则返回{@code false}.
     * @since  1.7
     */
    public static boolean isSurrogate(char ch) {
        return ch >= MIN_SURROGATE && ch < (MAX_SURROGATE + 1);
    }

    /**
     * 确定指定的一对{@code char}值是否是一个有效的<a href="http://www.unicode.org/glossary/#surrogate_pair">Unicode代理项对</a>.
     *
     * <p>该方法等价于以下表达式:
     * <blockquote><pre>{@code
     * isHighSurrogate(high) && isLowSurrogate(low)
     * }</pre></blockquote>
     *
     * @param  high 要测试的高代理项代码值
     * @param  low 要测试的低代理项代码值
     * @return 如果指定的高代理项和低代理项的代码值是一个有效的代理项对, 返回{@code true};
     *          否则返回{@code false}.
     * @since  1.5
     */
    public static boolean isSurrogatePair(char high, char low) {
        return isHighSurrogate(high) && isLowSurrogate(low);
    }

    /**
     * 确定表示指定字符(Unicode代码点)所需要的{@code char}值的数量.
     * 如果指定字符等于或大于0x10000, 那么该方法返回2.否则返回1.
     *
     * <p>该方法没有去验证指定的字符是否为一个有效的Unicode代码点.
     * 如有必要, 调用者必须使用 {@link #isValidCodePoint(int) isValidCodePoint} 验证字符值
     *
     * @param   codePoint 要测试的字符(Unicode代码点).
     * @return  如果字符是一个有效的增补字符, 则返回2; 否则返回1.
     * @see     Character#isSupplementaryCodePoint(int)
     * @since   1.5
     */
    public static int charCount(int codePoint) {
        return codePoint >= MIN_SUPPLEMENTARY_CODE_POINT ? 2 : 1;
    }

    /**
     * 将指定的代理项对转换为其增补代码点值.
     * 该方法没有验证指定的代理项对.
     * 如有必要, 调用者必须使用 {@link #isSurrogatePair(char, char) isSurrogatePair}去进行验证.
     *
     * @param  high 高代理项代码单元
     * @param  low 低代理项代码单元
     * @return 用指定代理项对组成的增补代码点.
     * @since  1.5
     */
    public static int toCodePoint(char high, char low) {
        // Optimized form of:
        // return ((high - MIN_HIGH_SURROGATE) << 10)
        //         + (low - MIN_LOW_SURROGATE)
        //         + MIN_SUPPLEMENTARY_CODE_POINT;
        return ((high << 10) + low) + (MIN_SUPPLEMENTARY_CODE_POINT
                                       - (MIN_HIGH_SURROGATE << 10)
                                       - MIN_LOW_SURROGATE);
    }

    /**
     * 返回{@code CharSequence}中给定索引上的代码点.
     * 如果{@code CharSequence}中的给定索引上的{@code char}值在高代理项范围内
     * 则下列索引的长度小于{@code CharSequence}的长度,
     * {@code CharSequence}中的给定索引上的{@code char}值在低代理项范围内
     * 则返回对应于此代理项对的增补代码点.
     * 否则, 返回给定索引上的{@code char}值.
     *
     * @param seq {@code char}(Unicode代码单元)值序列.
     * @param index 要转换的{@code seq}中的{@code char}值(Unicode 代码单元)的索引.
     * @return 给定索引上的 Unicode代码点
     * @exception NullPointerException 如果{@code seq}为null.
     * @exception IndexOutOfBoundsException 如果{@code index}的值为负数或者不小于{@link CharSequence#length() seq.length()}.
     *
     * @since  1.5
     */
    public static int codePointAt(CharSequence seq, int index) {
        char c1 = seq.charAt(index);
        if (isHighSurrogate(c1) && ++index < seq.length()) {
            char c2 = seq.charAt(index);
            if (isLowSurrogate(c2)) {
                return toCodePoint(c1, c2);
            }
        }
        return c1;
    }

    /**
     * 返回{@code char}数组的给定索引上的代码点.
     * 如果{@code char}数组中的给定索引上的{@code char}值在高代理项范围内,
     * 则下一个索引的长度小于{@code char}数组的长度,
     * 如果下一个索引上的{@code char}值在低代理项范围内,
     * 则返回对应于此代理项对的增补代码点.
     * 否则, 返回给定索引上的{@code char}值.
     *
     * @param a {@code char}数组
     * @param index 要转换的{@code char}数组中的{@code char}值(Unicode 代码单元)的索引.
     * @return 给定索引上的 Unicode代码点
     * @exception NullPointerException 如果{@code a}为null.
     * @exception IndexOutOfBoundsException 如果{@code index}是负数或者不小于{@code char}数组的长度.
     * @since  1.5
     */
    public static int codePointAt(char[] a, int index) {
        return codePointAtImpl(a, index, a.length);
    }

    /**
     * 返回{@code char}数组的给定索引上的代码点,其中只使用{@code index}小于{@code limit}的数组元素.
     * 如果{@code char}数组中给定索引上的{@code char}值在高代理项范围内,
     * 则下一个索引的长度小于 {@code limit},
     * 并且如果下一个索引上的 {@code char}值在低代理项范围内,
     * 则返回对应于此代理项对的增补代码点.
     * 否则返回返回给定索引上的{@code char}值.
     *
     * @param a {@code char}数组
     * @param index 要转换的{@code char}数组中的{@code char}值(Unicode 代码单元)的索引.
     * @param limit the index after the last array element that
     * can be used in the {@code char} array
     * @return )给定索引上的 Unicode代码点
     * @exception NullPointerException 如果{@code a}为null.
     * @exception IndexOutOfBoundsException 如果{@code index}参数是负数或者不小于{@code limit}参数,
     * 或者{@code limit}参数是负数或大于{@code char}数组的长度.
     * @since  1.5
     */
    public static int codePointAt(char[] a, int index, int limit) {
        if (index >= limit || limit < 0 || limit > a.length) {
            throw new IndexOutOfBoundsException();
        }
        return codePointAtImpl(a, index, limit);
    }

    // throws ArrayIndexOutOfBoundsException if index out of bounds
    static int codePointAtImpl(char[] a, int index, int limit) {
        char c1 = a[index];
        if (isHighSurrogate(c1) && ++index < limit) {
            char c2 = a[index];
            if (isLowSurrogate(c2)) {
                return toCodePoint(c1, c2);
            }
        }
        return c1;
    }

    /**
     * 返回{@code CharSequence}中给定索引前面的代码点.
     * 如果{@code CharSequence}中索引位置为{@code (index - 1)}的{@code char}值在低代理项范围内,
     * 并且{@code (index - 2)}不是负数,
     * 同时{@code CharSequence}中索引位置为{@code (index - 2)}的{@code char}值在高代理项范围内,
     * 则返回对应于此代理项对的增补代码点.
     * 否则,返回索引位置为{@code (index - 1)}的{@code char}值.
     *
     * @param seq {@code CharSequence}实例.
     * @param index 应该返回的代码点后面的索引.
     * @return 给定索引前面的Unicode代码点值.
     * @exception NullPointerException 如果{@code seq}为null.
     * @exception IndexOutOfBoundsException 如果{@code index}参数的值小于１或者大于{@link CharSequence#length() seq.length()}.
     * @since  1.5
     */
    public static int codePointBefore(CharSequence seq, int index) {
        char c2 = seq.charAt(--index);
        if (isLowSurrogate(c2) && index > 0) {
            char c1 = seq.charAt(--index);
            if (isHighSurrogate(c1)) {
                return toCodePoint(c1, c2);
            }
        }
        return c2;
    }

    /**
     * 返回{@code char}数组中给定索引前面的代码点.
     * 如果{@code char}数组中索引位置为{@code (index - 1)}的{@code char}值在低代理项范围内,
     * 并且{@code (index - 2)}不是负数,
     * 同时{@code char}数组中索引位置为{@code (index - 2)}的{@code char}值在高代理项范围内,
     * 则返回对应于此代理项对的增补代码点.
     * 否则,返回索引位置为{@code (index - 1)}的{@code char}值.
     *
     * @param a the {@code char} array
     * @param index 应该返回的代码点后面的索引
     * @return 给定索引前面的Unicode代码点值.
     * @exception NullPointerException 如果{@code a}为null.
     * @exception IndexOutOfBoundsException 如果{@code index}参数的值小于１或者大于{@code char}数组的长度.
     * @since  1.5
     */
    public static int codePointBefore(char[] a, int index) {
        return codePointBeforeImpl(a, index, 0);
    }

    /**
     * 返回{@code char}数组中给定索引前面的代码点,该数组中只有那些具有大于等于{@code start}的{@code index}值的数组元素可以使用.
     * 如果{@code char}数组中索引位置为{@code (index - 1)}的{@code char}值在低代理项范围内,
     * 并且{@code (index - 2)}不小于{@code start},
     * 同时{@code char}数组中索引位置为{@code (index - 2)}的{@code char}值在高代理项范围内,
     * 则返回对应于此代理项对的增补代码点.
     * 否则,返回索引位置为{@code (index - 1)}的{@code char}值.
     *
     * @param a the {@code char} array
     * @param index 应该返回的代码点后面的索引
     * @param start {@code char}数组中第一个数组元素的索引.
     * @return 给定索引前面的Unicode代码点值.
     * @exception NullPointerException 如果{@code a}为null.
     * @exception IndexOutOfBoundsException 如果{@code index}参数的值不大于{@code start}参数或者大于{@code char}数组的长度,
     * 或者 {@code start}是负数或者小于{@code char}数组的长度.
     * @since  1.5
     */
    public static int codePointBefore(char[] a, int index, int start) {
        if (index <= start || start < 0 || start >= a.length) {
            throw new IndexOutOfBoundsException();
        }
        return codePointBeforeImpl(a, index, start);
    }

    // throws ArrayIndexOutOfBoundsException if index-1 out of bounds
    static int codePointBeforeImpl(char[] a, int index, int start) {
        char c2 = a[--index];
        if (isLowSurrogate(c2) && index > start) {
            char c1 = a[--index];
            if (isHighSurrogate(c1)) {
                return toCodePoint(c1, c2);
            }
        }
        return c2;
    }

    /**
     * 返回UTF-16编码中代表指定增补字符(Unicode代码点)的<a href="http://www.unicode.org/glossary/#surrogate_pair">代理项对</a>
     * 的前导代理项(<a href="http://www.unicode.org/glossary/#high_surrogate_code_unit">高代理项代码单元</a>).
     * 如果指定的字符不是一个<a href="Character.html#supplementary">增补字符</a>,
     * 那么返回一个未指定的{@code char}.
     *
     * <p>如果{@link #isSupplementaryCodePoint isSupplementaryCodePoint(x)}为{@code true},
     * 那么{@link #isHighSurrogate isHighSurrogate}{@code (highSurrogate(x))}和
     * {@link #toCodePoint toCodePoint}{@code (highSurrogate(x), }{@link #lowSurrogate lowSurrogate}{@code (x)) == x}
     * 也都始终为{@code true}.
     *
     * @param   codePoint 增补字符 (Unicode代码点)
     * @return  UTF-16编码中代表指定字符的前导代码单元.
     * @since   1.7
     */
    public static char highSurrogate(int codePoint) {
        return (char) ((codePoint >>> 10)
            + (MIN_HIGH_SURROGATE - (MIN_SUPPLEMENTARY_CODE_POINT >>> 10)));
    }

    /**
     * 返回代表UTF-16编码中指定增补字符(Unicode代码点)的<a href="http://www.unicode.org/glossary/#surrogate_pair">代理项对</a>
     * 的尾部代理项 (<a href="http://www.unicode.org/glossary/#low_surrogate_code_unit">低代理项代码单元</a>).
     *
     * 如果指定的字符并不是一个<a href="Character.html#supplementary">增补字符</a>,那么返回未指明的{@code char}.
     *
     * <p>如果{@link #isSupplementaryCodePoint isSupplementaryCodePoint(x)}为{@code true},那么
     * 那么{@link #isLowSurrogate isLowSurrogate}{@code (lowSurrogate(x))}和
     * {@link #toCodePoint toCodePoint}{@code (}{@link #highSurrogate highSurrogate}{@code (x), lowSurrogate(x)) == x}也一直为{@code true}.
     *
     * @param   codePoint 一个增补字符 (Unicode 代码点)
     * @return  用UTF-16编码字符展示的尾部代理项代码单元.
     * @since   1.7
     */
    public static char lowSurrogate(int codePoint) {
        return (char) ((codePoint & 0x3ff) + MIN_LOW_SURROGATE);
    }

    /**
     * 将指定的字符(Unicode代码点)转换为UTF-16表现形式.
     * 如果指定的代码点为一个BMP(基本多文种平面或者平面0)值,那么同样的值被存在{@code dst[dstIndex]},然后返回1.
     * 如果指定的字符为增补字符, 那么它的代理项值被存在{@code dst[dstIndex]}(高代理项)和{@code dst[dstIndex+1]}(低代理项),然后返回2.
     *
     * @param  codePoint 要被转换的字符(Unicod代码点).
     * @param  dst 一个{@code char}数组,{@code codePoint}的 UTF-16 值存储在其中.
     * @param dstIndex 转换结果存放进{@code dst}数组时的起始索引.
     * @return 如果代码点为BMP代码点, 那么返回1;如果代码点为增补代码点,那么返回2.
     * @exception IllegalArgumentException 如果指定的{@code codePoint}并不是一个有效的Unicode代码点.
     * @exception NullPointerException 如果指定的{@code dst}为null.
     * @exception IndexOutOfBoundsException 如果{@code dstIndex}为负或者不小于{@code dst.length},
     * 或者 {@code dstIndex}处的{@code dst}并没有足够的数组元素来存放结果 {@code char}值.
     * (如果{@code dstIndex}等于{@code dst.length-1},
     * 并且指定的{@code codePoint}为增补字符,那么不在{@code dst[dstIndex]}中存放高代理项值.)
     * @since  1.5
     */
    public static int toChars(int codePoint, char[] dst, int dstIndex) {
        if (isBmpCodePoint(codePoint)) {
            dst[dstIndex] = (char) codePoint;
            return 1;
        } else if (isValidCodePoint(codePoint)) {
            toSurrogates(codePoint, dst, dstIndex);
            return 2;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * 将指定的字符(Unicode代码点)转换为UTF-16表现形式, 并存放到一个{@code char}数组中.
     * 如果指定的代码点为一个BMP(基本多文种平面或者平面0)值, 那么结果{@code char}数组和{@code codePoint}拥有同样的值．
     * 如果指定的字符为增补字符,　那么结果{@code char}数组拥有相应的代理项对.
     *
     * @param  codePoint 一个Unicode代码点.
     * @return 一个具有{@code codePoint}的 UTF-16 表示形式的{@code char}数组{@code char}.
     * @exception IllegalArgumentException 如果指定的{@code codePoint}并不是一个有效的Unicode代码点.
     * @since  1.5
     */
    public static char[] toChars(int codePoint) {
        if (isBmpCodePoint(codePoint)) {
            return new char[] { (char) codePoint };
        } else if (isValidCodePoint(codePoint)) {
            char[] result = new char[2];
            toSurrogates(codePoint, result, 0);
            return result;
        } else {
            throw new IllegalArgumentException();
        }
    }

    static void toSurrogates(int codePoint, char[] dst, int index) {
        // We write elements "backwards" to guarantee all-or-nothing
        dst[index+1] = lowSurrogate(codePoint);
        dst[index] = highSurrogate(codePoint);
    }

    /**
     * 返回指定字符序列的文本范围内的 Unicode 代码点数量.
     * 文本范围始于指定的 {@code beginIndex}, 并扩展到索引 {@code endIndex - 1}上的 {@code char}.
     * {@code char} at index {@code endIndex - 1}.
     * 因此文本范围的长度({@code char}形式)为{@code endIndex-beginIndex}.
     * 文本范围内的不成对代理项是按一个代码点算作一个项进行计数的.
     *
     * @param seq 字符序列
     * @param beginIndex 文本范围的第一个{@code char}的索引.
     * @param endIndex 文本范围的最后一个{@code char}后面的索引.
     * @return 指定文本范围内的 Unicode 代码点的数量
     * @exception NullPointerException 如果{@code seq}为null.
     * @exception IndexOutOfBoundsException 如果{@code beginIndex}为负数,
     * 或者{@code endIndex}大于给定的字符序列的长度,
     * 或者{@code beginIndex}大于{@code endIndex}.
     * @since  1.5
     */
    public static int codePointCount(CharSequence seq, int beginIndex, int endIndex) {
        int length = seq.length();
        if (beginIndex < 0 || endIndex > length || beginIndex > endIndex) {
            throw new IndexOutOfBoundsException();
        }
        int n = endIndex - beginIndex;
        for (int i = beginIndex; i < endIndex; ) {
            if (isHighSurrogate(seq.charAt(i++)) && i < endIndex &&
                isLowSurrogate(seq.charAt(i))) {
                n--;
                i++;
            }
        }
        return n;
    }

    /**
     * 返回{@code char}数组参数的子数组中 Unicode 代码点的数量.
     * {@code offset} 参数是子数组的第一个 {@code char} 的索引, {@code count} 参数指定了 {@code char} 中的子数组的长度.
     * 子数组中不成对的代理项是按一个代码点算作一个项进行计数的.
     *
     * @param a {@code char}数组
     * @param offset 给定{@code char}数组中第一个{@code char}的索引.
     * @param count {@code char}中的子数组的长度
     * @return 指定子数组中 Unicode 代码点的数量
     * @exception NullPointerException 如果{@code a}为null.
     * @exception IndexOutOfBoundsException 如果{@code offset}或{@code count}为负,
     * 或者 {@code offset + count} 大于给定数组的长度.
     * @since  1.5
     */
    public static int codePointCount(char[] a, int offset, int count) {
        if (count > a.length - offset || offset < 0 || count < 0) {
            throw new IndexOutOfBoundsException();
        }
        return codePointCountImpl(a, offset, count);
    }

    static int codePointCountImpl(char[] a, int offset, int count) {
        int endIndex = offset + count;
        int n = count;
        for (int i = offset; i < endIndex; ) {
            if (isHighSurrogate(a[i++]) && i < endIndex &&
                isLowSurrogate(a[i])) {
                n--;
                i++;
            }
        }
        return n;
    }

    /**
     * 返回给定字符序列中的索引, 它是从给定{@code index}到 {@code codePointOffset}代码点的偏移量.
     * {@code index}和 {@code codePointOffset} 给出的文本范围内的不成对代理项是按一个代码点算作一个项进行计数的.
     *
     * @param seq 字符序列
     * @param index 要偏移的索引
     * @param codePointOffset 代码点中的偏移量
     * @return 字符序列内的索引
     * @exception NullPointerException 如果{@code seq}为null.
     * @exception IndexOutOfBoundsException 如果 {@code index} 为负或大于字符序列的长度,
     * 或者 {@code codePointOffset} 为负并且起始于 {@code index} 的子序列拥有的偏移量少于 {@code codePointOffset} 代码点,
     * 或者 {@code codePointOffset} 为负并且 {@code index} 前面的子序列的偏移量少于 {@code codePointOffset} 代码点的绝对值.
     * @since 1.5
     */
    public static int offsetByCodePoints(CharSequence seq, int index, 
                                         int codePointOffset) {
        int length = seq.length();
        if (index < 0 || index > length) {
            throw new IndexOutOfBoundsException();
        }

        int x = index;
        if (codePointOffset >= 0) {
            int i;
            for (i = 0; x < length && i < codePointOffset; i++) {
                if (isHighSurrogate(seq.charAt(x++)) && x < length &&
                    isLowSurrogate(seq.charAt(x))) {
                    x++;
                }
            }
            if (i < codePointOffset) {
                throw new IndexOutOfBoundsException();
            }
        } else {
            int i;
            for (i = codePointOffset; x > 0 && i < 0; i++) {
                if (isLowSurrogate(seq.charAt(--x)) && x > 0 &&
                    isHighSurrogate(seq.charAt(x-1))) {
                    x--;
                }
            }
            if (i < 0) {
                throw new IndexOutOfBoundsException();
            }
        }
        return x;
    }

    /**
     * 返回给定{@code char}子数组中的索引, 它是从给定{@code index}到{@code codePointOffset}代码点的偏移量.
     * {@code start}和{@code count}参数指定了{@code char}数组的一个子数组.
     * {@code index}和{@code codePointOffset}给出的文本范围内的不成对代理项是按一个代码点算作一个项进行计数的.
     *
     * @param a {@code char}数组
     * @param start 子数组的第一个{@code char}的索引
     * @param count {@code char}中的子数组的长度
     * @param index 要偏移的索引
     * @param codePointOffset 代码点中的偏移量
     * @return 子数组内的索引
     * @exception NullPointerException 如果{@code a}为null.
     * @exception IndexOutOfBoundsException
     *   如果{@code start}或 {@code count}为负,
     *   或者{@code start + count}大于给定数组的长度,
     *   或者{@code index}小于{@code start}或者大于{@code start + count},
     *   或者 {@code codePointOffset}为负并且起始于{@code index}、终止于{@code start + count - 1}的文本字段的偏移量少于{@code codePointOffset}代码点,
     *   或者 {@code codePointOffset}为负并且起始于{@code index}、终止于{@code index - 1}的文本字段的偏移量少于{@code codePointOffset}代码点的绝对值.
     * @since 1.5
     */
    public static int offsetByCodePoints(char[] a, int start, int count, 
                                         int index, int codePointOffset) {
        if (count > a.length-start || start < 0 || count < 0
            || index < start || index > start+count) {
            throw new IndexOutOfBoundsException();
        }
        return offsetByCodePointsImpl(a, start, count, index, codePointOffset);
    }

    static int offsetByCodePointsImpl(char[]a, int start, int count, 
                                      int index, int codePointOffset) {
        int x = index;
        if (codePointOffset >= 0) {
            int limit = start + count;
            int i;
            for (i = 0; x < limit && i < codePointOffset; i++) {
                if (isHighSurrogate(a[x++]) && x < limit &&
                    isLowSurrogate(a[x])) {
                    x++;
                }
            }
            if (i < codePointOffset) {
                throw new IndexOutOfBoundsException();
            }
        } else {
            int i;
            for (i = codePointOffset; x > start && i < 0; i++) {
                if (isLowSurrogate(a[--x]) && x > start &&
                    isHighSurrogate(a[x-1])) {
                    x--;
                }
            }
            if (i < 0) {
                throw new IndexOutOfBoundsException();
            }
        }
        return x;
    }

    /**
     * 确定指定字符是否为小写字母.
     * <p>
     * 如果通过{@code Character.getType(ch)}提供的字符的常规类别类型为{@code LOWERCASE_LETTER},
     * 或者Unicode标准定义的其它的小写字母, 则字符为小写字母.
     * <p>
     * 以下是小写字母的示例:
     * <blockquote><pre>
     * a b c d e f g h i j k l m n o p q r s t u v w x y z
     * '&#92;u00DF' '&#92;u00E0' '&#92;u00E1' '&#92;u00E2' '&#92;u00E3' '&#92;u00E4' '&#92;u00E5' '&#92;u00E6'
     * '&#92;u00E7' '&#92;u00E8' '&#92;u00E9' '&#92;u00EA' '&#92;u00EB' '&#92;u00EC' '&#92;u00ED' '&#92;u00EE'
     * '&#92;u00EF' '&#92;u00F0' '&#92;u00F1' '&#92;u00F2' '&#92;u00F3' '&#92;u00F4' '&#92;u00F5' '&#92;u00F6'
     * '&#92;u00F8' '&#92;u00F9' '&#92;u00FA' '&#92;u00FB' '&#92;u00FC' '&#92;u00FD' '&#92;u00FE' '&#92;u00FF'
     * </pre></blockquote>
     * <p> 许多其它 Unicode 字符也是小写的.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isLowerCase(int)} 方法.
     *
     * @param   ch  要测试的字符.
     * @return  如果字符为小写,返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isLowerCase(char)
     * @see     Character#isTitleCase(char)
     * @see     Character#toLowerCase(char)
     * @see     Character#getType(char)
     */
    public static boolean isLowerCase(char ch) {
        return isLowerCase((int)ch);
    }

    /**
     * 确定指定字符(Unicode 代码点)是否为小写字母.
     * <p>
     * 如果通过{@link Character#getType getType(codePoint)}提供的字符的常规类别类型为{@code LOWERCASE_LETTER},
     * 或者Unicode标准定义的其它的小写字母, 则字符为小写字母.
     * <p>
     * 以下是小写字母的示例:
     * <blockquote><pre>
     * a b c d e f g h i j k l m n o p q r s t u v w x y z
     * '&#92;u00DF' '&#92;u00E0' '&#92;u00E1' '&#92;u00E2' '&#92;u00E3' '&#92;u00E4' '&#92;u00E5' '&#92;u00E6'
     * '&#92;u00E7' '&#92;u00E8' '&#92;u00E9' '&#92;u00EA' '&#92;u00EB' '&#92;u00EC' '&#92;u00ED' '&#92;u00EE'
     * '&#92;u00EF' '&#92;u00F0' '&#92;u00F1' '&#92;u00F2' '&#92;u00F3' '&#92;u00F4' '&#92;u00F5' '&#92;u00F6'
     * '&#92;u00F8' '&#92;u00F9' '&#92;u00FA' '&#92;u00FB' '&#92;u00FC' '&#92;u00FD' '&#92;u00FE' '&#92;u00FF'
     * </pre></blockquote>
     * <p> 许多其它 Unicode 字符也是小写的.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符为小写,返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isLowerCase(int)
     * @see     Character#isTitleCase(int)
     * @see     Character#toLowerCase(int)
     * @see     Character#getType(int)
     * @since   1.5
     */
    public static boolean isLowerCase(int codePoint) {
        return getType(codePoint) == Character.LOWERCASE_LETTER ||
               CharacterData.of(codePoint).isOtherLowercase(codePoint);
    }

    /**
     * 确定指定字符是否为大写字母.
     * <p>
     * 如果通过{@code Character.getType(ch)}提供的字符的常规类别类型为{@code UPPERCASE_LETTER},
     * 或者Unicode标准定义的其它的大写字母, 则字符为大写字母.
     * <p>
     * 以下是大写字母的示例:
     * <blockquote><pre>
     * A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
     * '&#92;u00C0' '&#92;u00C1' '&#92;u00C2' '&#92;u00C3' '&#92;u00C4' '&#92;u00C5' '&#92;u00C6' '&#92;u00C7'
     * '&#92;u00C8' '&#92;u00C9' '&#92;u00CA' '&#92;u00CB' '&#92;u00CC' '&#92;u00CD' '&#92;u00CE' '&#92;u00CF'
     * '&#92;u00D0' '&#92;u00D1' '&#92;u00D2' '&#92;u00D3' '&#92;u00D4' '&#92;u00D5' '&#92;u00D6' '&#92;u00D8'
     * '&#92;u00D9' '&#92;u00DA' '&#92;u00DB' '&#92;u00DC' '&#92;u00DD' '&#92;u00DE'
     * </pre></blockquote>
     * <p> 许多其它 Unicode 字符也是大写的.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isUpperCase(int)} 方法.
     *
     * @param   ch   要测试的字符.
     * @return  如果字符为大写,返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isLowerCase(char)
     * @see     Character#isTitleCase(char)
     * @see     Character#toUpperCase(char)
     * @see     Character#getType(char)
     * @since   1.0
     */
    public static boolean isUpperCase(char ch) {
        return isUpperCase((int)ch);
    }

    /**
     * 确定指定字符(Unicode 代码点)是否为大写字母.
     * <p>
     * 如果通过{@link Character#getType(int) getType(codePoint)}提供的字符的常规类别类型为{@code UPPERCASE_LETTER},
     * 或者Unicode标准定义的其它的大写字母, 则字符为大写字母.
     * <p>
     * 以下是大写字母的示例:
     * <blockquote><pre>
     * A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
     * '&#92;u00C0' '&#92;u00C1' '&#92;u00C2' '&#92;u00C3' '&#92;u00C4' '&#92;u00C5' '&#92;u00C6' '&#92;u00C7'
     * '&#92;u00C8' '&#92;u00C9' '&#92;u00CA' '&#92;u00CB' '&#92;u00CC' '&#92;u00CD' '&#92;u00CE' '&#92;u00CF'
     * '&#92;u00D0' '&#92;u00D1' '&#92;u00D2' '&#92;u00D3' '&#92;u00D4' '&#92;u00D5' '&#92;u00D6' '&#92;u00D8'
     * '&#92;u00D9' '&#92;u00DA' '&#92;u00DB' '&#92;u00DC' '&#92;u00DD' '&#92;u00DE'
     * </pre></blockquote>
     * <p> 许多其它 Unicode 字符也是大写的.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符为大写,返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isLowerCase(int)
     * @see     Character#isTitleCase(int)
     * @see     Character#toUpperCase(int)
     * @see     Character#getType(int)
     * @since   1.5
     */
    public static boolean isUpperCase(int codePoint) {
        return getType(codePoint) == Character.UPPERCASE_LETTER ||
               CharacterData.of(codePoint).isOtherUppercase(codePoint);
    }

    /**
     * 确定指定字符是否为首字母大写字符.
     * <p>
     * 如果通过 {@code Character.getType(ch)} 提供的字符的常规类别类型为 {@code TITLECASE_LETTER},则字符为首字母大写字符.
     * <p>
     * 一些字符看似成对的 Latin 字母.例如, 有一个看起来像"LJ"的大写字母和一个看起来像"lj"的对应小写字母.
     * 第三种形式看起来像"Lj" ,这是呈现首字母大写的小写单词时使用的适当形式, 比如用于书籍的标题.
     * <p>
     * 下面是一些可以让该方法返回{@code true}的 Unicode 字符:
     * <ul>
     * <li>{@code LATIN CAPITAL LETTER D WITH SMALL LETTER Z WITH CARON}
     * <li>{@code LATIN CAPITAL LETTER L WITH SMALL LETTER J}
     * <li>{@code LATIN CAPITAL LETTER N WITH SMALL LETTER J}
     * <li>{@code LATIN CAPITAL LETTER D WITH SMALL LETTER Z}
     * </ul>
     * <p> 其他许多Unicode字符也是首字母大写的.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isTitleCase(int)} 方法.
     *
     * @param   ch   要测试的字符.
     * @return  如果首字母大写,返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isLowerCase(char)
     * @see     Character#isUpperCase(char)
     * @see     Character#toTitleCase(char)
     * @see     Character#getType(char)
     * @since   1.0.2
     */
    public static boolean isTitleCase(char ch) {
        return isTitleCase((int)ch);
    }

    /**
     *　确定指定字符(Unicode 代码点)是否为首字母大写字符.
     * <p>
     * 如果通过 {@link Character#getType(int) getType(codePoint)}提供的字符的常规类别类型为 {@code TITLECASE_LETTER},
     * 则字符为首字母大写字符.
     * <p>
     * 一些字符看似成对的 Latin 字母.例如, 有一个看起来像"LJ"的大写字母和一个看起来像"lj"的对应小写字母.
     * 第三种形式看起来像"Lj" ,这是呈现首字母大写的小写单词时使用的适当形式, 比如用于书籍的标题.
     * <p>
     * 下面是一些可以让该方法返回{@code true}的Unicode字符:
     * <ul>
     * <li>{@code LATIN CAPITAL LETTER D WITH SMALL LETTER Z WITH CARON}
     * <li>{@code LATIN CAPITAL LETTER L WITH SMALL LETTER J}
     * <li>{@code LATIN CAPITAL LETTER N WITH SMALL LETTER J}
     * <li>{@code LATIN CAPITAL LETTER D WITH SMALL LETTER Z}
     * </ul>
     * <p> 其他许多Unicode字符也是首字母大写的.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果首字母大写,返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isLowerCase(int)
     * @see     Character#isUpperCase(int)
     * @see     Character#toTitleCase(int)
     * @see     Character#getType(int)
     * @since   1.5
     */
    public static boolean isTitleCase(int codePoint) {
        return getType(codePoint) == Character.TITLECASE_LETTER;
    }

    /**
     * 确定指定字符是否为数字.
     * <p>
     * 如果通过 {@code Character.getType(ch)} 提供的字符的常规类别类型为 {@code DECIMAL_DIGIT_NUMBER},则字符为数字
     * <p>
     *包含数字的 Unicode 字符范围:
     * <ul>
     * <li>{@code '\u005Cu0030'} 到 {@code '\u005Cu0039'},
     *     ISO-LATIN-1 数字 ({@code '0'} 到 {@code '9'})
     * <li>{@code '\u005Cu0660'} 到 {@code '\u005Cu0669'},
     *     Arabic-Indic 数字
     * <li>{@code '\u005Cu06F0'} 到 {@code '\u005Cu06F9'},
     *     扩展 Arabic-Indic 数字
     * <li>{@code '\u005Cu0966'} 到 {@code '\u005Cu096F'},
     *     梵文数字
     * <li>{@code '\u005CuFF10'} 到 {@code '\u005CuFF19'},
     *     全形数字
     * </ul>
     *
     * 其他许多字符范围也包含数字.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isDigit(int)}方法.
     *
     * @param   ch   要测试的字符.
     * @return  如果字符为数字,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#digit(char, int)
     * @see     Character#forDigit(int, int)
     * @see     Character#getType(char)
     */
    public static boolean isDigit(char ch) {
        return isDigit((int)ch);
    }

    /**
     * 确定指定的字符(Unicode 代码点 )是否是数字.
     * <p>
     * 如果通过 {@code Character.getType(ch)} 提供的字符的常规类别类型为 {@code DECIMAL_DIGIT_NUMBER},则字符为数字
     * <p>
     *包含数字的 Unicode 字符范围:
     * <ul>
     * <li>{@code '\u005Cu0030'} 到 {@code '\u005Cu0039'},
     *     ISO-LATIN-1 数字 ({@code '0'} 到 {@code '9'})
     * <li>{@code '\u005Cu0660'} 到 {@code '\u005Cu0669'},
     *     Arabic-Indic 数字
     * <li>{@code '\u005Cu06F0'} 到 {@code '\u005Cu06F9'},
     *     扩展 Arabic-Indic 数字
     * <li>{@code '\u005Cu0966'} 到 {@code '\u005Cu096F'},
     *     梵文数字
     * <li>{@code '\u005CuFF10'} 到 {@code '\u005CuFF19'},
     *     全形数字
     * </ul>
     *
     * 其他许多字符范围也包含数字.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符为数字,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#forDigit(int, int)
     * @see     Character#getType(int)
     * @since   1.5
     */
    public static boolean isDigit(int codePoint) {
        return getType(codePoint) == Character.DECIMAL_DIGIT_NUMBER;
    }

    /**
     * 确定一个字符是否是一个被定义为 Unicode 中的字符.
     * <p>
     * 如果以下条件中至少有一个为真, 则字符被定义为 Unicode 中的字符:
     * <ul>
     * <li>它具有 UnicodeData 文件中的条目.
     * <li>它具有 UnicodeData 文件定义的范围中的值.
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isDefined(int)}方法.
     *
     * @param   ch   要测试的字符
     * @return  如果字符具有为 Unicode 中字符定义的含义,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isDigit(char)
     * @see     Character#isLetter(char)
     * @see     Character#isLetterOrDigit(char)
     * @see     Character#isLowerCase(char)
     * @see     Character#isTitleCase(char)
     * @see     Character#isUpperCase(char)
     * @since   1.0.2
     */
    public static boolean isDefined(char ch) {
        return isDefined((int)ch);
    }

    /**
     * 确定一个字符(Unicode代码点)是否是一个被定义为 Unicode 中的字符.
     * <p>
     * 如果以下条件中至少有一个为真, 则字符被定义为 Unicode 中的字符:
     * <ul>
     * <li>它具有 UnicodeData 文件中的条目.
     * <li>它具有 UnicodeData 文件定义的范围中的值.
     * </ul>
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符具有为 Unicode 中字符定义的含义,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isDigit(int)
     * @see     Character#isLetter(int)
     * @see     Character#isLetterOrDigit(int)
     * @see     Character#isLowerCase(int)
     * @see     Character#isTitleCase(int)
     * @see     Character#isUpperCase(int)
     * @since   1.5
     */
    public static boolean isDefined(int codePoint) {
        return getType(codePoint) != Character.UNASSIGNED;
    }

    /**
     * 确定指定字符是否为字母.
     * <p>
     * 如果通过 {@code Character.getType(ch)} 为字符提供的常规类别的类型为以下类型中的任意一种, 则认为该字符为字母:
     * <ul>
     * <li> {@code UPPERCASE_LETTER}
     * <li> {@code LOWERCASE_LETTER}
     * <li> {@code TITLECASE_LETTER}
     * <li> {@code MODIFIER_LETTER}
     * <li> {@code OTHER_LETTER}
     * </ul>
     *
     * 并非所有的字母都有大小写.许多字符都是字母,但它们既不是大写的,也不是小写的,并且也不是首字母大写的.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isLetter(int)}方法.
     *
     * @param   ch   要测试的字符.
     * @return  如果指定的字符是一个 ,那么返回{@code true} letter;
     *          否则返回{@code false}.
     * @see     Character#isDigit(char)
     * @see     Character#isJavaIdentifierStart(char)
     * @see     Character#isJavaLetter(char)
     * @see     Character#isJavaLetterOrDigit(char)
     * @see     Character#isLetterOrDigit(char)
     * @see     Character#isLowerCase(char)
     * @see     Character#isTitleCase(char)
     * @see     Character#isUnicodeIdentifierStart(char)
     * @see     Character#isUpperCase(char)
     */
    public static boolean isLetter(char ch) {
        return isLetter((int)ch);
    }

    /**
     * 确定指定的字符(Unicode 代码点 )是否是字母.
     * <p>
     * 如果通过 {@link Character#getType(int) getType(codePoint)} 为字符提供的常规类别的类型为以下类型中的任意一种, 则认为该字符为字母:
     * <ul>
     * <li> {@code UPPERCASE_LETTER}
     * <li> {@code LOWERCASE_LETTER}
     * <li> {@code TITLECASE_LETTER}
     * <li> {@code MODIFIER_LETTER}
     * <li> {@code OTHER_LETTER}
     * </ul>
     *
     * 并非所有的字母都有大小写.许多字符都是字母,但它们既不是大写的,也不是小写的,并且也不是首字母大写的.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果指定的字符是一个字母,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isDigit(int)
     * @see     Character#isJavaIdentifierStart(int)
     * @see     Character#isLetterOrDigit(int)
     * @see     Character#isLowerCase(int)
     * @see     Character#isTitleCase(int)
     * @see     Character#isUnicodeIdentifierStart(int)
     * @see     Character#isUpperCase(int)
     * @since   1.5
     */
    public static boolean isLetter(int codePoint) {
        return ((((1 << Character.UPPERCASE_LETTER) |
            (1 << Character.LOWERCASE_LETTER) |
            (1 << Character.TITLECASE_LETTER) |
            (1 << Character.MODIFIER_LETTER) |
            (1 << Character.OTHER_LETTER)) >> getType(codePoint)) & 1)
            != 0;
    }

    /**
     * 确定指定的字符是否是一个字母或数字.
     * <p>
     * 如果{@code Character.isLetter(char ch)}或{@code Character.isDigit(char ch)}对返回的是{@code true}, 则认为字符是一个字母或数字.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isLetterOrDigit(int)}方法.
     *
     * @param   ch   要测试的字符.
     * @return  如果指定的字符是一个字母或数字,那么返回{@code true} letter or digit;
     *          否则返回{@code false}.
     * @see     Character#isDigit(char)
     * @see     Character#isJavaIdentifierPart(char)
     * @see     Character#isJavaLetter(char)
     * @see     Character#isJavaLetterOrDigit(char)
     * @see     Character#isLetter(char)
     * @see     Character#isUnicodeIdentifierPart(char)
     * @since   1.0.2
     */
    public static boolean isLetterOrDigit(char ch) {
        return isLetterOrDigit((int)ch);
    }

    /**
     * 确定指定的字符(Unicode 代码点 )是否是字母或数字.
     * <p>
     * 如果{@link #isLetter(int) isLetter(codePoint)}或{@link #isDigit(int) isDigit(codePoint)}对返回的是{@code true}, 则认为字符是一个字母或数字.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果指定的字符是一个字母或数字,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isDigit(int)
     * @see     Character#isJavaIdentifierPart(int)
     * @see     Character#isLetter(int)
     * @see     Character#isUnicodeIdentifierPart(int)
     * @since   1.5
     */
    public static boolean isLetterOrDigit(int codePoint) {
        return ((((1 << Character.UPPERCASE_LETTER) |
            (1 << Character.LOWERCASE_LETTER) |
            (1 << Character.TITLECASE_LETTER) |
            (1 << Character.MODIFIER_LETTER) |
            (1 << Character.OTHER_LETTER) |
            (1 << Character.DECIMAL_DIGIT_NUMBER)) >> getType(codePoint)) & 1)
            != 0;
    }

    /**
     * 确定是否允许将指定字符作为 Java 标识符中的首字符.
     * <p>
     * 当且仅当以下条件之一为真时, 字符才可以作为 Java 标识符的首字母:
     * <ul>
     * <li> {@link #isLetter(char) isLetter(ch)} 返回 {@code true}
     * <li> {@link #getType(char) getType(ch)} 返回 {@code LETTER_NUMBER}
     * <li> {@code ch} 是一个货币符号 (比如 {@code '$'})
     * <li> {@code ch} 是一个连字符 (比如 {@code '_'}).
     * </ul>
     *
     * @param   ch 要测试的字符.
     * @return  如果字符为 Java 标识符的首字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaLetterOrDigit(char)
     * @see     Character#isJavaIdentifierStart(char)
     * @see     Character#isJavaIdentifierPart(char)
     * @see     Character#isLetter(char)
     * @see     Character#isLetterOrDigit(char)
     * @see     Character#isUnicodeIdentifierStart(char)
     * @since   1.02
     * @deprecated 被isJavaIdentifierStart(char)替代.
     */
    @Deprecated
    public static boolean isJavaLetter(char ch) {
        return isJavaIdentifierStart(ch);
    }

    /**
     * 判断指定的字符是否可以是Java标识符中首字符以外的一部分.
     * <p>
     * 当且仅当如下条件中的任意一个为true的时候,一个字符才可能是Java标识符的一部分:
     * of the following are true:
     * <ul>
     * <li>  是一个字母
     * <li>  是一个货币符号(比如{@code '$'})
     * <li>  是一个连字符(比如{@code '_'})
     * <li>  是一个数字
     * <li>  是一个数字字母(比如一个罗马数字字母)
     * <li>  是一个合成标记
     * <li>  是一个非空个字符
     * <li> {@code isIdentifierIgnorable} 对该字符返回{@code true}.
     * </ul>
     *
     * @param   ch 要测试的字符.
     * @return  如果指定的字符是Java标识符的一部分,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaLetter(char)
     * @see     Character#isJavaIdentifierStart(char)
     * @see     Character#isJavaIdentifierPart(char)
     * @see     Character#isLetter(char)
     * @see     Character#isLetterOrDigit(char)
     * @see     Character#isUnicodeIdentifierPart(char)
     * @see     Character#isIdentifierIgnorable(char)
     * @since   1.02
     * @deprecated 被isJavaIdentifierStart(char)替代.
     */
    @Deprecated
    public static boolean isJavaLetterOrDigit(char ch) {
        return isJavaIdentifierPart(ch);
    }

    /**
     * 确定指定的字符(Unicode 代码点 )是否是一个字母表.
     * <p>
     * 如果一个字符的常规类型(由{@link Character#getType(int) getType(codePoint)}方法提供)满足下列条件中的任意一项时,才会被认为是字母表:
     * <ul>
     * <li> <code>UPPERCASE_LETTER</code>
     * <li> <code>LOWERCASE_LETTER</code>
     * <li> <code>TITLECASE_LETTER</code>
     * <li> <code>MODIFIER_LETTER</code>
     * <li> <code>OTHER_LETTER</code>
     * <li> <code>LETTER_NUMBER</code>
     * </ul>
     * 或者是Unicode标准定义的其它字母表属性.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果该字符为Unicode字母表,那么返回<code>true</code>,否则返回<code>false</code>.
     * @since   1.7
     */
    public static boolean isAlphabetic(int codePoint) {
        return (((((1 << Character.UPPERCASE_LETTER) |
            (1 << Character.LOWERCASE_LETTER) |
            (1 << Character.TITLECASE_LETTER) |
            (1 << Character.MODIFIER_LETTER) |
            (1 << Character.OTHER_LETTER) |
            (1 << Character.LETTER_NUMBER)) >> getType(codePoint)) & 1) != 0) ||
            CharacterData.of(codePoint).isOtherAlphabetic(codePoint);
    }

    /**
     * 确定指定的字符(Unicode 代码点 )是否是Unicode标准所定义的CJKV(中文,日语,韩文和越南语)象形字符.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果指定的字符是一个Unicode象形字符,那么返回<code>true</code>,否则返回<code>false</code>.
     * @since   1.7
     */
    public static boolean isIdeographic(int codePoint) {
        return CharacterData.of(codePoint).isIdeographic(codePoint);
    }

    /**
     * 确定是否允许将指定字符作为Java标识符中的首字符.
     * <p>
     * 当且仅当下列条件中的一项为true时,一个字符才可能是一个Java标识符的首字符:
     * <ul>
     * <li> {@link #isLetter(char) isLetter(ch)} 返回 {@code true}
     * <li> {@link #getType(char) getType(ch)} returns {@code LETTER_NUMBER}
     * <li> {@code ch} 是一个货币符号(比如 {@code '$'})
     * <li> {@code ch} 是一个连字符(比如{@code '_'}).
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isJavaIdentifierStart(int)}方法.
     *
     * @param   ch 要测试的字符.
     * @return  如果字符为 Java 标识符的首字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaIdentifierPart(char)
     * @see     Character#isLetter(char)
     * @see     Character#isUnicodeIdentifierStart(char)
     * @see     javax.lang.model.SourceVersion#isIdentifier(CharSequence)
     * @since   1.1
     */
    public static boolean isJavaIdentifierStart(char ch) {
        return isJavaIdentifierStart((int)ch);
    }

    /**
     * 确定是否允许将字符(Unicode 代码点)作为 Java 标识符中的首字符.
     * <p>
     * 当且仅当下列条件中的一项为true时,一个字符才可能是一个Java标识符的首字符:
     * <ul>
     * <li> {@link #isLetter(int) isLetter(codePoint)}返回 {@code true}
     * <li> {@link #getType(int) getType(codePoint)}返回 {@code LETTER_NUMBER}
     * <li> 引用的字符是一个货币符号(比如 {@code '$'})
     * <li> 引用的字符是一个连字符(比如{@code '_'}).
     * </ul>
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符为 Java 标识符的首字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaIdentifierPart(int)
     * @see     Character#isLetter(int)
     * @see     Character#isUnicodeIdentifierStart(int)
     * @see     javax.lang.model.SourceVersion#isIdentifier(CharSequence)
     * @since   1.5
     */
    public static boolean isJavaIdentifierStart(int codePoint) {
        return CharacterData.of(codePoint).isJavaIdentifierStart(codePoint);
    }

    /**
     * 确定指定字符是否可以是 Java 标识符中首字符以外的部分.
     * <p>
     * 如果以下任何条件为true, 那么字符可能是 Java 标识符的一部分:
     * <ul>
     * <li>  是一个字母
     * <li>  是一个货币符号(比如{@code '$'})
     * <li>  是一个连字符 (比如 {@code '_'})
     * <li>  是一个数字
     * <li>  是一个数字字母 (比如一个罗马数字字母)
     * <li>  是一个合成标记
     * <li>  是一个非空格标记
     * <li> {@code isIdentifierIgnorable} 对该字符返回 {@code true}
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isJavaIdentifierPart(int)}方法.
     *
     * @param   ch      要测试的字符.
     * @return 如果字符可以为 Java 标识符的一部分,那么返回{@code true};否则返回{@code false}.
     * @see     Character#isIdentifierIgnorable(char)
     * @see     Character#isJavaIdentifierStart(char)
     * @see     Character#isLetterOrDigit(char)
     * @see     Character#isUnicodeIdentifierPart(char)
     * @see     javax.lang.model.SourceVersion#isIdentifier(CharSequence)
     * @since   1.1
     */
    public static boolean isJavaIdentifierPart(char ch) {
        return isJavaIdentifierPart((int)ch);
    }

    /**
     * 确定指定字符(Unicode代码点)是否可以是 Java 标识符中首字符以外的部分.
     * <p>
     * 如果以下任何条件为true, 那么字符可能是 Java 标识符的一部分:
     * <ul>
     * <li>  是一个字母
     * <li>  是一个货币符号(比如{@code '$'})
     * <li>  是一个连字符 (比如 {@code '_'})
     * <li>  是一个数字
     * <li>  是一个数字字母 (比如一个罗马数字字母)
     * <li>  是一个合成标记
     * <li>  是一个非空格标记
     * <li> {@link #isIdentifierIgnorable(int)
     * isIdentifierIgnorable(codePoint)} returns {@code true} for
     * the character
     * </ul>
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return {@code true} if the character may be part of a
     *          Java identifier; 否则返回{@code false}.
     * @see     Character#isIdentifierIgnorable(int)
     * @see     Character#isJavaIdentifierStart(int)
     * @see     Character#isLetterOrDigit(int)
     * @see     Character#isUnicodeIdentifierPart(int)
     * @see     javax.lang.model.SourceVersion#isIdentifier(CharSequence)
     * @since   1.5
     */
    public static boolean isJavaIdentifierPart(int codePoint) {
        return CharacterData.of(codePoint).isJavaIdentifierPart(codePoint);
    }

    /**
     * 确定是否允许将指定字符作为 Unicode 标识符中的首字符.
     * <p>
     * 当且仅当以下条件之一为真时, 字符才可以作为 Unicode 标识符的首字符:
     * <ul>
     * <li> {@link #isLetter(char) isLetter(ch)} 返回 {@code true}
     * <li> {@link #getType(char) getType(ch)} 返回 {@code LETTER_NUMBER}.
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isUnicodeIdentifierStart(int)}方法.
     *
     * @param   ch      要测试的字符.
     * @return  如果字符可以作为 Unicode 标识符的首字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaIdentifierStart(char)
     * @see     Character#isLetter(char)
     * @see     Character#isUnicodeIdentifierPart(char)
     * @since   1.1
     */
    public static boolean isUnicodeIdentifierStart(char ch) {
        return isUnicodeIdentifierStart((int)ch);
    }

    /**
     * 确定是否允许将指定字符(Unicode 代码点)作为 Unicode 标识符中的首字符.
     * <p>
     * 当且仅当以下条件之一为真时, 字符才可以作为 Unicode 标识符的首字符:
     * <ul>
     * <li> {@link #isLetter(int) isLetter(codePoint)} 返回 {@code true}
     * <li> {@link #getType(int) getType(codePoint)} 返回 {@code LETTER_NUMBER}.
     * </ul>
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符可以作为 Unicode 标识符的首字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaIdentifierStart(int)
     * @see     Character#isLetter(int)
     * @see     Character#isUnicodeIdentifierPart(int)
     * @since   1.5
     */
    public static boolean isUnicodeIdentifierStart(int codePoint) {
        return CharacterData.of(codePoint).isUnicodeIdentifierStart(codePoint);
    }

    /**
     * 确定指定字符是否可以是 Unicode 标识符中首字符以外的部分.
     * <p>
     * 当且仅当以下语句之一为true时, 字符才可能是 Unicode 标识符的一部分:
     * <ul>
     * <li>  是一个字母
     * <li>  是一个连字符 (比如 {@code '_'})
     * <li>  是一个数字
     * <li>  是一个数字字母 (比如一个罗马数字字母)
     * <li>  是一个合成标记
     * <li>  是一个非空格标记
     * <li> {@code isIdentifierIgnorable} 对该字符返回{@code true}.
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isUnicodeIdentifierPart(int)}方法.
     *
     * @param   ch      要测试的字符.
     * @return  如果字符可以为 Unicode 标识符的一部分,那么返回{@code true}; 否则返回{@code false}.
     * @see     Character#isIdentifierIgnorable(char)
     * @see     Character#isJavaIdentifierPart(char)
     * @see     Character#isLetterOrDigit(char)
     * @see     Character#isUnicodeIdentifierStart(char)
     * @since   1.1
     */
    public static boolean isUnicodeIdentifierPart(char ch) {
        return isUnicodeIdentifierPart((int)ch);
    }

    /**
     * 确定指定字符(Unicode代码点)是否可以是 Unicode 标识符中首字符以外的部分.
     * <p>
     * 当且仅当以下语句之一为true时, 字符才可能是 Unicode 标识符的一部分:
     * <ul>
     * <li>  是一个字母
     * <li>  是一个连字符 (比如 {@code '_'})
     * <li>  是一个数字
     * <li>  是一个数字字母 (比如一个罗马数字字母)
     * <li>  是一个合成标记
     * <li>  是一个非空格标记
     * <li> {@code isIdentifierIgnorable} 对该字符返回{@code true}.
     * </ul>
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符可以为 Unicode 标识符的一部分,那么返回{@code true}; 否则返回{@code false}.
     * @see     Character#isIdentifierIgnorable(int)
     * @see     Character#isJavaIdentifierPart(int)
     * @see     Character#isLetterOrDigit(int)
     * @see     Character#isUnicodeIdentifierStart(int)
     * @since   1.5
     */
    public static boolean isUnicodeIdentifierPart(int codePoint) {
        return CharacterData.of(codePoint).isUnicodeIdentifierPart(codePoint);
    }

    /**
     * 确定是否应该认为指定字符是 Java 标识符或 Unicode 标识符中可忽略的一个字符.
     * <p>
     * 以下 Unicode 字符是 Java 标识符或 Unicode 标识符中可忽略的字符:
     * <ul>
     * <li>非空白的 ISO 控制字符
     * <ul>
     * <li>{@code '\u005Cu0000'} 到 {@code '\u005Cu0008'}
     * <li>{@code '\u005Cu000E'} 到 {@code '\u005Cu001B'}
     * <li>{@code '\u005Cu007F'} 到 {@code '\u005Cu009F'}
     * </ul>
     *
     * <li>拥有 {@code FORMAT} 常规类别值的所有字符
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isIdentifierIgnorable(int)}方法.
     *
     * @param   ch      要测试的字符.
     * @return  如果字符是可以成为 Java 或 Unicode 标识符的一部分的可忽略控制字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isJavaIdentifierPart(char)
     * @see     Character#isUnicodeIdentifierPart(char)
     * @since   1.1
     */
    public static boolean isIdentifierIgnorable(char ch) {
        return isIdentifierIgnorable((int)ch);
    }

    /**
     * 确定是否应该认为指定字符(Unicode代码点)是 Java 标识符或 Unicode 标识符中可忽略的一个字符.
     * <p>
     * 以下 Unicode 字符是 Java 标识符或 Unicode 标识符中可忽略的字符:
     * <ul>
     * <li>非空白的 ISO 控制字符
     * <ul>
     * <li>{@code '\u005Cu0000'} 到 {@code '\u005Cu0008'}
     * <li>{@code '\u005Cu000E'} 到 {@code '\u005Cu001B'}
     * <li>{@code '\u005Cu007F'} 到 {@code '\u005Cu009F'}
     * </ul>
     *
     * <li>拥有 {@code FORMAT} 常规类别值的所有字符
     * </ul>
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符是可以成为 Java 或 Unicode 标识符的一部分的可忽略控制字符,那么返回{@code true};
     *          否则返回{@code false}..
     * @see     Character#isJavaIdentifierPart(int)
     * @see     Character#isUnicodeIdentifierPart(int)
     * @since   1.5
     */
    public static boolean isIdentifierIgnorable(int codePoint) {
        return CharacterData.of(codePoint).isIdentifierIgnorable(codePoint);
    }

    /**
     * 使用取自 UnicodeData 文件的大小写映射信息将字符参数转换为小写.
     * <p>
     * 需要注意的是,对于某些范围内的字符,特别是那些是符号或表意符号的字符,
     * {@code Character.isLowerCase(Character.toLowerCase(ch))},并不是总是返回{@code true}.
     *
     * <p>
     * 通常, 应当使用{@link String#toLowerCase()}来将字符影射为小写.
     * {@code String}大小写映射方法有几个胜过{@code Character}大小写映射方法的优点.
     * {@code String}大小写映射方法可以执行语言环境敏感的映射、上下文相关的映射和 1:M 字符映射,
     * 而 {@code Character} 大小写映射方法却不能
     *
     * <p>
     * <b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #toLowerCase(int)}方法.
     *
     * @param   ch   要转换的字符.
     * @return  等效于该字符的小写形式,如果有的话;否则返回该字符本身.
     * @see     Character#isLowerCase(char)
     * @see     String#toLowerCase()
     */
    public static char toLowerCase(char ch) {
        return (char)toLowerCase((int)ch);
    }

    /**
     * 使用取自 UnicodeData 文件的大小写映射信息将字符(Unicode 代码点)参数转换为小写.
     *
     * <p>
     * 需要注意的是,对于某些范围内的字符,特别是那些是符号或表意符号的字符,
     * {@code Character.isLowerCase(Character.toLowerCase(codePoint))},并不是总是返回{@code true}.
     *
     * <p>
     * 通常, 应当使用{@link String#toLowerCase()}来将字符影射为小写.
     * {@code String}大小写映射方法有几个胜过{@code Character}大小写映射方法的优点.
     * {@code String}大小写映射方法可以执行语言环境敏感的映射、上下文相关的映射和 1:M 字符映射,
     * 而 {@code Character} 大小写映射方法却不能
     *
     * @param   codePoint   要转换的字符(Unicode 代码点).
     * @return  等效于该字符(Unicode代码点)的小写形式,如果有的话;否则返回该字符本身.
     * @see     Character#isLowerCase(int)
     * @see     String#toLowerCase()
     *
     * @since   1.5
     */
    public static int toLowerCase(int codePoint) {
        return CharacterData.of(codePoint).toLowerCase(codePoint);
    }

    /**
     * 使用取自 UnicodeData 文件的大小写映射信息将字符参数转换为大写.
     * <p>
     * 需要注意的是,对于某些范围内的字符,特别是那些是符号或表意符号的字符,
     * {@code Character.isUpperCase(Character.toUpperCase(ch))},并不是总是返回{@code true}.
     *
     * <p>
     * 通常, 应当使用{@link String#toUpperCase()}来将字符影射为大写.
     * {@code String}大小写映射方法有几个胜过{@code Character}大小写映射方法的优点.
     * {@code String}大小写映射方法可以执行语言环境敏感的映射、上下文相关的映射和 1:M 字符映射,
     * 而 {@code Character} 大小写映射方法却不能.
     *
     * <p>
     * <b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #toUpperCase(int)}方法.
     *
     * @param   ch   要转换的字符.
     * @return  等效于该字符的大写形式,如果有的话;否则返回该字符本身.
     * @see     Character#isUpperCase(char)
     * @see     String#toUpperCase()
     */
    public static char toUpperCase(char ch) {
        return (char)toUpperCase((int)ch);
    }

    /**
     * 使用取自 UnicodeData 文件的大小写映射信息将字符(Unicode 代码点)参数转换为大写.
     *
     * <p>
     * 需要注意的是,对于某些范围内的字符,特别是那些是符号或表意符号的字符,
     * {@code Character.isUpperCase(Character.toUpperCase(codePoint))},并不是总是返回{@code true}.
     *
     * <p>
     * 通常, 应当使用{@link String#toUpperCase()}来将字符影射为大写.
     * {@code String}大小写映射方法有几个胜过{@code Character}大小写映射方法的优点.
     * {@code String}大小写映射方法可以执行语言环境敏感的映射、上下文相关的映射和 1:M 字符映射,
     * 而 {@code Character} 大小写映射方法却不能.
     *
     * @param   codePoint   要转换的字符(Unicode 代码点).
     * @return  等效于该字符(Unicode代码点)的大写形式,如果有的话;否则返回该字符本身.
     * @see     Character#isUpperCase(int)
     * @see     String#toUpperCase()
     *
     * @since   1.5
     */
    public static int toUpperCase(int codePoint) {
        return CharacterData.of(codePoint).toUpperCase(codePoint);
    }

    /**
     * 使用取自 UnicodeData 文件的大小写映射信息将字符参数转换为首字母大写.
     * 如果字符没有明确的首字母大写映射, 并且根据 UnicodeData文件, 它本身并不是一个首字母大写的 char, 
     * 则返回大写映射作为等效的首字母大写映射.
     * 如果 {@code char} 参数总是一个首字母大写的 {@code char}, 则返回相同的 {@code char} 值.
     * 
     * <p>
     * 需要注意的是,对于某些范围内的字符,
     * {@code Character.isTitleCase(Character.toTitleCase(ch))}并不是总是返回{@code true}.
     *
     * <p>
     * <b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #toTitleCase(int)}方法.
     *
     * @param   ch   要转换的字符.
     * @return  等效于该字符的首字母大写形式,如果有的话;否则返回该字符本身.
     *          otherwise, the character itself.
     * @see     Character#isTitleCase(char)
     * @see     Character#toLowerCase(char)
     * @see     Character#toUpperCase(char)
     * @since   1.0.2
     */
    public static char toTitleCase(char ch) {
        return (char)toTitleCase((int)ch);
    }

    /**
     * 使用取自 UnicodeData 文件的大小写映射信息将字符(Unicode 代码点)参数转换为首字母大写.
     * 如果字符没有明确的首字母大写映射, 并且根据 UnicodeData文件, 它本身并不是一个首字母大写的 char, 
     * 则返回大写映射作为等效的首字母大写映射.
     * 如果 {@code char} 参数总是一个首字母大写的 {@code char}, 则返回相同的 {@code char} 值.
     *
     * <p>
     * 需要注意的是,对于某些范围内的字符,
     * {@code Character.isTitleCase(Character.toTitleCase(codePoint))}并不是总是返回{@code true}.
     *
     * @param   codePoint   要转换的字符(Unicode 代码点).
     * @return  等效于该字符(Unicode代码点)的首字母大写形式,如果有的话;否则返回该字符本身.
     * @see     Character#isTitleCase(int)
     * @see     Character#toLowerCase(int)
     * @see     Character#toUpperCase(int)
     * @since   1.5
     */
    public static int toTitleCase(int codePoint) {
        return CharacterData.of(codePoint).toTitleCase(codePoint);
    }

    /**
     * 返回使用指定基数的字符{@code ch}的数值.
     * <p>
     * 如果基数不在 {@code MIN_RADIX} &le; {@code radix} &le; {@code MAX_RADIX} 返回之内,
     * 或者使用指定的基数的{@code ch}的值并不是一个有效的数字,那么返回{@code -1}.
     * 如果以下条件中至少有一个为真, 则字符是一个有效数字:
     * <ul>
     * <li>该字符的{@code isDigit}方法为{@code true},
     *     且字符(或分解的单字符)的 Unicode 十进制数值小于指定的基数.
     *     在这种情况下,返回十进制数值.
     * <li>字符为大写拉丁字母 {@code 'A'} 到 {@code 'Z'} 范围内的字母之一,
     *     且它的代码小于 {@code radix + 'A' - 10}.
     *     在这种情况下,返回 {@code ch - 'A' + 10}.
     * <li>字符为小写拉丁字母 {@code 'a'} 到 {@code 'z'} 范围内的字母之一,
     *     且它的代码小于 {@code radix + 'a' - 10}.
     *     在这种情况下,返回 {@code ch - 'a' + 10}.
     * <li>字符为全宽大写拉丁字母 A({@code '\u005CuFF21'}) 到 Z ({@code '\u005CuFF3A'}) 范围内的字母之一,
     *     且它的代码小于 {@code radix + '\u005CuFF21' - 10}.
     *     在这种情况下,返回 {@code ch - '\u005CuFF21' + 10}.
     * <li>字符为全宽小写拉丁字母 a({@code '\u005CuFF41'}) 到 z ({@code '\u005CuFF5A'}) 范围内的字母之一,
     *     且它的代码小于 {@code radix + '\u005CuFF41' - 10}.
     *     在这种情况下,返回 {@code ch - '\u005CuFF41' + 10}.
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #digit(int, int)}方法.
     *
     * @param   ch      要转换的字符.
     * @param   radix  基数.
     * @return  使用指定基数的字符所表示的数值.
     * @see     Character#forDigit(int, int)
     * @see     Character#isDigit(char)
     */
    public static int digit(char ch, int radix) {
        return digit((int)ch, radix);
    }

    /**
     * 返回使用指定基数的字符(Unicode 代码点)的数值.
     *
     * <p>
     * 如果基数不在 {@code MIN_RADIX} &le; {@code radix} &le; {@code MAX_RADIX} 返回之内,
     * 或者使用指定的基数的{@code ch}的值并不是一个有效的数字,那么返回{@code -1}.
     * 如果以下条件中至少有一个为真, 则字符是一个有效数字:
     * <ul>
     * <li>该字符的{@link #isDigit(int) isDigit(codePoint)}方法为{@code true},
     *     且字符(或分解的单字符)的 Unicode 十进制数值小于指定的基数.
     *     在这种情况下,返回十进制数值.
     * <li>字符为大写拉丁字母 {@code 'A'} 到 {@code 'Z'} 范围内的字母之一,
     *     且它的代码小于 {@code radix + 'A' - 10}.
     *     在这种情况下,返回 {@code ch - 'A' + 10}.
     * <li>字符为小写拉丁字母 {@code 'a'} 到 {@code 'z'} 范围内的字母之一,
     *     且它的代码小于 {@code radix + 'a' - 10}.
     *     在这种情况下,返回 {@code ch - 'a' + 10}.
     * <li>字符为全宽大写拉丁字母 A({@code '\u005CuFF21'}) 到 Z ({@code '\u005CuFF3A'}) 范围内的字母之一,
     *     且它的代码小于 {@code radix + '\u005CuFF21' - 10}.
     *     在这种情况下,返回 {@code ch - '\u005CuFF21' + 10}.
     * <li>字符为全宽小写拉丁字母 a({@code '\u005CuFF41'}) 到 z ({@code '\u005CuFF5A'}) 范围内的字母之一,
     *     且它的代码小于 {@code radix + '\u005CuFF41' - 10}.
     *     在这种情况下,返回 {@code ch - '\u005CuFF41' + 10}.
     * </ul>
     *
     * @param   codePoint 要转换的字符(Unicode 代码点).
     * @param   radix  基数.
     * @return  使用指定基数的字符所表示的数值.
     * @see     Character#forDigit(int, int)
     * @see     Character#isDigit(int)
     * @since   1.5
     */
    public static int digit(int codePoint, int radix) {
        return CharacterData.of(codePoint).digit(codePoint, radix);
    }

    /**
     * 返回指定的 Unicode 字符表示的{@code int}值.
     * 例如,字符  {@code '\u005Cu216C'}(罗马数字50)将返回一个值为50的整数.
     * <p>
     * 字母 A-Z 的大写 ({@code '\u005Cu0041'}到{@code '\u005Cu005A'}),
     * 小写({@code '\u005Cu0061'}到{@code '\u005Cu007A'}),
     * 以及全宽变体({@code '\u005CuFF21'}到{@code '\u005CuFF3A'} 和 {@code '\u005CuFF41'}到{@code '\u005CuFF5A'})
     * 形式拥有从 10 到 35 的数值.
     * 独立于 Unicode 规范,该规范没有为这些{@code char}值分配数值.
     * <p>
     * 如果字符中没有数值,则返回 -1.如果字符中有一个数值,但无法将它表示为非负整数(例如,小数值),则返回 -2.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #getNumericValue(int)} 方法.
     *
     * @param   ch      要转换的字符.
     * @return  以非负{@code int}值形式返回字符的数值;如果字符中有一个不是非负整数的数值,则返回 -2;如果字符中没有数值,则返回 -1;
     * @see     Character#forDigit(int, int)
     * @see     Character#isDigit(char)
     * @since   1.1
     */
    public static int getNumericValue(char ch) {
        return getNumericValue((int)ch);
    }

    /**
     * 返回指定的 Unicode 字符(Unicode代码点)表示的{@code int}值.
     * 例如,字符  {@code '\u005Cu216C'}(罗马数字50)将返回一个值为50的整数.
     * <p>
     * 字母 A-Z 的大写 ({@code '\u005Cu0041'}到{@code '\u005Cu005A'}),
     * 小写({@code '\u005Cu0061'}到{@code '\u005Cu007A'}),
     * 以及全宽变体({@code '\u005CuFF21'}到{@code '\u005CuFF3A'} 和 {@code '\u005CuFF41'}到{@code '\u005CuFF5A'})
     * 形式拥有从 10 到 35 的数值.
     * 独立于 Unicode 规范,该规范没有为这些{@code char}值分配数值.
     * <p>
     * 如果字符中没有数值,则返回 -1.如果字符中有一个数值,但无法将它表示为非负整数(例如,小数值),则返回 -2.
     *
     * @param   codePoint 要转换的字符(Unicode 代码点).
     * @return  以非负{@code int}值形式返回字符的数值;如果字符中有一个不是非负整数的数值,则返回 -2;如果字符中没有数值,则返回 -1;
     * @see     Character#forDigit(int, int)
     * @see     Character#isDigit(int)
     * @since   1.5
     */
    public static int getNumericValue(int codePoint) {
        return CharacterData.of(codePoint).getNumericValue(codePoint);
    }

    /**
     * 确定指定字符是否为 ISO-LATIN-1 空白(white space).该方法只对以下五个字符返回{@code true}:
     * <table summary="truechars">
     * <tr><td>{@code '\t'}</td>            <td>{@code U+0009}</td>
     *     <td>{@code HORIZONTAL TABULATION}</td></tr>
     * <tr><td>{@code '\n'}</td>            <td>{@code U+000A}</td>
     *     <td>{@code NEW LINE}</td></tr>
     * <tr><td>{@code '\f'}</td>            <td>{@code U+000C}</td>
     *     <td>{@code FORM FEED}</td></tr>
     * <tr><td>{@code '\r'}</td>            <td>{@code U+000D}</td>
     *     <td>{@code CARRIAGE RETURN}</td></tr>
     * <tr><td>{@code ' '}</td>             <td>{@code U+0020}</td>
     *     <td>{@code SPACE}</td></tr>
     * </table>
     *
     * @param      ch   要测试的字符.
     * @return     如果字符为 ISO-LATIN-1 空白,那么返回{@code true};
     *             否则返回{@code false}.
     * @see        Character#isSpaceChar(char)
     * @see        Character#isWhitespace(char)
     * @deprecated 被isWhitespace(char)代替.
     */
    @Deprecated
    public static boolean isSpace(char ch) {
        return (ch <= 0x0020) &&
            (((((1L << 0x0009) |
            (1L << 0x000A) |
            (1L << 0x000C) |
            (1L << 0x000D) |
            (1L << 0x0020)) >> ch) & 1L) != 0);
    }


    /**
     * 确定指定的字符是否是一个 Unicode 空白字符(space character).
     * 当且仅当根据 Unicode 标准将字符指定为空白字符时, 才认为字符是一个空白字符.
     * 如果字符的常规类别的类型为以下类型中的任意一种, 则该方法返回 true:
     * <ul>
     * <li> {@code SPACE_SEPARATOR}
     * <li> {@code LINE_SEPARATOR}
     * <li> {@code PARAGRAPH_SEPARATOR}
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isSpaceChar(int)} 方法.
     *
     * @param   ch      要测试的字符.
     * @return  如果指定的字符是一个空白字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isWhitespace(char)
     * @since   1.1
     */
    public static boolean isSpaceChar(char ch) {
        return isSpaceChar((int)ch);
    }

    /**
     * 确定指定的字符(Unicode 代码点 )是否是一个Unicode 空白字符.
     * 当且仅当根据 Unicode 标准将字符指定为空白字符时, 才认为字符是一个空白字符.
     * 如果字符的常规类别的类型为以下类型中的任意一种, 则该方法返回 true:
     *
     * <ul>
     * <li> {@link #SPACE_SEPARATOR}
     * <li> {@link #LINE_SEPARATOR}
     * <li> {@link #PARAGRAPH_SEPARATOR}
     * </ul>
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果指定的字符是一个空白字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isWhitespace(int)
     * @since   1.5
     */
    public static boolean isSpaceChar(int codePoint) {
        return ((((1 << Character.SPACE_SEPARATOR) |
                  (1 << Character.LINE_SEPARATOR) |
                  (1 << Character.PARAGRAPH_SEPARATOR)) >> getType(codePoint)) & 1)
            != 0;
    }

    /**
     * 确定指定字符依据 Java 标准是否为空白字符(whitespace character).
     * 当且仅当字符满足以下标准时,该字符才是一个 Java 空白字符:
     * <ul>
     * <li> 它是 Unicode 空格字符({@code SPACE_SEPARATOR},{@code LINE_SEPARATOR}, 或者 {@code PARAGRAPH_SEPARATOR})
     *      但不是非中断空格 ({@code '\u005Cu00A0'},{@code '\u005Cu2007'}, {@code '\u005Cu202F'}).
     * <li> 它是 {@code '\u005Ct'}, U+0009 HORIZONTAL TABULATION.
     * <li> 它是 {@code '\u005Cn'}, U+000A LINE FEED.
     * <li> 它是 {@code '\u005Cu000B'}, U+000B VERTICAL TABULATION.
     * <li> 它是 {@code '\u005Cf'}, U+000C FORM FEED.
     * <li> 它是 {@code '\u005Cr'}, U+000D CARRIAGE RETURN.
     * <li> 它是 {@code '\u005Cu001C'}, U+001C FILE SEPARATOR.
     * <li> 它是 {@code '\u005Cu001D'}, U+001D GROUP SEPARATOR.
     * <li> 它是 {@code '\u005Cu001E'}, U+001E RECORD SEPARATOR.
     * <li> 它是 {@code '\u005Cu001F'}, U+001F UNIT SEPARATOR.
     * </ul>
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isWhitespace(int)} 方法.
     *
     * @param   ch 要测试的字符.
     * @return  如果指定的字符是一个 Java 空白字符,那么返回{@code true}; 否则返回{@code false}.
     * @see     Character#isSpaceChar(char)
     * @since   1.1
     */
    public static boolean isWhitespace(char ch) {
        return isWhitespace((int)ch);
    }

    /**
     * 确定指定字符(Unicode 代码点)依据 Java 标准是否为空白字符(whitespace character).
     * 当且仅当字符满足以下标准时,该字符才是一个 Java 空白字符:
     * <ul>
     * <li> 它是 Unicode 空格字符({@code SPACE_SEPARATOR},{@code LINE_SEPARATOR}, 或者 {@code PARAGRAPH_SEPARATOR})
     *      但不是非中断空格 ({@code '\u005Cu00A0'},{@code '\u005Cu2007'}, {@code '\u005Cu202F'}).
     * <li> 它是 {@code '\u005Ct'}, U+0009 HORIZONTAL TABULATION.
     * <li> 它是 {@code '\u005Cn'}, U+000A LINE FEED.
     * <li> 它是 {@code '\u005Cu000B'}, U+000B VERTICAL TABULATION.
     * <li> 它是 {@code '\u005Cf'}, U+000C FORM FEED.
     * <li> 它是 {@code '\u005Cr'}, U+000D CARRIAGE RETURN.
     * <li> 它是 {@code '\u005Cu001C'}, U+001C FILE SEPARATOR.
     * <li> 它是 {@code '\u005Cu001D'}, U+001D GROUP SEPARATOR.
     * <li> 它是 {@code '\u005Cu001E'}, U+001E RECORD SEPARATOR.
     * <li> 它是 {@code '\u005Cu001F'}, U+001F UNIT SEPARATOR.
     * </ul>
     * <p>
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果指定的字符是一个 Java 空白字符,那么返回{@code true}; 否则返回{@code false}.
     * @see     Character#isSpaceChar(int)
     * @since   1.5
     */
    public static boolean isWhitespace(int codePoint) {
        return CharacterData.of(codePoint).isWhitespace(codePoint);
    }

    /**
     * 确定指定的字符是否是一个ISO控制字符.
     * 如果字符的代码在从{@code '\u005Cu0000'}到 {@code '\u005Cu001F'}
     * 或者从 {@code '\u005Cu007F'} 到 {@code '\u005Cu009F'}的范围内,
     * 则被认为是ISO控制字符.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isISOControl(int)} 方法.
     *
     * @param   ch      要测试的字符.
     * @return  如果指定的字符是一个ISO控制字符,那么返回{@code true;
     *          否则返回{@code false}.
     *
     * @see     Character#isSpaceChar(char)
     * @see     Character#isWhitespace(char)
     * @since   1.1
     */
    public static boolean isISOControl(char ch) {
        return isISOControl((int)ch);
    }

    /**
     * 确定指定的字符(Unicode代码点)是否是一个ISO控制字符.
     * 如果字符的代码在从{@code '\u005Cu0000'}到 {@code '\u005Cu001F'}
     * 或者从 {@code '\u005Cu007F'} 到 {@code '\u005Cu009F'}的范围内,
     * 则被认为是ISO控制字符.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果指定的字符是一个ISO控制字符,那么返回{@code true};
     *          否则返回{@code false}.
     * @see     Character#isSpaceChar(int)
     * @see     Character#isWhitespace(int)
     * @since   1.5
     */
    public static boolean isISOControl(int codePoint) {
        // Optimized form of:
        //     (codePoint >= 0x00 && codePoint <= 0x1F) ||
        //     (codePoint >= 0x7F && codePoint <= 0x9F);
        return codePoint <= 0x9F &&
            (codePoint >= 0x7F || (codePoint >>> 5 == 0));
    }

    /**
     * 返回一个指示字符的常规类别的值.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #getType(int)}方法.
     *
     * @param   ch      要测试的字符.
     * @return  表示字符的常规类别的{@code int}类型的值 {@code int}.
     * @see     Character#COMBINING_SPACING_MARK
     * @see     Character#CONNECTOR_PUNCTUATION
     * @see     Character#CONTROL
     * @see     Character#CURRENCY_SYMBOL
     * @see     Character#DASH_PUNCTUATION
     * @see     Character#DECIMAL_DIGIT_NUMBER
     * @see     Character#ENCLOSING_MARK
     * @see     Character#END_PUNCTUATION
     * @see     Character#FINAL_QUOTE_PUNCTUATION
     * @see     Character#FORMAT
     * @see     Character#INITIAL_QUOTE_PUNCTUATION
     * @see     Character#LETTER_NUMBER
     * @see     Character#LINE_SEPARATOR
     * @see     Character#LOWERCASE_LETTER
     * @see     Character#MATH_SYMBOL
     * @see     Character#MODIFIER_LETTER
     * @see     Character#MODIFIER_SYMBOL
     * @see     Character#NON_SPACING_MARK
     * @see     Character#OTHER_LETTER
     * @see     Character#OTHER_NUMBER
     * @see     Character#OTHER_PUNCTUATION
     * @see     Character#OTHER_SYMBOL
     * @see     Character#PARAGRAPH_SEPARATOR
     * @see     Character#PRIVATE_USE
     * @see     Character#SPACE_SEPARATOR
     * @see     Character#START_PUNCTUATION
     * @see     Character#SURROGATE
     * @see     Character#TITLECASE_LETTER
     * @see     Character#UNASSIGNED
     * @see     Character#UPPERCASE_LETTER
     * @since   1.1
     */
    public static int getType(char ch) {
        return getType((int)ch);
    }

    /**
     * 返回一个指示字符的常规类别的值.
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  表示字符的常规类别的{@code int}类型的值 {@code int}.
     * @see     Character#COMBINING_SPACING_MARK COMBINING_SPACING_MARK
     * @see     Character#CONNECTOR_PUNCTUATION CONNECTOR_PUNCTUATION
     * @see     Character#CONTROL CONTROL
     * @see     Character#CURRENCY_SYMBOL CURRENCY_SYMBOL
     * @see     Character#DASH_PUNCTUATION DASH_PUNCTUATION
     * @see     Character#DECIMAL_DIGIT_NUMBER DECIMAL_DIGIT_NUMBER
     * @see     Character#ENCLOSING_MARK ENCLOSING_MARK
     * @see     Character#END_PUNCTUATION END_PUNCTUATION
     * @see     Character#FINAL_QUOTE_PUNCTUATION FINAL_QUOTE_PUNCTUATION
     * @see     Character#FORMAT FORMAT
     * @see     Character#INITIAL_QUOTE_PUNCTUATION INITIAL_QUOTE_PUNCTUATION
     * @see     Character#LETTER_NUMBER LETTER_NUMBER
     * @see     Character#LINE_SEPARATOR LINE_SEPARATOR
     * @see     Character#LOWERCASE_LETTER LOWERCASE_LETTER
     * @see     Character#MATH_SYMBOL MATH_SYMBOL
     * @see     Character#MODIFIER_LETTER MODIFIER_LETTER
     * @see     Character#MODIFIER_SYMBOL MODIFIER_SYMBOL
     * @see     Character#NON_SPACING_MARK NON_SPACING_MARK
     * @see     Character#OTHER_LETTER OTHER_LETTER
     * @see     Character#OTHER_NUMBER OTHER_NUMBER
     * @see     Character#OTHER_PUNCTUATION OTHER_PUNCTUATION
     * @see     Character#OTHER_SYMBOL OTHER_SYMBOL
     * @see     Character#PARAGRAPH_SEPARATOR PARAGRAPH_SEPARATOR
     * @see     Character#PRIVATE_USE PRIVATE_USE
     * @see     Character#SPACE_SEPARATOR SPACE_SEPARATOR
     * @see     Character#START_PUNCTUATION START_PUNCTUATION
     * @see     Character#SURROGATE SURROGATE
     * @see     Character#TITLECASE_LETTER TITLECASE_LETTER
     * @see     Character#UNASSIGNED UNASSIGNED
     * @see     Character#UPPERCASE_LETTER UPPERCASE_LETTER
     * @since   1.5
     */
    public static int getType(int codePoint) {
        return CharacterData.of(codePoint).getType(codePoint);
    }

    /**
     * 确定使用指定基数的特定数字的字符表示形式.
     * 如果{@code radix}的值不是一个有效基数,或者{@code digit}的值不是一个使用指定基数的有效数字,
     * 则返回null字符({@code '\u005Cu0000'}).
     * <p>
     * 如果{@code radix}参数大于等于{@code MIN_RADIX}并且小于等于{@code MAX_RADIX},则该参数是有效的.
     * 如果{@code 0 <= digit < radix},则该参数是有效的.
     * <p>
     * 如果数字小于 10,则返回{@code '0' + digit}.否则,返回的值为{@code 'a' + digit - 10}.
     *
     * @param   digit   转换为字符的数字.
     * @param   radix  基数.
     * @return  使用指定基数的指定数字的{@code char}表示形式.
     * @see     Character#MIN_RADIX
     * @see     Character#MAX_RADIX
     * @see     Character#digit(char, int)
     */
    public static char forDigit(int digit, int radix) {
        if ((digit >= radix) || (digit < 0)) {
            return '\0';
        }
        if ((radix < Character.MIN_RADIX) || (radix > Character.MAX_RADIX)) {
            return '\0';
        }
        if (digit < 10) {
            return (char)('0' + digit);
        }
        return (char)('a' - 10 + digit);
    }

    /**
     * 返回给定字符的 Unicode 方向属性.
     * 利用字符方向性来计算文本的视觉顺序.未定义的{@code char}值的方向值是{@code DIRECTIONALITY_UNDEFINED}.
     *
     * <p><b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #getDirectionality(int)} 方法.
     *
     * @param  ch 为其请求方向属性的{@code char}.
     * @return {@code char}值的方向属性.
     *
     * @see Character#DIRECTIONALITY_UNDEFINED
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR
     * @see Character#DIRECTIONALITY_ARABIC_NUMBER
     * @see Character#DIRECTIONALITY_COMMON_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_NONSPACING_MARK
     * @see Character#DIRECTIONALITY_BOUNDARY_NEUTRAL
     * @see Character#DIRECTIONALITY_PARAGRAPH_SEPARATOR
     * @see Character#DIRECTIONALITY_SEGMENT_SEPARATOR
     * @see Character#DIRECTIONALITY_WHITESPACE
     * @see Character#DIRECTIONALITY_OTHER_NEUTRALS
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE
     * @see Character#DIRECTIONALITY_POP_DIRECTIONAL_FORMAT
     * @since 1.4
     */
    public static byte getDirectionality(char ch) {
        return getDirectionality((int)ch);
    }

    /**
     * 返回给定字符(Unicode代码点)的 Unicode 方向属性.
     * 利用字符方向性来计算文本的视觉顺序.未定义的{@code char}值的方向值是{@code DIRECTIONALITY_UNDEFINED}.
     *
     * @param   codePoint 为其请求方向属性的字符(Unicode代码点).
     * @return 字符的方向属性.
     *
     * @see Character#DIRECTIONALITY_UNDEFINED DIRECTIONALITY_UNDEFINED
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT DIRECTIONALITY_LEFT_TO_RIGHT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT DIRECTIONALITY_RIGHT_TO_LEFT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER DIRECTIONALITY_EUROPEAN_NUMBER
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR
     * @see Character#DIRECTIONALITY_ARABIC_NUMBER DIRECTIONALITY_ARABIC_NUMBER
     * @see Character#DIRECTIONALITY_COMMON_NUMBER_SEPARATOR DIRECTIONALITY_COMMON_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_NONSPACING_MARK DIRECTIONALITY_NONSPACING_MARK
     * @see Character#DIRECTIONALITY_BOUNDARY_NEUTRAL DIRECTIONALITY_BOUNDARY_NEUTRAL
     * @see Character#DIRECTIONALITY_PARAGRAPH_SEPARATOR DIRECTIONALITY_PARAGRAPH_SEPARATOR
     * @see Character#DIRECTIONALITY_SEGMENT_SEPARATOR DIRECTIONALITY_SEGMENT_SEPARATOR
     * @see Character#DIRECTIONALITY_WHITESPACE DIRECTIONALITY_WHITESPACE
     * @see Character#DIRECTIONALITY_OTHER_NEUTRALS DIRECTIONALITY_OTHER_NEUTRALS
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE
     * @see Character#DIRECTIONALITY_POP_DIRECTIONAL_FORMAT DIRECTIONALITY_POP_DIRECTIONAL_FORMAT
     * @since    1.5
     */
    public static byte getDirectionality(int codePoint) {
        return CharacterData.of(codePoint).getDirectionality(codePoint);
    }

    /**
     * 确定指定字符依据Unicode规范是否对称.
     * 当显示在以从右至左的方式显示的文本中时,对称字符的字形在水平方向上应该是对称的.
     * 例如,{@code '\u005Cu0028'} LEFT PARENTHESIS 在语义上被定义为是 开括号.
     * 在从左至右显示的文本中,它将显示为"(",但在以从右至左的方式显示的文本中,它显示为")".
     *
     * <p>
     * <b>注意:</b> 此方法无法处理<a href="#supplementary">增补字符</a>.
     * 若要支持所有 Unicode 字符, 包括增补字符,请使用 {@link #isMirrored(int)} 方法.
     *
     * @param  ch 为其请求对称属性的{@code char}
     * @return 如果字符为对称字符,那么返回{@code true};
     *         如果{@code char}不是对称的或者尚未定义,那么返回{@code false}.
     * @since 1.4
     */
    public static boolean isMirrored(char ch) {
        return isMirrored((int)ch);
    }

    /**
     * 确定指定字符(Unicode代码点)依据Unicode规范是否对称.
     * 当显示在以从右至左的方式显示的文本中时,对称字符的字形在水平方向上应该是对称的.
     * 例如,{@code '\u005Cu0028'} LEFT PARENTHESIS 在语义上被定义为是 开括号.
     * 在从左至右显示的文本中,它将显示为"(",但在以从右至左的方式显示的文本中,它显示为")".
     *
     * @param   codePoint 要测试的字符(Unicode 代码点).
     * @return  如果字符为对称字符,那么返回{@code true};
     *         如果{@code char}不是对称的或者尚未定义,那么返回{@code false}.
     * @since   1.5
     */
    public static boolean isMirrored(int codePoint) {
        return CharacterData.of(codePoint).isMirrored(codePoint);
    }

    /**
     * 从数值上比较两个{@code Character}对象.
     *
     * @param   anotherCharacter   要比较的{@code Character}.

     * @return  如果{@code Character}参数等于此{@code Character}对象,那么返回{@code 0};
     *          如果此{@code Character}对象小于{@code Character}参数,那么返回小于{@code 0}的值;
     *          如果此{@code Character}对象大于{@code Character}参数,那么返回大于{@code 0}的值(无符号比较).
     *          需要注意的是,这是严格的数字比较;它并不依赖于区域.
     * @since   1.2
     */
    public int compareTo(Character anotherCharacter) {
        return compare(this.value, anotherCharacter.value);
    }

    /**
     * 从数值上比较两个{@code char}值.
     * 返回值同下列代码的返回值一样:
     * <pre>
     *    Character.valueOf(x).compareTo(Character.valueOf(y))
     * </pre>
     *
     * @param  x 要比较的第一个{@code char}.
     * @param  y 要比较的第二个{@code char}.
     * @return 如果{@code x == y},那么返回{@code 0};
     *         如果{@code x < y},那么返回小于{@code 0}的值;
     *         如果{@code x > y},那么返回大于{@code 0}的值;
     * @since 1.7
     */
    public static int compare(char x, char y) {
        return x - y;
    }

    /**
     * 使用来自UnicodeData文件的信息将字符(Unicode 代码点)转换为大写．
     * <p>
     *
     * @param   codePoint   要转换的字符(Unicode 代码点).
     * @return  该字符的等价大写形式, 如果存在的话．
     *          或者返回错误标识({@code Character.ERROR}),这表明存在1:M的{@code char}映射．
     * @see     Character#isLowerCase(char)
     * @see     Character#isUpperCase(char)
     * @see     Character#toLowerCase(char)
     * @see     Character#toTitleCase(char)
     * @since 1.4
     */
    static int toUpperCaseEx(int codePoint) {
        assert isValidCodePoint(codePoint);
        return CharacterData.of(codePoint).toUpperCaseEx(codePoint);
    }

    /**
     * 使用来自Unicode规范中的SpecialCasing文件中的信息将字符(Unicode 代码点)转换为大写．
     * 如果一个字符并没有确切的大写映射,那么将{@code char}自身存在数组{@code char[]}中返回.
     *
     * @param   codePoint   要转换的字符(Unicode 代码点).
     * @return 包含大写字符的{@code char[]}.
     * @since 1.4
     */
    static char[] toUpperCaseCharArray(int codePoint) {
        // As of Unicode 6.0, 1:M uppercasings only happen in the BMP.
        assert isBmpCodePoint(codePoint);
        return CharacterData.of(codePoint).toUpperCaseCharArray(codePoint);
    }

    /**
     * 用来以无符号二进制形式表示<tt>char</tt>值的位数, 为常量 {@code 16}.
     *
     * @since 1.5
     */
    public static final int SIZE = 16;

    /**
     * 用来以无符号二进制形式表示<tt>char</tt>值的字节数, 为常量 {@code 16}.
     *
     * @since 1.8
     */
    public static final int BYTES = SIZE / Byte.SIZE;

    /**
     * 返回通过反转指定<tt>char</tt>值中的字节顺序而获得的值.
     *
     * @param ch 要反转字节顺序的{@code char}.
     * @return 通过反转(或者等效于交换)指定<tt>char</tt>值中的字节而获得的值.
     * @since 1.5
     */
    public static char reverseBytes(char ch) {
        return (char) (((ch & 0xFF00) >> 8) | (ch << 8));
    }

    /**
     * 返回指定的字符{@code codePoint}的Unicode名称,如果代码点为{@link #UNASSIGNED unassigned},那么返回null.
     * <p>
     * 注意: 如果字符并没有被<i>UnicodeData</i>文件(Unicode Consortium维护的Unicode Character Database的一部分)指定一个名称,
     * 那么返回的名称和以下表达式的结果一样:
     *
     * <blockquote>{@code
     *     Character.UnicodeBlock.of(codePoint).toString().replace('_', ' ')
     *     + " "
     *     + Integer.toHexString(codePoint).toUpperCase(Locale.ENGLISH);
     *
     * }</blockquote>
     *
     * @param  codePoint 字符(Unicode 代码点)
     *
     * @return 指定字符的Unicode名称, 如果代码点为{@link #UNASSIGNED unassigned},那么返回null.
     *
     * @exception IllegalArgumentException 如果指定的{@code codePoint}并不是一个有效的Unicode代码点.
     *
     * @since 1.7
     */
    public static String getName(int codePoint) {
        if (!isValidCodePoint(codePoint)) {
            throw new IllegalArgumentException();
        }
        String name = CharacterName.get(codePoint);
        if (name != null)
            return name;
        if (getType(codePoint) == UNASSIGNED)
            return null;
        UnicodeBlock block = UnicodeBlock.of(codePoint);
        if (block != null)
            return block.toString().replace('_', ' ') + " "
                   + Integer.toHexString(codePoint).toUpperCase(Locale.ENGLISH);
        // should never come here
        return Integer.toHexString(codePoint).toUpperCase(Locale.ENGLISH);
    }
}
