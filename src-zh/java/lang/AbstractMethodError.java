/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * 当一个应用尝试调用一个抽象方法时,抛出该错误.
 * 通常情况下, 该错误由编译器捕获;
 * 如果某个类的定义自当前执行方法最后一次编译以后作了不兼容的更改, 那么此错误只可能在运行时发生.
 *
 * @author  unascribed
 * @since   JDK1.0
 */
public
class AbstractMethodError extends IncompatibleClassChangeError {
    private static final long serialVersionUID = -1654391082989018462L;

    /**
     * 构造一个不带详细消息的<code>AbstractMethodError</code>.
     */
    public AbstractMethodError() {
        super();
    }

    /**
     * 构造一个使用指定详细消息的<code>AbstractMethodError</code>.
     *
     * @param   s  详细信息.
     */
    public AbstractMethodError(String s) {
        super(s);
    }
}
