/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * 用非法索引访问数组时抛出的异常.
 * 如果索引为负或大于等于数组大小,则该索引为非法索引.
 *
 * @author  unascribed
 * @since   JDK1.0
 */
public
class ArrayIndexOutOfBoundsException extends IndexOutOfBoundsException {
    private static final long serialVersionUID = -5116101128118950844L;

    /**
     * 构造一个不带详细消息的<code>ArrayIndexOutOfBoundsException</code>.
     */
    public ArrayIndexOutOfBoundsException() {
        super();
    }

    /**
     * 构造具有指示非法索引的参数的新的<code>ArrayIndexOutOfBoundsException</code>类.
     *
     * @param   index   非法的索引.
     */
    public ArrayIndexOutOfBoundsException(int index) {
        super("Array index out of range: " + index);
    }

    /**
     * 构造一个带指定详细消息的<code>ArrayIndexOutOfBoundsException</code>类.
     *
     * @param   s　详细消息.
     */
    public ArrayIndexOutOfBoundsException(String s) {
        super(s);
    }
}
