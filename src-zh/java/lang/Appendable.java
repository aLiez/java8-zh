/*
 * Copyright (c) 2003, 2004, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.io.IOException;

/**
 * 一个可增加<tt>char</tt>序列和值的对象.
 * 如果某个类的实例打算接收取自 {@link java.util.Formatter}的格式化输出,那么该类必须实现 <tt>Appendable</tt> 接口
 *
 * <p> 要添加的字符应该是<a href="Character.html#unicode">Unicode Character Representation</a>中所描述的有效的 Unicode 字符,
 * 需要注意的是, 增补字符可能由多个 16 位<tt>char</tt>值组成.
 *
 * <p> Appendable 对于多线程访问而言没必要是安全的.线程安全由扩展和实现此接口的类负责.
 *
 * <p> 由于此接口可能由现有的具有不同的错误处理风格的类实现,所以无法保证错误不会传播给调用者.
 *
 * @since 1.5
 */
public interface Appendable {

    /**
     * 向此 <tt>Appendable</tt> 添加指定字符.
     *
     * <p> 有时可能没有添加整个序列,这取决于使用哪个类来实现字符序列<tt>csq</tt>.
     * 例如,如果 <tt>csq</tt> 是 {@link java.nio.CharBuffer} 的一个实例,则通过缓冲区的位置和限制来定义要添加的子序列.
     *
     * @param  csq 要添加的字符串序列.如果 <tt>csq</tt> 为 <tt>null</tt>,则向该 Appendable 添加四个字符 <tt>"null"</tt>.
     *
     * @return  此 <tt>Appendable</tt> 的引用
     *
     * @throws  IOException 如果发生 I/O 错误
     */
    Appendable append(CharSequence csq) throws IOException;

    /**
     * 将指定字符序列的子序列添加至此<tt>Appendable</tt>.
     *
     * <p>当 <tt>csq</tt> 不为 <tt>null</tt> 时,
     * 用 <tt>out.append(csq, start, end)</tt> 的形式调用此方法与用以下形式调用此方法的行为完全相同:
     *
     * <pre>
     *     out.append(csq.subSequence(start, end)) </pre>
     *
     * @param  csq 子序列将被添加的字符序列.
     *             如果 <tt>csq</tt> 为 <tt>null</tt>,则向该 Appendable 添加四个字符 <tt>"null"</tt>,就好像 <tt>csq</tt> 包含这些字符一样.
     *
     * @param  start 子序列中第一个字符的索引
     *
     * @param  end 紧随子序列中最后一个字符的字符的索引
     *
     * @return  此 <tt>Appendable</tt> 的引用
     *
     * @throws  IndexOutOfBoundsException 如果 <tt>start</tt> 或 <tt>end</tt> 为负,
     *          以及 <tt>start</tt> 大于 <tt>end</tt> 或者 <tt>end</tt> 大于 <tt>csq.length()</tt>.
     *
     * @throws  IOException
     *          如果发生 I/O 错误
     */
    Appendable append(CharSequence csq, int start, int end) throws IOException;

    /**
     * 向此 <tt>Appendable</tt> 添加指定字符.
     *
     * @param  c 要添加的字符
     *
     * @return  此 <tt>Appendable</tt> 的引用
     *
     * @throws  IOException
     *          如果发生 I/O 错误
     */
    Appendable append(char c) throws IOException;
}
