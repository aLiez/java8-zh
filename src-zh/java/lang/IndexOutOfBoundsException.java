/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * 指示某排序索引(例如对array、string或vector的排序)超出范围时抛出.
 * <p>
 * 应用程序可以为这个类创建子类,以指示类似的异常.
 *
 * @author  Frank Yellin
 * @since   JDK1.0
 */
public
class IndexOutOfBoundsException extends RuntimeException {
    private static final long serialVersionUID = 234122996006267687L;

    /**
     * 构造不带详细消息的<code>IndexOutOfBoundsException</code>.
     */
    public IndexOutOfBoundsException() {
        super();
    }

    /**
     * 构造带指定详细消息的<code>IndexOutOfBoundsException</code>.
     *
     * @param   s  详细消息.
     */
    public IndexOutOfBoundsException(String s) {
        super(s);
    }
}
