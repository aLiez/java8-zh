/*
 * Copyright (c) 1996, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * {@code Void}类是一个不可实例化的类,它持有对Java关键字void的{@code Class}对象的引用.
 *
 * @author  unascribed
 * @since   JDK1.1
 */
public final
class Void {

    /**
     * 表示对应于关键字{@code void}的伪类型的{@code Class}对象.
     *
     */
    @SuppressWarnings("unchecked")
    public static final Class<Void> TYPE = (Class<Void>) Class.getPrimitiveClass("void");

    /*
     * Void类不能被实例化.
     */
    private Void() {}
}
