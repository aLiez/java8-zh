/*
 * Copyright (c) 1994, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 *
 * {@code Vector} 类可以实现可增长的对象数组.
 * 与数组一样,它包含可以使用整数索引进行访问的组件.
 * 但是,{@code Vector} 的大小可以根据需要增大或缩小,以适应创建 {@code Vector} 后进行添加或移除项的操作.
 *
 * <p>每个向量会试图通过维护 {@code capacity} 和 {@code capacityIncrement} 来优化存储管理.
 * {@code capacity} 始终至少应与向量的大小相等;这个值通常比后者大些,因为随着将组件添加到向量中,其存储将按 {@code capacityIncrement} 的大小增加存储块.
 * 应用程序可以在插入大量组件前增加向量的容量;这样就减少了重分配的量.
 *
 * <p><a name="fail-fast">
 * 由 Vector 的 {@link #iterator() iterator} 和 {@link #listIterator(int) listIterator} 方法
 * 所返回的迭代器是 <em>快速失败的</em></a>:如果在迭代器创建后的任意时间从结构上修改了向量
 * (通过迭代器自身的 {@link ListIterator#remove() remove} 或 {@link ListIterator#add(Object) add} 方法之外的任何其他方式),
 * 则迭代器将抛出 {@link ConcurrentModificationException}.
 * 因此,面对并发的修改,迭代器很快就完全失败,而不是冒着在将来不确定的时间任意发生不确定行为的风险.
 * {@link #elements() elements} 方法返回的 {@link Enumeration Enumerations} <em>不是</em> 快速失败的.
 *
 * <p>需要注意的是,迭代器的快速失败行为不能得到保证,一般来说,存在不同步的并发修改时,不可能作出任何坚决的保证.
 * 快速失败迭代器尽最大努力抛出 {@code ConcurrentModificationException}.
 * 因此,编写依赖于此异常的程序的方式是错误的,正确做法是: <i>迭代器的快速失败行为应该仅用于检测 bug.</i>
 *
 * <p>从 Java 2 平台 v1.2 开始,此类改进为可以实现 {@link List} 接口,
 * 使它成为 <a href="{@docRoot}/../technotes/guides/collections/index.html">Java Collections Framework</a> 的成员.
 * 与新 collection 实现不同,{@code Vector} 是同步的.
 * 如果不需要线程安全的实现,那么建议使用{@link ArrayList} 来替代 {@code Vector}.
 *
 * 译者注:Vector的多个方法的注释上,原文都写的是list,个人感觉应该为vector更合适,但是在翻译时,依旧是翻译做了列表.
 *
 * @author  Lee Boynton
 * @author  Jonathan Payne
 * @see Collection
 * @see LinkedList
 * @since   JDK1.0
 */
public class Vector<E>
    extends AbstractList<E>
    implements List<E>, RandomAccess, Cloneable, java.io.Serializable
{
    /**
     * 存储向量组件的数组缓冲区.vector 的容量就是此数据缓冲区的长度,该长度至少要足以包含向量的所有元素.
     *
     * <p>Vector 中的最后一个元素后的任何数组元素都为 null.
     *
     * @serial
     */
    protected Object[] elementData;

    /**
     * {@code Vector} 对象中的有效组件数.从 {@code elementData[0]} 到 {@code elementData[elementCount-1]} 的组件均为实际项.
     *
     * @serial
     */
    protected int elementCount;

    /**
     * 向量的大小大于其容量时,容量自动增加的量.如果容量的增量小于等于零,则每次需要增大容量时,向量的容量将增大一倍.
     *
     * @serial
     */
    protected int capacityIncrement;

    /** use serialVersionUID from JDK 1.0.2 for interoperability */
    private static final long serialVersionUID = -2767605614048989439L;

    /**
     * 使用指定的初始容量和容量增量构造一个空的向量.
     *
     * @param   initialCapacity     向量的初始容量
     * @param   capacityIncrement   当向量溢出时容量增加的量
     * @throws IllegalArgumentException 如果指定的初始容量为负数
     */
    public Vector(int initialCapacity, int capacityIncrement) {
        super();
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal Capacity: "+
                                               initialCapacity);
        this.elementData = new Object[initialCapacity];
        this.capacityIncrement = capacityIncrement;
    }

    /**
     * 使用指定的初始容量和等于0的容量增量构造一个空向量.
     *
     * @param   initialCapacity   向量的初始容量
     * @throws IllegalArgumentException 如果指定的初始容量为负数
     */
    public Vector(int initialCapacity) {
        this(initialCapacity, 0);
    }

    /**
     * 构造一个空的向量,使其内部数据数组的大小为 {@code 10},其标准容量增量为0.
     */
    public Vector() {
        this(10);
    }

    /**
     * 构造一个包含指定 collection 中的元素的向量,这些元素按其 collection 的迭代器返回元素的顺序排列.
     *
     * @param c 其元素要放入此向量中的 collection
     * @throws NullPointerException 如果指定的 collection 为 null
     * @since   1.2
     */
    public Vector(Collection<? extends E> c) {
        elementData = c.toArray();
        elementCount = elementData.length;
        // c.toArray might (incorrectly) not return Object[] (see 6260652)
        if (elementData.getClass() != Object[].class)
            elementData = Arrays.copyOf(elementData, elementCount, Object[].class);
    }

    /**
     * 将此向量的组件复制到指定的数组中.此向量中索引 {@code k} 处的项将复制到 {@code anArray} 的组件 {@code k} 中.
     *
     * @param  anArray 要将组件复制到其中的数组
     * @throws NullPointerException 如果给定的数组为 null
     * @throws IndexOutOfBoundsException 如果指定数组不够大,不能够保存此向量中的所有组件
     * @throws ArrayStoreException 如果此向量的组件不属于可在指定数组中存储的运行时类型
     * @see #toArray(Object[])
     */
    public synchronized void copyInto(Object[] anArray) {
        System.arraycopy(elementData, 0, anArray, 0, elementCount);
    }

    /**
     * 对此向量的容量进行微调,使其等于向量的当前大小.
     * 如果此向量的容量大于其当前大小,则通过将其内部数据数组(保存在字段 {@code elementData} 中)替换为一个较小的数组,从而将容量更改为等于当前大小.
     * 应用程序可以使用此操作最小化向量的存储.
     */
    public synchronized void trimToSize() {
        modCount++;
        int oldCapacity = elementData.length;
        if (elementCount < oldCapacity) {
            elementData = Arrays.copyOf(elementData, elementCount);
        }
    }

    /**
     * 增加此向量的容量(如有必要),以确保其至少能够保存最小容量参数指定的组件数.
     *
     * <p>如果此向量的当前容量小于 {@code minCapacity},则通过将其内部数据数组(保存在字段 {@code elementData} 中)替换为一个较大的数组来增加其容量.
     * 新数据数组的大小将为原来的大小加上 {@code capacityIncrement},除非 {@code capacityIncrement} 的值小于等于零,
     * 在后一种情况下,新的容量将为原来容量的两倍;不过,如果此大小仍然小于 {@code minCapacity},则新容量将为 {@code minCapacity}.
     *
     * @param minCapacity 需要的最小容量
     */
    public synchronized void ensureCapacity(int minCapacity) {
        if (minCapacity > 0) {
            modCount++;
            ensureCapacityHelper(minCapacity);
        }
    }

    /**
     * This implements the unsynchronized semantics of ensureCapacity.
     * Synchronized methods in this class can internally call this
     * method for ensuring capacity without incurring the cost of an
     * extra synchronization.
     *
     * @see #ensureCapacity(int)
     */
    private void ensureCapacityHelper(int minCapacity) {
        // overflow-conscious code
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

    /**
     * The maximum size of array to allocate.
     * Some VMs reserve some header words in an array.
     * Attempts to allocate larger arrays may result in
     * OutOfMemoryError: Requested array size exceeds VM limit
     */
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    private void grow(int minCapacity) {
        // overflow-conscious code
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + ((capacityIncrement > 0) ?
                                         capacityIncrement : oldCapacity);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0) // overflow
            throw new OutOfMemoryError();
        return (minCapacity > MAX_ARRAY_SIZE) ?
            Integer.MAX_VALUE :
            MAX_ARRAY_SIZE;
    }

    /**
     * 设置此向量的大小.
     * 如果新大小大于当前大小,则会在向量的末尾添加相应数量的 {@code null} 项.
     * 如果新大小小于当前大小,则丢弃索引 {@code newSize} 处及其之后的所有项.
     *
     * @param  newSize   此向量的新大小
     * @throws ArrayIndexOutOfBoundsException 如果新大小为负数
     */
    public synchronized void setSize(int newSize) {
        modCount++;
        if (newSize > elementCount) {
            ensureCapacityHelper(newSize);
        } else {
            for (int i = newSize ; i < elementCount ; i++) {
                elementData[i] = null;
            }
        }
        elementCount = newSize;
    }

    /**
     * 返回此向量的当前容量.
     *
     * @return  当前容量 (内部数据数组的长度,保存在此向量的 {@code elementData} 字段中)
     */
    public synchronized int capacity() {
        return elementData.length;
    }

    /**
     * 返回此向量中的组件数.
     *
     * @return  此向量中的组件数
     */
    public synchronized int size() {
        return elementCount;
    }

    /**
     * 测试此向量是否不包含组件.
     *
     * @return  {@code true} 当且仅当此向量没有组件,也就是说其大小为0时,返回 {@code true};否则返回 {@code false}.
     */
    public synchronized boolean isEmpty() {
        return elementCount == 0;
    }

    /**
     * 返回此向量的组件的枚举.
     * 返回的 {@code Enumeration} 对象将生成此向量中的所有项.
     * 生成的第一项为索引 {@code 0} 处的项,然后是索引 {@code 1} 处的项,依此类推.
     *
     * @return  此向量的组件的枚举
     * @see     Iterator
     */
    public Enumeration<E> elements() {
        return new Enumeration<E>() {
            int count = 0;

            public boolean hasMoreElements() {
                return count < elementCount;
            }

            public E nextElement() {
                synchronized (Vector.this) {
                    if (count < elementCount) {
                        return elementData(count++);
                    }
                }
                throw new NoSuchElementException("Vector Enumeration");
            }
        };
    }

    /**
     * 如果此向量包含指定的元素,则返回 {@code true}.
     * 更确切地讲,当且仅当此向量至少包含一个满足 <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt> 的元素 e 时,返回 {@code true}.
     *
     * @param o 测试在此向量中是否存在的元素
     * @return  如果此向量包含指定的元素,则返回 {@code true}
     */
    public boolean contains(Object o) {
        return indexOf(o, 0) >= 0;
    }

    /**
     * 返回此向量中第一次出现的指定元素的索引,如果此向量不包含该元素,则返回 -1.
     * 更确切地讲,返回满足 <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt> 的最低索引 {@code i};
     * 如果没有这样的索引,则返回 -1.
     *
     * @param o 要搜索的元素
     * @return 此向量中第一次出现的指定元素的索引;如果此向量不包含该元素,则返回 -1
     */
    public int indexOf(Object o) {
        return indexOf(o, 0);
    }

    /**
     * 返回此向量中第一次出现的指定元素的索引,从 {@code index} 处正向搜索,如果未找到该元素,则返回 -1.
     * 更确切地讲,返回满足 <tt>(i&nbsp;&gt;=&nbsp;index&nbsp;&amp;&amp;&nbsp;(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i))))</tt> 的最低索引 {@code i};
     * 如果没有这样的索引,则返回 -1.
     *
     * @param o 要搜索的元素
     * @param index 搜索开始处的索引
     * @return 此向量中 {@code index} 位置或之后位置处第一次出现的指定元素的索引;如果未找到该元素,则返回 {@code -1}.
     * @throws IndexOutOfBoundsException 如果指定索引为负数
     * @see     Object#equals(Object)
     */
    public synchronized int indexOf(Object o, int index) {
        if (o == null) {
            for (int i = index ; i < elementCount ; i++)
                if (elementData[i]==null)
                    return i;
        } else {
            for (int i = index ; i < elementCount ; i++)
                if (o.equals(elementData[i]))
                    return i;
        }
        return -1;
    }

    /**
     * 中最后一次出现的指定元素的索引,如果此向量不包含该元素,则返回 -1.
     * 更确切地讲,返回满足 <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt> 的最高索引 {@code i};
     * 如果没有这样的索引,则返回 -1.
     *
     * @param o 要搜索的元素
     * @return 此向量中最后一次出现的指定元素的索引,如果此向量不包含该元素,则返回 -1
     */
    public synchronized int lastIndexOf(Object o) {
        return lastIndexOf(o, elementCount-1);
    }

    /**
     * 返回此向量中最后一次出现的指定元素的索引,从 {@code index} 处逆向搜索,如果未找到该元素,则返回 -1.
     * 更确切地讲,返回满足 <tt>(i&nbsp;&lt;=&nbsp;index&nbsp;&amp;&amp;&nbsp;(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i))))</tt> 的最高索引 {@code i};
     * 如果没有这样的索引,则返回 -1.
     *
     * @param o 要搜索的元素
     * @param index 逆向搜索开始处的索引
     * @return 此向量中小于等于 {@code index} 位置处最后一次出现的指定元素的索引;如果未找到该元素,则返回 -1.
     * @throws IndexOutOfBoundsException 如果指定索引大于等于此向量的当前大小
     */
    public synchronized int lastIndexOf(Object o, int index) {
        if (index >= elementCount)
            throw new IndexOutOfBoundsException(index + " >= "+ elementCount);

        if (o == null) {
            for (int i = index; i >= 0; i--)
                if (elementData[i]==null)
                    return i;
        } else {
            for (int i = index; i >= 0; i--)
                if (o.equals(elementData[i]))
                    return i;
        }
        return -1;
    }

    /**
     * 返回指定索引处的组件.
     *
     * <p>此方法的功能与 {@link #get(int)} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     *
     * @param      index   此向量的一个索引
     * @return     指定索引处的组件
     * @throws ArrayIndexOutOfBoundsException 如果该索引超出范围 ({@code index < 0 || index >= size()})
     *
     */
    public synchronized E elementAt(int index) {
        if (index >= elementCount) {
            throw new ArrayIndexOutOfBoundsException(index + " >= " + elementCount);
        }

        return elementData(index);
    }

    /**
     * 返回此向量的第一个组件 (位于索引 {@code 0} 处的项).
     *
     * @return     此向量的第一个组件
     * @throws NoSuchElementException  如果此向量没有组件
     */
    public synchronized E firstElement() {
        if (elementCount == 0) {
            throw new NoSuchElementException();
        }
        return elementData(0);
    }

    /**
     * 返回此向量的最后一个组件.
     *
     * @return  向量的最后一个组件,即索引 <code>size()&nbsp;-&nbsp;1</code> 处的组件.
     * @throws NoSuchElementException 如果此向量为空
     */
    public synchronized E lastElement() {
        if (elementCount == 0) {
            throw new NoSuchElementException();
        }
        return elementData(elementCount - 1);
    }

    /**
     * 将此向量指定 {@code index} 处的组件设置为指定的对象.丢弃该位置以前的组件.
     *
     * <p>索引必须为一个大于等于 {@code 0} 且小于向量当前大小的值.
     *
     * <p>此方法的功能与 {@link #set(int, Object) set(int, E)} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     * 注意,{@code set} 方法将反转参数的顺序,与数组用法更为匹配.
     * 另外还要注意,{@code set} 方法将返回以前存储在指定位置的旧值.
     *
     * @param      obj     将用来设置组件的内容
     * @param      index   指定的索引
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index >= size()})
     *
     */
    public synchronized void setElementAt(E obj, int index) {
        if (index >= elementCount) {
            throw new ArrayIndexOutOfBoundsException(index + " >= " +
                                                     elementCount);
        }
        elementData[index] = obj;
    }

    /**
     * 删除指定索引处的组件.
     * 此向量中的每个索引大于等于指定 {@code index} 的组件都将下移,使其索引值变成比以前小 1 的值.
     * 此向量的大小将减 {@code 1}.
     *
     * <p>T索引必须为一个大于等于 {@code 0} 且小于向量当前大小的值.
     *
     * <p>此方法的功能与 {@link #remove(int)} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     * 注意,{@code remove} 方法将返回以前存储在指定位置的旧值.
     *
     * @param      index   要移除对象的索引
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index >= size()})
     */
    public synchronized void removeElementAt(int index) {
        modCount++;
        if (index >= elementCount) {
            throw new ArrayIndexOutOfBoundsException(index + " >= " +
                                                     elementCount);
        }
        else if (index < 0) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        int j = elementCount - index - 1;
        if (j > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, j);
        }
        elementCount--;
        elementData[elementCount] = null; /* to let gc do its work */
    }

    /**
     * 将指定对象作为此向量中的组件插入到指定的 {@code index} 处.
     * 此向量中的每个索引大于等于指定 {@code index} 的组件都将向上移位,使其索引值变成比以前大 1 的值.
     *
     * <p>索引必须为一个大于等于 {@code 0} 且小于等于向量当前大小的值.
     * (如果索引等于向量的当前大小,则将新元素添加到向量.)
     *
     * <p>此方法的功能与 {@link #add(int, Object) add(int, E)} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     * 注意,{@code add} 方法将反转参数的顺序,与数组用法更为匹配.
     *
     * @param      obj     要插入的组件
     * @param      index   新组件的插入位置
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index > size()})
     *
     */
    public synchronized void insertElementAt(E obj, int index) {
        modCount++;
        if (index > elementCount) {
            throw new ArrayIndexOutOfBoundsException(index
                                                     + " > " + elementCount);
        }
        ensureCapacityHelper(elementCount + 1);
        System.arraycopy(elementData, index, elementData, index + 1, elementCount - index);
        elementData[index] = obj;
        elementCount++;
    }

    /**
     * 将指定的组件添加到此向量的末尾,将其大小增加 1.如果向量的大小比容量大,则增大其容量.
     *
     * <p>此方法的功能与 {@link #add(Object) add(E)} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     *
     * @param   obj   要添加的组件
     */
    public synchronized void addElement(E obj) {
        modCount++;
        ensureCapacityHelper(elementCount + 1);
        elementData[elementCount++] = obj;
    }

    /**
     * 从此向量中移除变量的第一个 (索引最小的) 匹配项.
     * 如果在此向量中找到该对象,那么向量中索引大于等于该对象索引的每个组件都会下移,使其索引值变成比以前小 1 的值.
     *
     * <p>此方法的功能与 {@link #remove(Object)} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     *
     * @param   obj 要移除的组件
     * @return  如果变量值是此向量的一个组件,则返回{@code true};否则返回 {@code false}.
     */
    public synchronized boolean removeElement(Object obj) {
        modCount++;
        int i = indexOf(obj);
        if (i >= 0) {
            removeElementAt(i);
            return true;
        }
        return false;
    }

    /**
     * 从此向量中移除全部组件,并将其大小设置为零.
     *
     * <p>此方法的功能与 {@link #clear} 方法的功能完全相同(后者是 {@link List} 接口的一部分).
     */
    public synchronized void removeAllElements() {
        modCount++;
        // Let gc do its work
        for (int i = 0; i < elementCount; i++)
            elementData[i] = null;

        elementCount = 0;
    }

    /**
     * 返回向量的一个副本.副本中将包含一个对内部数据数组副本的引用,而非对此 {@code Vector} 对象的原始内部数据数组的引用.
     *
     * @return  此向量的一个副本
     */
    public synchronized Object clone() {
        try {
            @SuppressWarnings("unchecked")
                Vector<E> v = (Vector<E>) super.clone();
            v.elementData = Arrays.copyOf(elementData, elementCount);
            v.modCount = 0;
            return v;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError(e);
        }
    }

    /**
     * 返回一个数组,包含此向量中以恰当顺序存放的所有元素.
     *
     * @since 1.2
     */
    public synchronized Object[] toArray() {
        return Arrays.copyOf(elementData, elementCount);
    }

    /**
     * 返回一个数组,包含此向量中以恰当顺序存放的所有元素;返回数组的运行时类型为指定数组的类型.
     * 如果向量能够适应指定的数组,则返回该数组.否则使用此数组的运行时类型和此向量的大小分配一个新数组.
     *
     * <p>如果向量能够适应指定的数组,而且还有多余空间(即数组的元素比向量的元素多),则将紧跟向量末尾的数组元素设置为 null.
     * (<em>仅</em> 在调用者知道向量不包含任何 null 元素的情况下,这对确定向量的长度才有用.)
     *
     * @param a 要在其中存储向量元素的数组(如果该数组足够大);否则,将为此分配一个具有相同运行时类型的新数组.
     * @return 包含向量元素的数组
     * @throws ArrayStoreException 如果 a 的运行时类型不是此向量中每个元素的运行时类型的超类型
     * @throws NullPointerException 如果给定的数组为 null
     * @since 1.2
     */
    @SuppressWarnings("unchecked")
    public synchronized <T> T[] toArray(T[] a) {
        if (a.length < elementCount)
            return (T[]) Arrays.copyOf(elementData, elementCount, a.getClass());

        System.arraycopy(elementData, 0, a, 0, elementCount);

        if (a.length > elementCount)
            a[elementCount] = null;

        return a;
    }

    // Positional Access Operations

    @SuppressWarnings("unchecked")
    E elementData(int index) {
        return (E) elementData[index];
    }

    /**
     * 返回向量中指定位置的元素.
     *
     * @param index 要返回元素的索引
     * @return 指定索引处的对象
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index >= size()})
     *
     * @since 1.2
     */
    public synchronized E get(int index) {
        if (index >= elementCount)
            throw new ArrayIndexOutOfBoundsException(index);

        return elementData(index);
    }

    /**
     * 用指定的元素替换此向量中指定位置处的元素.
     *
     * @param index 要替换元素的索引
     * @param element 要存储在指定位置的元素
     * @return 以前位于指定位置处的元素
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index >= size()})
     * @since 1.2
     */
    public synchronized E set(int index, E element) {
        if (index >= elementCount)
            throw new ArrayIndexOutOfBoundsException(index);

        E oldValue = elementData(index);
        elementData[index] = element;
        return oldValue;
    }

    /**
     * 将指定元素添加到此向量的末尾.
     *
     * @param e 要添加到此向量的元素
     * @return {@code true} (根据 {@link Collection#add} 的规定)
     * @since 1.2
     */
    public synchronized boolean add(E e) {
        modCount++;
        ensureCapacityHelper(elementCount + 1);
        elementData[elementCount++] = e;
        return true;
    }

    /**
     * 移除此向量中指定元素的第一个匹配项,如果向量不包含该元素,则元素保持不变.
     * 更确切地讲,移除其索引 i 满足 {@code (o==null ? get(i)==null : o.equals(get(i)))} 的元素(如果存在这样的元素).
     *
     * @param o 要从向量中移除的元素,如果存在
     * @return 如果向量包含指定元素,则返回 true
     * @since 1.2
     */
    public boolean remove(Object o) {
        return removeElement(o);
    }

    /**
     * 在此向量的指定位置插入指定的元素.将当前位于该位置的元素(如果有)及所有后续元素右移(将其索引加 1).
     *
     * @param index 要在其位置插入指定元素的索引
     * @param element 要插入的元素
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index > size()})
     * @since 1.2
     */
    public void add(int index, E element) {
        insertElementAt(element, index);
    }

    /**
     * 移除此向量中指定位置的元素.将所有后续元素左移(将其索引减 1).返回此向量中移除的元素.
     *
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index >= size()})
     *
     * @param index 要移除元素的索引
     * @return 移除的元素
     * @since 1.2
     */
    public synchronized E remove(int index) {
        modCount++;
        if (index >= elementCount)
            throw new ArrayIndexOutOfBoundsException(index);
        E oldValue = elementData(index);

        int numMoved = elementCount - index - 1;
        if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index,
                             numMoved);
        elementData[--elementCount] = null; // Let gc do its work

        return oldValue;
    }

    /**
     * 从此向量中移除所有元素.此调用返回后,向量将为空(除非抛出了异常).
     *
     * @since 1.2
     */
    public void clear() {
        removeAllElements();
    }

    // Bulk Operations

    /**
     * 如果此向量包含指定 Collection 中的所有元素,则返回 true.
     *
     * @param   c  要在此向量中测试是否包含其元素的 collection
     * @return 如果此向量包含指定 collection 中的所有元素,则返回 true
     * @throws NullPointerException 如果指定 collection 为 null
     */
    public synchronized boolean containsAll(Collection<?> c) {
        return super.containsAll(c);
    }

    /**
     * 将指定 Collection 中的所有元素添加到此向量的末尾,按照指定 collection 的迭代器所返回的顺序添加这些元素.
     * 如果指定的 Collection 在操作过程中被修改,则此操作的行为是不确定的
     * (这意味着如果指定的 Collection 是此向量,而此向量不为空,则此调用的行为是不确定的)、
     *
     * @param c 要插入到此向量的元素
     * @return 如果此向量由于调用而更改,则返回 {@code true}
     * @throws NullPointerException 如果指定 collection 为 null
     * @since 1.2
     */
    public synchronized boolean addAll(Collection<? extends E> c) {
        modCount++;
        Object[] a = c.toArray();
        int numNew = a.length;
        ensureCapacityHelper(elementCount + numNew);
        System.arraycopy(a, 0, elementData, elementCount, numNew);
        elementCount += numNew;
        return numNew != 0;
    }

    /**
     * 从此向量中移除包含在指定 Collection 中的所有元素.
     *
     * @param c 要从向量中移除的元素的 collection
     * @return 如果此向量由于调用而更改,则返回true
     * @throws ClassCastException 如果此向量中的一个或多个元素的类型与指定 collection 不兼容(<a href="Collection.html#optional-restrictions">可选</a>)
     * @throws NullPointerException 如果此向量包含一个或多个 null 元素并且指定 collection 不支持 null 元素 (<a href="Collection.html#optional-restrictions">可选</a>),
     *         或者如果指定 collection 为 null
     * @since 1.2
     */
    public synchronized boolean removeAll(Collection<?> c) {
        return super.removeAll(c);
    }

    /**
     * 在此向量中仅保留包含在指定 Collection 中的元素.
     * 换句话说,从此向量中移除所有未包含在指定 Collection 中的元素.
     *
     * @param c 要在此向量中保留的元素的 collection (移除其他所有元素)
     * @return 如果此向量由于调用而更改,则返回true
     * @throws ClassCastException 如果此向量中的一个或多个元素的类型与指定 collection 不兼容 (<a href="Collection.html#optional-restrictions">可选</a>)
     * @throws NullPointerException 如果此向量包含一个或多个 null 元素并且指定 collection 不支持 null 元素 (<a href="Collection.html#optional-restrictions">可选</a>),
     *        或者如果指定 collection 为 null
     * @since 1.2
     */
    public synchronized boolean retainAll(Collection<?> c) {
        return super.retainAll(c);
    }

    /**
     * 在指定位置将指定 Collection 中的所有元素插入到此向量中.
     * 将当前位于该位置的元素(如果有)及所有后续元素右移(增大其索引值).
     * 新元素在向量中按照其由指定 Collection 的迭代器所返回的顺序出现.
     *
     * @param index 要插入指定 collection 的第一个元素的索引
     * @param c  要插入到此向量的元素
     * @return 如果此向量由于调用而更改,则返回 {@code true}
     * @throws ArrayIndexOutOfBoundsException 如果索引超出范围 ({@code index < 0 || index > size()})
     * @throws NullPointerException 如果指定 collection 为 null
     * @since 1.2
     */
    public synchronized boolean addAll(int index, Collection<? extends E> c) {
        modCount++;
        if (index < 0 || index > elementCount)
            throw new ArrayIndexOutOfBoundsException(index);

        Object[] a = c.toArray();
        int numNew = a.length;
        ensureCapacityHelper(elementCount + numNew);

        int numMoved = elementCount - index;
        if (numMoved > 0)
            System.arraycopy(elementData, index, elementData, index + numNew,
                             numMoved);

        System.arraycopy(a, 0, elementData, index, numNew);
        elementCount += numNew;
        return numNew != 0;
    }

    /**
     * 将指定的对象跟此向量进行相等性比较.
     * 当且仅当指定的对象也是一个列表,两个列表有相同的大小,并且两个列表中的所有相应的元素对<em>相等</em>时才返回 <tt>true</tt>.
     * (如果  {@code (e1==null ? e2==null : e1.equals(e2))},那么两个元素 {@code e1} 和 {@code e2} 是<em>相等</em>的.)
     * 换句话说,如果所定义的两个列表以相同的顺序包含相同的元素,那么它们是相等的.
     *
     * @param o 要与此向量进行相等性比较的对象
     * @return 如果指定的 Object 与此向量相等,则返回 true
     */
    public synchronized boolean equals(Object o) {
        return super.equals(o);
    }

    /**
     * 返回此向量的哈希码值.
     */
    public synchronized int hashCode() {
        return super.hashCode();
    }

    /**
     * 返回此向量的字符串表示形式,其中包含每个元素的 String 表示形式.
     */
    public synchronized String toString() {
        return super.toString();
    }

    /**
     * 返回列表中指定的 <tt>fromIndex</tt>(包括 )和 <tt>toIndex</tt>(不包括)之间的部分视图.
     * (如果 <tt>fromIndex</tt> 和 <tt>toIndex</tt> 相等,那么返回的列表为空.)
     * 返回的列表由此列表支持,因此返回列表中的非结构性更改将反映在此列表中,反之亦然.
     * 返回的列表支持此列表支持的所有可选列表操作.
     *
     * <p>此方法省去了显式范围操作(通常是针对数组的操作).
     * 通过传递 subList 视图而非整个列表,可使列表的任何操作可成为范围操作.
     * 例如,下面的语句从列表中移除了指定范围内的元素:
     * <pre>
     *      list.subList(from, to).clear();
     * </pre>
     * 可以对 <tt>indexOf</tt> 和 <tt>lastIndexOf</tt> 构造类似的语句,
     * 而且 <tt>Collections</tt> 类中的所有算法都可以应用于 subList.
     *
     * <p>如果支持列表(即此列表)通过任何其他方式(而不是通过返回的列表)<i>从结构上修改</i>,
     * 则此方法返回的列表语义将变为未定义(从结构上修改是指更改列表的大小,或者以其他方式打乱列表,使正在进行的迭代产生错误的结果).
     *
     * @param fromIndex subList 的低端 (包括)
     * @param toIndex subList 的高端 (不包括)
     * @return 列表中指定范围的视图
     * @throws IndexOutOfBoundsException 非法的端点值{@code (fromIndex < 0 || toIndex > size)}
     * @throws IllegalArgumentException 如果端点索引顺序错误 {@code (fromIndex > toIndex)}
     *
     */
    public synchronized List<E> subList(int fromIndex, int toIndex) {
        return Collections.synchronizedList(super.subList(fromIndex, toIndex),
                                            this);
    }

    /**
     * 从此 List 中移除其索引位于 {@code fromIndex}（包括）与 {@code toIndex}(不包括)之间的所有元素.
     * 将所有后续元素左移(减小其索引值).
     * 此调用会将列表缩小 {@code (toIndex - fromIndex)} 个元素(如果 {@code toIndex==fromIndex},则此操作没有任何效果.)
     */
    protected synchronized void removeRange(int fromIndex, int toIndex) {
        modCount++;
        int numMoved = elementCount - toIndex;
        System.arraycopy(elementData, toIndex, elementData, fromIndex,
                         numMoved);

        // Let gc do its work
        int newElementCount = elementCount - (toIndex-fromIndex);
        while (elementCount != newElementCount)
            elementData[--elementCount] = null;
    }

    /**
     * Save the state of the {@code Vector} instance to a stream (that
     * is, serialize it).
     * This method performs synchronization to ensure the consistency
     * of the serialized data.
     */
    private void writeObject(java.io.ObjectOutputStream s)
            throws java.io.IOException {
        final java.io.ObjectOutputStream.PutField fields = s.putFields();
        final Object[] data;
        synchronized (this) {
            fields.put("capacityIncrement", capacityIncrement);
            fields.put("elementCount", elementCount);
            data = elementData.clone();
        }
        fields.put("elementData", data);
        s.writeFields();
    }

    /**
     * 返回此列表元素的列表迭代器 (按适当顺序), 从列表的指定位置开始.
     * 指定的索引表示 {@link ListIterator#next next} 的初始调用所返回的第一个元素.
     * {@link ListIterator#previous previous} 方法的初始调用将返回索引比指定索引少 1 的元素.
     *
     * <p>返回的列表迭代器是 <a href="#fail-fast"><i>快速失败</i></a>的.
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public synchronized ListIterator<E> listIterator(int index) {
        if (index < 0 || index > elementCount)
            throw new IndexOutOfBoundsException("Index: "+index);
        return new ListItr(index);
    }

    /**
     * 返回此列表元素的列表迭代器 (按适当顺序).
     *
     * <p>返回的列表迭代器是 <a href="#fail-fast"><i>快速失败</i></a>的.
     *
     * @see #listIterator(int)
     */
    public synchronized ListIterator<E> listIterator() {
        return new ListItr(0);
    }

    /**
     * 返回按适当顺序在列表的元素上进行迭代的迭代器.
     *
     * <p>返回的迭代器是 <a href="#fail-fast"><i>快速失败</i></a>的.
     *
     * @return 个按适当顺序在列表的元素上进行迭代的迭代器
     */
    public synchronized Iterator<E> iterator() {
        return new Itr();
    }

    /**
     * An optimized version of AbstractList.Itr
     */
    private class Itr implements Iterator<E> {
        int cursor;       // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such
        int expectedModCount = modCount;

        public boolean hasNext() {
            // Racy but within spec, since modifications are checked
            // within or after synchronization in next/previous
            return cursor != elementCount;
        }

        public E next() {
            synchronized (Vector.this) {
                checkForComodification();
                int i = cursor;
                if (i >= elementCount)
                    throw new NoSuchElementException();
                cursor = i + 1;
                return elementData(lastRet = i);
            }
        }

        public void remove() {
            if (lastRet == -1)
                throw new IllegalStateException();
            synchronized (Vector.this) {
                checkForComodification();
                Vector.this.remove(lastRet);
                expectedModCount = modCount;
            }
            cursor = lastRet;
            lastRet = -1;
        }

        @Override
        public void forEachRemaining(Consumer<? super E> action) {
            Objects.requireNonNull(action);
            synchronized (Vector.this) {
                final int size = elementCount;
                int i = cursor;
                if (i >= size) {
                    return;
                }
        @SuppressWarnings("unchecked")
                final E[] elementData = (E[]) Vector.this.elementData;
                if (i >= elementData.length) {
                    throw new ConcurrentModificationException();
                }
                while (i != size && modCount == expectedModCount) {
                    action.accept(elementData[i++]);
                }
                // update once at end of iteration to reduce heap write traffic
                cursor = i;
                lastRet = i - 1;
                checkForComodification();
            }
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    /**
     * An optimized version of AbstractList.ListItr
     */
    final class ListItr extends Itr implements ListIterator<E> {
        ListItr(int index) {
            super();
            cursor = index;
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public E previous() {
            synchronized (Vector.this) {
                checkForComodification();
                int i = cursor - 1;
                if (i < 0)
                    throw new NoSuchElementException();
                cursor = i;
                return elementData(lastRet = i);
            }
        }

        public void set(E e) {
            if (lastRet == -1)
                throw new IllegalStateException();
            synchronized (Vector.this) {
                checkForComodification();
                Vector.this.set(lastRet, e);
            }
        }

        public void add(E e) {
            int i = cursor;
            synchronized (Vector.this) {
                checkForComodification();
                Vector.this.add(i, e);
                expectedModCount = modCount;
            }
            cursor = i + 1;
            lastRet = -1;
        }
    }

    @Override
    public synchronized void forEach(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        final int expectedModCount = modCount;
        @SuppressWarnings("unchecked")
        final E[] elementData = (E[]) this.elementData;
        final int elementCount = this.elementCount;
        for (int i=0; modCount == expectedModCount && i < elementCount; i++) {
            action.accept(elementData[i]);
        }
        if (modCount != expectedModCount) {
            throw new ConcurrentModificationException();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public synchronized boolean removeIf(Predicate<? super E> filter) {
        Objects.requireNonNull(filter);
        // figure out which elements are to be removed
        // any exception thrown from the filter predicate at this stage
        // will leave the collection unmodified
        int removeCount = 0;
        final int size = elementCount;
        final BitSet removeSet = new BitSet(size);
        final int expectedModCount = modCount;
        for (int i=0; modCount == expectedModCount && i < size; i++) {
            @SuppressWarnings("unchecked")
            final E element = (E) elementData[i];
            if (filter.test(element)) {
                removeSet.set(i);
                removeCount++;
            }
        }
        if (modCount != expectedModCount) {
            throw new ConcurrentModificationException();
        }

        // shift surviving elements left over the spaces left by removed elements
        final boolean anyToRemove = removeCount > 0;
        if (anyToRemove) {
            final int newSize = size - removeCount;
            for (int i=0, j=0; (i < size) && (j < newSize); i++, j++) {
                i = removeSet.nextClearBit(i);
                elementData[j] = elementData[i];
            }
            for (int k=newSize; k < size; k++) {
                elementData[k] = null;  // Let gc do its work
            }
            elementCount = newSize;
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
            modCount++;
        }

        return anyToRemove;
    }

    @Override
    @SuppressWarnings("unchecked")
    public synchronized void replaceAll(UnaryOperator<E> operator) {
        Objects.requireNonNull(operator);
        final int expectedModCount = modCount;
        final int size = elementCount;
        for (int i=0; modCount == expectedModCount && i < size; i++) {
            elementData[i] = operator.apply((E) elementData[i]);
        }
        if (modCount != expectedModCount) {
            throw new ConcurrentModificationException();
        }
        modCount++;
    }

    @SuppressWarnings("unchecked")
    @Override
    public synchronized void sort(Comparator<? super E> c) {
        final int expectedModCount = modCount;
        Arrays.sort((E[]) elementData, 0, elementCount, c);
        if (modCount != expectedModCount) {
            throw new ConcurrentModificationException();
        }
        modCount++;
    }

    /**
     * Creates a <em><a href="Spliterator.html#binding">late-binding</a></em>
     * and <em>fail-fast</em> {@link Spliterator} over the elements in this
     * list.
     *
     * <p>The {@code Spliterator} reports {@link Spliterator#SIZED},
     * {@link Spliterator#SUBSIZED}, and {@link Spliterator#ORDERED}.
     * Overriding implementations should document the reporting of additional
     * characteristic values.
     *
     * @return a {@code Spliterator} over the elements in this list
     * @since 1.8
     */
    @Override
    public Spliterator<E> spliterator() {
        return new VectorSpliterator<>(this, null, 0, -1, 0);
    }

    /** Similar to ArrayList Spliterator */
    static final class VectorSpliterator<E> implements Spliterator<E> {
        private final Vector<E> list;
        private Object[] array;
        private int index; // current index, modified on advance/split
        private int fence; // -1 until used; then one past last index
        private int expectedModCount; // initialized when fence set

        /** Create new spliterator covering the given  range */
        VectorSpliterator(Vector<E> list, Object[] array, int origin, int fence,
                          int expectedModCount) {
            this.list = list;
            this.array = array;
            this.index = origin;
            this.fence = fence;
            this.expectedModCount = expectedModCount;
        }

        private int getFence() { // initialize on first use
            int hi;
            if ((hi = fence) < 0) {
                synchronized(list) {
                    array = list.elementData;
                    expectedModCount = list.modCount;
                    hi = fence = list.elementCount;
                }
            }
            return hi;
        }

        public Spliterator<E> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid) ? null :
                new VectorSpliterator<E>(list, array, lo, index = mid,
                                         expectedModCount);
        }

        @SuppressWarnings("unchecked")
        public boolean tryAdvance(Consumer<? super E> action) {
            int i;
            if (action == null)
                throw new NullPointerException();
            if (getFence() > (i = index)) {
                index = i + 1;
                action.accept((E)array[i]);
                if (list.modCount != expectedModCount)
                    throw new ConcurrentModificationException();
                return true;
            }
            return false;
        }

        @SuppressWarnings("unchecked")
        public void forEachRemaining(Consumer<? super E> action) {
            int i, hi; // hoist accesses and checks from loop
            Vector<E> lst; Object[] a;
            if (action == null)
                throw new NullPointerException();
            if ((lst = list) != null) {
                if ((hi = fence) < 0) {
                    synchronized(lst) {
                        expectedModCount = lst.modCount;
                        a = array = lst.elementData;
                        hi = fence = lst.elementCount;
                    }
                }
                else
                    a = array;
                if (a != null && (i = index) >= 0 && (index = hi) <= a.length) {
                    while (i < hi)
                        action.accept((E) a[i++]);
                    if (lst.modCount == expectedModCount)
                        return;
                }
            }
            throw new ConcurrentModificationException();
        }

        public long estimateSize() {
            return (long) (getFence() - index);
        }

        public int characteristics() {
            return Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SUBSIZED;
        }
    }
}
