/*
 * Copyright (c) 1997, 2006, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

/**
 * 此类提供了 <tt>List</tt> 接口的骨干实现,从而最大限度地减少了实现受"连续访问"数据存储(如链接列表)支持的此接口所需的工作.
 * 对于随机访问数据(如数组),应该优先使用 <tt>AbstractList</tt>,而不是先使用此类.
 *
 * <p>从某种意义上说,此类与在列表的列表迭代器上实现"随机访问"方法(<tt>get(int index)</tt>,
 * <tt>set(int index, E element)</tt>, <tt>add(int index, E element)</tt> 和 <tt>remove(int index)</tt>)的 <tt>AbstractList</tt> 类相对立,
 * 而不是其他关系.
 *
 * <p>要实现一个列表,程序员只需要扩展此类,并提供 <tt>listIterator</tt> 和 <tt>size</tt> 方法的实现即可.
 * 对于不可修改的列表,程序员只需要实现列表迭代器的 <tt>hasNext</tt>, <tt>next</tt>, <tt>hasPrevious</tt>,<tt>previous</tt>
 * 和 <tt>index</tt> 方法即可.
 *
 * <p>对于可修改的列表,程序员应该再另外实现列表迭代器的 <tt>set</tt> 方法.
 * 对于可变大小的列表,程序员应该再另外实现列表迭代器的 <tt>remove</tt> 和 <tt>add</tt> 方法.
 *
 * <p>按照 {@link Collection} 接口规范中的建议,编程人员通常应该提供一个 void(无参数)和 collection 构造方法.
 *
 * <p>此类是<a href="{@docRoot}/../technotes/guides/collections/index.html">Java Collections Framework</a>的成员之一.
 *
 * @author  Josh Bloch
 * @author  Neal Gafter
 * @see Collection
 * @see List
 * @see AbstractList
 * @see AbstractCollection
 * @since 1.2
 */

public abstract class AbstractSequentialList<E> extends AbstractList<E> {
    /**
     * 唯一的构造方法.  (由子类构造方法调用,通常是隐式的.)
     */
    protected AbstractSequentialList() {
    }

    /**
     * 返回此列表中指定位置上的元素.
     *
     * <p>此实现首先获得一个指向索引元素的列表迭代器(通过<tt>listIterator(index)</tt>方法).
     * 然后它使用 <tt>ListIterator.next</tt> 获得该元素并返回它.
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public E get(int index) {
        try {
            return listIterator(index).next();
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: "+index);
        }
    }

    /**
     * 用指定的元素替代此列表中指定位置上的元素 (可选操作).
     *
     * <p>此实现首先获得一个指向索引元素的列表迭代器(通过<tt>listIterator(index)</tt>方法).
     * 然后它使用 <tt>ListIterator.next</tt> 获取当前元素,并使用 <tt>ListIterator.set</tt> 替代它.
     *
     * <p>注意,如果该列表迭代器没有实现 <tt>set</tt> 操作,则此实现将抛出 <tt>UnsupportedOperationException</tt>.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public E set(int index, E element) {
        try {
            ListIterator<E> e = listIterator(index);
            E oldVal = e.next();
            e.set(element);
            return oldVal;
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: "+index);
        }
    }

    /**
     * 在此列表中的指定位置上插入指定的元素(可选操作).
     * 向右移动当前位于该位置上的元素(如果有) 以及所有后续元素 (将其索引加 1).
     *
     * <p>此实现首先获得一个指向索引元素的列表迭代器 (通过 <tt>listIterator(index)</tt>).
     * 然后它使用 <tt>ListIterator.add</tt> 插入指定的元素.
     *
     * <p>注意,如果该列表迭代器没有实现 <tt>add</tt> 操作,则此实现将抛出 <tt>UnsupportedOperationException</tt>.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public void add(int index, E element) {
        try {
            listIterator(index).add(element);
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: "+index);
        }
    }

    /**
     * 移除此列表中指定位置上的元 (可选操作).
     * 向左移动所有后续元素(将其索引减 1).返回从列表中移除的元素.
     *
     * <p>此实现首先获得一个指向索引元素的列表迭代器 (通过 <tt>listIterator(index)</tt>).
     * 然后它使用 <tt>ListIterator.remove</tt> 移除指定的元素.
     *
     * <p>注意,如果该列表迭代器没有实现 <tt>remove</tt> 操作,则此实现将抛出 <tt>UnsupportedOperationException</tt>.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public E remove(int index) {
        try {
            ListIterator<E> e = listIterator(index);
            E outCast = e.next();
            e.remove();
            return outCast;
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: "+index);
        }
    }


    // Bulk Operations

    /**
     * 在此列表中指定的位置上插入指定 collection 中的所有元素(可选操作).
     * 向右移动当前位于该位置上的元素 (如果有) 以及所有后续元素 (增加其索引).
     * 新元素将按指定 collection 的迭代器所返回的顺序出现在列表中.
     * 如果正在进行此操作时修改指定的 collection,则此操作行为是未指定的.
     * (注意,如果指定的 collection 是此列表并且是非空的,则会发生这种情况.)
     *
     * <p>此实现获得指定 collection 的迭代器,以及此列表指向索引元素的列表迭代器 (通过 <tt>listIterator(index)</tt>).
     * 然后,它在指定的 collection 上进行迭代,通过使用 <tt>ListIterator.next</tt> 之后
     * 紧接着使用 <tt>ListIterator.add</tt>.add 方法(以跳过添加的元素),把从迭代器中获得的元素逐个插入此列表中.
     *
     * <p>注意,如果 <tt>listIterator</tt> 返回的列表迭代器没有实现 <tt>add</tt> 操作,
     * 则此实现将抛出 <tt>UnsupportedOperationException</tt>.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public boolean addAll(int index, Collection<? extends E> c) {
        try {
            boolean modified = false;
            ListIterator<E> e1 = listIterator(index);
            Iterator<? extends E> e2 = c.iterator();
            while (e2.hasNext()) {
                e1.add(e2.next());
                modified = true;
            }
            return modified;
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: "+index);
        }
    }


    // Iterators

    /**
     * 返回在此列表中的元素上进行迭代的迭代器 (按适当顺序).<p>
     *
     * 此实现仅返回列表的一个列表迭代器.
     *
     * @return 在此列表中的元素上进行迭代的迭代器 (按适当顺序)
     */
    public Iterator<E> iterator() {
        return listIterator();
    }

    /**
     * 返回在此列表中的元素上进行迭代的迭代器(按适当顺序).
     *
     * @param  index 从列表迭代器返回(通过调用 <code>next</code> 方法)的第一个元素的索引
     * @return 在此列表中的元素上进行迭代的迭代器 (按适当顺序)
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public abstract ListIterator<E> listIterator(int index);
}
