/*
 * Copyright (c) 1994, 2004, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

import java.lang.*;

/**
 * string tokenizer 类允许应用程序将字符串分解为标记(token).
 * tokenization 方法比 <code>StreamTokenizer</code> 类所使用的方法更简单.
 * <code>StringTokenizer</code> 方法不区分标识符、数和带引号的字符串,它们也不识别并跳过注释.
 * <p>
 * 分隔符(分隔标记的字符)集可以在创建时指定,也可以根据每个标记来指定.
 * <p>
 * <code>StringTokenizer</code> 的实例有两种行为方式,
 * 这取决于它在创建时使用的 <code>returnDelims</code> 标志的值是 <code>true</code> 还是 <code>false</code>:
 * <ul>
 * <li>如果标志为 <code>false</code>, 则分隔符字符用来分隔标记.标记是连续字符(不是分隔符)的最大序列.
 * <li>如果标志为 <code>true</code>, 则认为那些分隔符字符本身即为标记.因此标记要么是一个分隔符字符,要么是那些连续字符(不是分隔符)的最大序列.
 * </ul><p>
 * <tt>StringTokenizer</tt> 对象在内部维护字符串中要被标记的当前位置.
 * 某些操作将此当前位置移至已处理的字符后.<p>
 *
 * 通过截取字符串的一个子串来返回标记,该字符串用于创建 <tt>StringTokenizer</tt> 对象.<p>
 *
 * 下面是一个使用 tokenizer 的实例.代码如下:
 * <blockquote><pre>
 *     StringTokenizer st = new StringTokenizer("this is a test");
 *     while (st.hasMoreTokens()) {
 *         System.out.println(st.nextToken());
 *     }
 * </pre></blockquote>
 * <p>
 * 输出以下字符串:
 * <blockquote><pre>
 *     this
 *     is
 *     a
 *     test
 * </pre></blockquote>
 *
 * <p>
 * <tt>StringTokenizer</tt> 是出于兼容性的原因而被保留的遗留类,在新代码中并不鼓励使用它.
 * 建议所有寻求此功能的人使用 <tt>String</tt> 的 <tt>split</tt> 方法或 java.util.regex 包来代替.
 * <p>
 * 下面的示例阐明了如何使用 <tt>String.split</tt> 方法将字符串分解为基本标记:
 * <blockquote><pre>
 *     String[] result = "this is a test".split("\\s");
 *     for (int x=0; x&lt;result.length; x++)
 *         System.out.println(result[x]);
 * </pre></blockquote>
 * <p>
 * 输出以下字符串:
 * <blockquote><pre>
 *     this
 *     is
 *     a
 *     test
 * </pre></blockquote>
 *
 * @author  unascribed
 * @see     java.io.StreamTokenizer
 * @since   JDK1.0
 */
public
class StringTokenizer implements Enumeration<Object> {
    private int currentPosition;
    private int newPosition;
    private int maxPosition;
    private String str;
    private String delimiters;
    private boolean retDelims;
    private boolean delimsChanged;

    /**
     * maxDelimCodePoint stores the value of the delimiter character with the
     * highest value. It is used to optimize the detection of delimiter
     * characters.
     *
     * It is unlikely to provide any optimization benefit in the
     * hasSurrogates case because most string characters will be
     * smaller than the limit, but we keep it so that the two code
     * paths remain similar.
     */
    private int maxDelimCodePoint;

    /**
     * If delimiters include any surrogates (including surrogate
     * pairs), hasSurrogates is true and the tokenizer uses the
     * different code path. This is because String.indexOf(int)
     * doesn't handle unpaired surrogates as a single character.
     */
    private boolean hasSurrogates = false;

    /**
     * When hasSurrogates is true, delimiters are converted to code
     * points and isDelimiter(int) is used to determine if the given
     * codepoint is a delimiter.
     */
    private int[] delimiterCodePoints;

    /**
     * Set maxDelimCodePoint to the highest char in the delimiter set.
     */
    private void setMaxDelimCodePoint() {
        if (delimiters == null) {
            maxDelimCodePoint = 0;
            return;
        }

        int m = 0;
        int c;
        int count = 0;
        for (int i = 0; i < delimiters.length(); i += Character.charCount(c)) {
            c = delimiters.charAt(i);
            if (c >= Character.MIN_HIGH_SURROGATE && c <= Character.MAX_LOW_SURROGATE) {
                c = delimiters.codePointAt(i);
                hasSurrogates = true;
            }
            if (m < c)
                m = c;
            count++;
        }
        maxDelimCodePoint = m;

        if (hasSurrogates) {
            delimiterCodePoints = new int[count];
            for (int i = 0, j = 0; i < count; i++, j += Character.charCount(c)) {
                c = delimiters.codePointAt(j);
                delimiterCodePoints[i] = c;
            }
        }
    }

    /**
     * 为指定字符串构造一个 string tokenizer.
     * <code>delim</code> 参数中的所有字符都是分隔标记的分隔符.
     * <p>
     * 如果 <code>returnDelims</code> 标志为 <code>true</code>,则分隔符字符也作为标记返回.每个分隔符都作为一个长度为 1 的字符串返回.
     * 如果标志为 <code>false</code>,则跳过分隔符,只是用作标记之间的分隔符.
     * <p>
     * 注意,如果 <tt>delim</tt> 为 <tt>null</tt>,则此构造方法不抛出异常.
     * 但是,尝试对得到的 <tt>StringTokenizer</tt> 调用其他方法则可能抛出 <tt>NullPointerException</tt>.
     *
     * @param   str           要解析的字符串.
     * @param   delim          分隔符.
     * @param   returnDelims   指示是否将分隔符作为标记返回的标志.
     * @exception NullPointerException 如果 str 为 <CODE>null</CODE>
     */
    public StringTokenizer(String str, String delim, boolean returnDelims) {
        currentPosition = 0;
        newPosition = -1;
        delimsChanged = false;
        this.str = str;
        maxPosition = str.length();
        delimiters = delim;
        retDelims = returnDelims;
        setMaxDelimCodePoint();
    }

    /**
     * 为指定字符串构造一个 string tokenizer.
     * <code>delim</code> 参数中的字符都是分隔标记的分隔符.分隔符字符本身不作为标记.
     * <p>
     * 注意,如果 <tt>delim</tt> 为 <tt>null</tt>,则此构造方法不抛出异常.
     * 但是,尝试对得到的 <tt>StringTokenizer</tt> 调用其他方法则可能抛出 <tt>NullPointerException</tt>.
     *
     * @param   str    要解析的字符串.
     * @param   delim   分隔符.
     * @exception NullPointerException 如果 str 为 <CODE>null</CODE>
     */
    public StringTokenizer(String str, String delim) {
        this(str, delim, false);
    }

    /**
     * 为指定字符串构造一个 string tokenizer.
     * tokenizer 使用默认的分隔符集,即 <code>"&nbsp;&#92;t&#92;n&#92;r&#92;f"</code>:空白字符、制表符、换行符、回车符和换页符.
     * 分隔符字符本身不作为标记.
     *
     * @param   str  要解析的字符串.
     * @exception NullPointerException 如果 str 为 <CODE>null</CODE>
     */
    public StringTokenizer(String str) {
        this(str, " \t\n\r\f", false);
    }

    /**
     * Skips delimiters starting from the specified position. If retDelims
     * is false, returns the index of the first non-delimiter character at or
     * after startPos. If retDelims is true, startPos is returned.
     */
    private int skipDelimiters(int startPos) {
        if (delimiters == null)
            throw new NullPointerException();

        int position = startPos;
        while (!retDelims && position < maxPosition) {
            if (!hasSurrogates) {
                char c = str.charAt(position);
                if ((c > maxDelimCodePoint) || (delimiters.indexOf(c) < 0))
                    break;
                position++;
            } else {
                int c = str.codePointAt(position);
                if ((c > maxDelimCodePoint) || !isDelimiter(c)) {
                    break;
                }
                position += Character.charCount(c);
            }
        }
        return position;
    }

    /**
     * Skips ahead from startPos and returns the index of the next delimiter
     * character encountered, or maxPosition if no such delimiter is found.
     */
    private int scanToken(int startPos) {
        int position = startPos;
        while (position < maxPosition) {
            if (!hasSurrogates) {
                char c = str.charAt(position);
                if ((c <= maxDelimCodePoint) && (delimiters.indexOf(c) >= 0))
                    break;
                position++;
            } else {
                int c = str.codePointAt(position);
                if ((c <= maxDelimCodePoint) && isDelimiter(c))
                    break;
                position += Character.charCount(c);
            }
        }
        if (retDelims && (startPos == position)) {
            if (!hasSurrogates) {
                char c = str.charAt(position);
                if ((c <= maxDelimCodePoint) && (delimiters.indexOf(c) >= 0))
                    position++;
            } else {
                int c = str.codePointAt(position);
                if ((c <= maxDelimCodePoint) && isDelimiter(c))
                    position += Character.charCount(c);
            }
        }
        return position;
    }

    private boolean isDelimiter(int codePoint) {
        for (int i = 0; i < delimiterCodePoints.length; i++) {
            if (delimiterCodePoints[i] == codePoint) {
                return true;
            }
        }
        return false;
    }

    /**
     * 测试此 tokenizer 的字符串中是否还有更多的可用标记.
     * 如果此方法返回 <tt>true</tt>,那么后续调用无参数的 <tt>nextToken</tt> 方法将成功地返回一个标记.
     *
     * @return  当且仅当该字符串中当前位置后至少有一个标记时才为 <code>true</code>;否则为 <code>false</code>.
     */
    public boolean hasMoreTokens() {
        /*
         * Temporarily store this position and use it in the following
         * nextToken() method only if the delimiters haven't been changed in
         * that nextToken() invocation.
         */
        newPosition = skipDelimiters(currentPosition);
        return (newPosition < maxPosition);
    }

    /**
     * 返回此 string tokenizer 的下一个标记.
     *
     * @return     此 string tokenizer 的下一个标记.
     * @exception  NoSuchElementException  如果此 tokenizer 的字符串中没有更多标记.
     */
    public String nextToken() {
        /*
         * If next position already computed in hasMoreElements() and
         * delimiters have changed between the computation and this invocation,
         * then use the computed value.
         */

        currentPosition = (newPosition >= 0 && !delimsChanged) ?
            newPosition : skipDelimiters(currentPosition);

        /* Reset these anyway */
        delimsChanged = false;
        newPosition = -1;

        if (currentPosition >= maxPosition)
            throw new NoSuchElementException();
        int start = currentPosition;
        currentPosition = scanToken(currentPosition);
        return str.substring(start, currentPosition);
    }

    /**
     * 返回此 string tokenizer 的字符串中的下一个标记.
     * 首先,字符集被更改为字符串 <tt>delim</tt> 中的字符,该字符集被认为是 <tt>StringTokenizer</tt> 对象的分隔符.
     * 接着返回字符串中当前位置之后的下一个标记.当前位置被提前到所识别的标记前.进行此调用后新的分隔符集仍然保持默认值.
     *
     * @param      delim  新的分隔符.
     * @return     转换到新的分隔符集后的下一个标记.
     * @exception  NoSuchElementException  如果此 tokenizer 的字符串中没有更多标记.
     * @exception NullPointerException 如果 delim 为 <CODE>null</CODE>
     */
    public String nextToken(String delim) {
        delimiters = delim;

        /* delimiter string specified, so set the appropriate flag. */
        delimsChanged = true;

        setMaxDelimCodePoint();
        return nextToken();
    }

    /**
     * 返回与 <code>hasMoreTokens</code> 方法相同的值.
     * 它的存在使得此类可以实现 <code>Enumeration</code> 接口.
     *
     * @return  如果有更多标记,则为 <code>true</code>;否则为 <code>false</code>.
     * @see     java.util.Enumeration
     * @see     java.util.StringTokenizer#hasMoreTokens()
     */
    public boolean hasMoreElements() {
        return hasMoreTokens();
    }

    /**
     * 除了其声明返回值是 <code>Object</code> 而不是 <code>String</code> 之外,它返回与 <code>nextToken</code> 方法相同的值.
     * 它的存在使得此类可以实现 <code>Enumeration</code> 接口.
     *
     * @return     字符串中的下一个标记.
     * @exception  NoSuchElementException  如果此 tokenizer 的字符串中没有更多标记.
     * @see        java.util.Enumeration
     * @see        java.util.StringTokenizer#nextToken()
     */
    public Object nextElement() {
        return nextToken();
    }

    /**
     * 计算在生成异常之前可以调用此 tokenizer 的 <code>nextToken</code> 方法的次数.当前位置没有提前.
     *
     * @return  使用当前分隔符集的字符串中剩余的标记数.
     * @see     java.util.StringTokenizer#nextToken()
     */
    public int countTokens() {
        int count = 0;
        int currpos = currentPosition;
        while (currpos < maxPosition) {
            currpos = skipDelimiters(currpos);
            if (currpos >= maxPosition)
                break;
            currpos = scanToken(currpos);
            count++;
        }
        return count;
    }
}
